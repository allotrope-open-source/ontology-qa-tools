# Ontology Catalog Generator

This tool will parse existing catalog files and update file locations and add any new ontologies to a given group.

See our [Devops Wiki](https://gitlab.com/allotrope-open-source/allotrope-devops/wikis/The-Ontology_Catalog-Generator) for documentation.