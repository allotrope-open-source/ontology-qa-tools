package com.osthus.ontology_catalog_generator;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.Vector;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.vocabulary.OWL;

import com.osthus.ontology_qa_common.ModulesHandler;
import com.osthus.ontology_qa_common.ModulesHandler.CatalogEntry;
import com.osthus.ontology_qa_common.TextFileHandler;

/*
 *    Copyright 2017-2021 OSTHUS
 *
 *      https://www.osthus.com
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * This tool will parse existing catalog files and update file locations and add
 * any new ontologies to a given group.
 */
public class Main {

	// For getting the ontology
	static final String RDF_TYPE_KEY = "http://www.w3.org/1999/02/22-rdf-syntax-ns#type";
	static final String IMPORTS_KEY = "http://www.w3.org/2002/07/owl#imports";
	private static final String IMPORT_GROUP = "import";
	private static final String CATALOG_FILENAME = "catalog-v001.xml";

	private static final String XmlCatalogHeader = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n"
			+ "<!DOCTYPE catalog[\n" //
			+ "<!ELEMENT catalog (uri|group)+>\n" //
			+ "<!ELEMENT group (uri)+>\n" //
			+ "<!ELEMENT uri EMPTY>\n" //
			+ "<!ATTLIST catalog\n" //
			+ "\txmlns CDATA #IMPLIED\n" //
			+ "\tprefer (system|public) #IMPLIED\n" //
			+ ">\n" //
			+ "<!ATTLIST uri\n" //
			+ "\tid ID #IMPLIED\n" //
			+ "\tname CDATA #REQUIRED\n" //
			+ "\turi CDATA #REQUIRED\n" //
			+ ">\n" //
			+ "<!ATTLIST group\n" //
			+ "\tid ID #IMPLIED\n" //
			+ "\tprefer (system|public) #IMPLIED\n" //
			+ "\txml:base CDATA #IMPLIED\n" //
			+ ">\n" //
			+ "]>\n" //
			+ "<catalog prefer=\"public\" xmlns=\"urn:oasis:names:tc:entity:xmlns:xml:catalog\">\n";
	private static final String XmlCatalogFooter = "</catalog>\n";
	private static final String XmlGroupHeaderTemplate = "\t<group id=\"{GROUPNAME}\" prefer=\"public\" xml:base=\"\">\n";
	private static final String XmlGroupFooter = "\t</group>\n";
	private static final String XmlUriTemplate = "\t\t<uri id=\"id{ID}\" name=\"{NAME}\" uri=\"{URI}\"/>\n";

	private static Vector<String> infos = new Vector<String>();
	private static Vector<String> warnings = new Vector<String>();
	private static Vector<String> errors = new Vector<String>();

	/**
	 * Container with information about a Turtle file
	 */
	private static class FileGroupInfo {

		/**
		 * Keep track of the highest ID number so that we can assign IDs to new entries
		 */
		private static int highestId = 0;

		/** A unique ID */
		private int id;

		/** The full path to .ttl file */
		public String filePath;

		/** The group name that this file is in */
		public String group;

		/**
		 * We track the last catalog that this was read from for error handling purposes
		 */
		public String lastCatalog;

		/** List of ontologies that are imported by this ontology */
		public TreeSet<String> importedOntologies;

		/**
		 * Information about an ontology Turtle file
		 * 
		 * @param filePath    the full path to the file
		 * @param group       the group that this file is assigned to
		 * @param lastCatalog the last catalog that this file was listed in
		 * @param id          A unique ID for the XML file entry, in the format
		 *                    "id&lt;digits&gt;"
		 */
		public FileGroupInfo(String filePath, String group, String lastCatalog, Integer id) {
			super();
			this.filePath = filePath;
			this.group = group;
			this.lastCatalog = lastCatalog;
			importedOntologies = new TreeSet<String>();
			this.id = id.intValue();
			FileGroupInfo.highestId = Math.max(FileGroupInfo.highestId, this.id);
		}

		public FileGroupInfo(String filePath, String group, String lastCatalog) {
			super();
			this.filePath = filePath;
			this.group = group;
			this.lastCatalog = lastCatalog;
			this.id = -1;
			importedOntologies = new TreeSet<String>();
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((filePath == null) ? 0 : filePath.hashCode());
			result = prime * result + ((group == null) ? 0 : group.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			FileGroupInfo other = (FileGroupInfo) obj;
			if (filePath == null) {
				if (other.filePath != null)
					return false;
			} else if (!filePath.equals(other.filePath))
				return false;
			if (group == null) {
				if (other.group != null)
					return false;
			} else if (!group.equals(other.group))
				return false;
			return true;
		}

		@Override
		public String toString() {
			return "FileGroupInfo [id=" + id + ", filePath=" + filePath + ", group=" + group + ", lastCatalog="
					+ lastCatalog + ", importedOntologies=" + importedOntologies + "]";
		}

		public int id() {
			if (id == -1) {
				FileGroupInfo.highestId++;
				id = FileGroupInfo.highestId;
			}
			return this.id;
		}

	}

	/**
	 * This tool will parse existing catalog files and update file locations and add
	 * any new ontologies to the given group.
	 * 
	 * @param args
	 *             <dl>
	 *             <dt>base_path
	 *             <dd>The path to walk</dd>
	 *             <dt>group_name</dt>
	 *             <dd>The group that any new ontologies will be assigned to</dd>
	 *             </dl>
	 */
	public static void main(String[] args) {
		System.out.println(
				"This tool will parse existing catalog files and update file locations and add any new ontologies to the given group.");
		if (args.length != 2) {
			System.out.println("Usage: java -jar ontology-catalog-generator.jar <base_path> <group_name>");
			System.exit(1);
		}

		// Group to assign to any new ontologies
		final String groupToAssign = args[1];
		// Base path to search for catalog xml files and .ttl files
		String basePath = args[0];
		File dir = new File(basePath);
		try {
			if (!dir.isDirectory()) {
				System.out.println("Base path '" + basePath + "' is not a directory!");
				System.exit(1);
			}
			basePath = dir.getCanonicalPath();
			System.out.println("Base path is: " + basePath);
		} catch (IOException e) {
			System.out.println("Illegal base path: " + basePath + "!");
			e.printStackTrace();
			System.exit(1);
		}

		// Build up catalog information
		Map<String, FileGroupInfo> ontologies = fetchCatalogs(basePath);
		reportErrors();
		fetchFiles(groupToAssign, dir, ontologies);
		reportErrors();
		readImports(ontologies);
		reportErrors();
		writeCatalogs(groupToAssign, dir, ontologies);

		// Check error and warning states
		if (!infos.isEmpty()) {
			System.out.println();
			for (final String infoMessage : infos) {
				System.out.println(infoMessage);
			}
		}
		reportErrors();
		System.out.println("\nCatalogs generated.");
	}

	/**
	 * Print any errors and/or warnings. Exit 1 if any errors were found.
	 */
	private static void reportErrors() {
		if (!errors.isEmpty()) {
			if (!warnings.isEmpty()) {
				System.out.println("\nFound " + warnings.size() + " warning(s):");
				for (final String warningMessage : warnings) {
					System.out.println(warningMessage);
				}
			}
			System.out.println("\nFound " + errors.size() + " error(s):");
			for (final String errorMessage : errors) {
				System.out.println(errorMessage);
			}
			System.out.println("\nCatalog generation failed. Please fix the errors manually, then run the tool again.");
			System.exit(1);
		}
	}

	/**
	 * Writes all ontology to catalogs. Each directory below baseDirectory will
	 * receive a catalog file if it contains any Turtle files
	 * 
	 * @param groupToAssign the catalog group to assign if the ontology only exists
	 *                      as an import so far
	 * @param baseDirectory the directory to walk
	 * @param ontologies    the ontology info to write
	 */
	private static void writeCatalogs(String groupToAssign, File baseDirectory, Map<String, FileGroupInfo> ontologies) {
		// Reorganize per directory for writing
		TreeMap<String, TreeMap<String, TreeMap<String, FileGroupInfo>>> directoryGroups = new TreeMap<String, TreeMap<String, TreeMap<String, FileGroupInfo>>>();

		for (final String ontology : ontologies.keySet()) {
			final FileGroupInfo info = ontologies.get(ontology);
			// Some ontologies might already be declared as input, but missing a
			// basic catalog entry
			if (info.group.equals("import")) {
				info.group = groupToAssign;
				handleInfo("Added file: " + info.filePath + " - " + ontology);
			}

			final File file = new File(info.filePath);
			final String directoryName = file.getParent();
			if (!directoryGroups.containsKey(directoryName)) {
				directoryGroups.put(directoryName, new TreeMap<String, TreeMap<String, FileGroupInfo>>());
			}
			TreeMap<String, TreeMap<String, FileGroupInfo>> ontologyMap = directoryGroups.get(directoryName);
			if (!ontologyMap.containsKey(info.group)) {
				ontologyMap.put(info.group, new TreeMap<String, FileGroupInfo>());
			}
			ontologyMap.get(info.group).put(ontology, info);
		}

		try {
			System.out.println("Writing catalogs ...");
			// Ignore files from .ttlignore
			HashSet<String> ignorelist = new HashSet<String>();

			// Collect all catalog and TTL files
			Queue<File> directories = new LinkedList<File>();
			directories.add(baseDirectory);
			final int baseDirectoryStringLengthWithSeparator = baseDirectory.getCanonicalPath().length() + 1;

			while (!directories.isEmpty()) {
				final File directory = directories.remove();
				final String directoryFilename = directory.getCanonicalPath();
				boolean hasTTLFile = false;
				for (File file : directory.listFiles()) {
					if (file.isDirectory()) {
						directories.add(file);
					} else if (file.getName().equals(".ttlignore")) {
						TextFileHandler.read(ignorelist, file.getCanonicalPath(), false);
					}
				}
				for (File file : directory.listFiles()) {
					if (file.getName().toLowerCase().endsWith(".ttl") && !ignorelist.contains(file.getName())) {
						hasTTLFile = true;
						break;
					}
				}
				if (hasTTLFile) {
					if (!directoryGroups.containsKey(directoryFilename)) {
						// TODO only a warning for now, because we have
						// anonymous ontologies for our licensing. Revisit this
						// decision.
						handleWarning(
								"Something's wrong, there should be something in the catalog for " + directory + "!");
					}
					if (directoryGroups.containsKey(directoryFilename)) {
						// Filter the imports for files used in the current
						// directory
						TreeSet<String> importedOntologiesForDirectory = new TreeSet<String>();
						Queue<String> importsToWalk = new LinkedList<String>();
						for (final String ontology : ontologies.keySet()) {
							final FileGroupInfo filegroupinfo = ontologies.get(ontology);
							final String ontologyDirectoryName = (new File(filegroupinfo.filePath)).getParent();
							if (ontologyDirectoryName.equals(directoryFilename)) {
								for (final String importedOntology : filegroupinfo.importedOntologies) {
									importedOntologiesForDirectory.add(importedOntology);
									importsToWalk.add(importedOntology);
								}
							}
						}

						// Add transitive imports to the list
						while (!importsToWalk.isEmpty()) {
							final String ontology = importsToWalk.remove();
							if (ontologies.containsKey(ontology)) {
								final FileGroupInfo filegroupinfo = ontologies.get(ontology);
								for (final String importedOntology : filegroupinfo.importedOntologies) {
									if (importedOntology.equals(ontology)) {
										System.out.println("ERROR: " + ontology + " is importing itself! Aborting.");
										System.exit(1);
									}
									if (!importedOntologiesForDirectory.contains(importedOntology)) {
										importedOntologiesForDirectory.add(importedOntology);
										importsToWalk.add(importedOntology);
									}
								}
							}
						}

						// Fetch the imports
						final String depthPrefix = calculateDepth(baseDirectory, directory);
						String otherGroupsXML = "";
						TreeSet<String> writtenFiles = new TreeSet<String>();

						// Now read the groups for the current directory. Skip
						// the imports.
						TreeMap<String, TreeMap<String, FileGroupInfo>> groupInfos = directoryGroups
								.get(directoryFilename);
						for (final String group : groupInfos.keySet()) {
							if (group.equals(IMPORT_GROUP)) {
								continue;
							}
							otherGroupsXML += XmlGroupHeaderTemplate.replace("{GROUPNAME}", group);
							TreeMap<String, FileGroupInfo> ontologyList = groupInfos.get(group);
							for (final String ontology : ontologyList.keySet()) {
								writtenFiles.add(ontology);
								FileGroupInfo info = ontologyList.get(ontology);
								otherGroupsXML += XmlUriTemplate.replace("{ID}", Integer.toString(info.id()))
										.replace("{NAME}", ontology)
										.replace("{URI}", (new File(info.filePath).getName()));
							}
							otherGroupsXML += XmlGroupFooter;
						}

						String importGroupXML = XmlGroupHeaderTemplate.replace("{GROUPNAME}", IMPORT_GROUP);
						for (final String importDirectory : directoryGroups.keySet()) {
							if (importDirectory.equals(directoryFilename)) {
								// We will want the real groups for the
								// current directory
								continue;
							} else {
								// Now read imports
								groupInfos = directoryGroups.get(importDirectory);
								for (final String group : groupInfos.keySet()) {
									TreeMap<String, FileGroupInfo> ontologyList = groupInfos.get(group);
									for (final String ontology : ontologyList.keySet()) {
										if (importedOntologiesForDirectory.contains(ontology)) {
											FileGroupInfo info = ontologyList.get(ontology);
											final String filename = info.filePath
													.substring(baseDirectoryStringLengthWithSeparator);

											importGroupXML += XmlUriTemplate
													.replace("{ID}", Integer.toString(info.id()))
													.replace("{NAME}", ontology)
													.replace("{URI}", depthPrefix + filename.replace("\\", "/"));
										}
									}
								}
							}

						}
						importGroupXML += XmlGroupFooter;

						System.out.println("Writing catalog for dir: " + directoryFilename);
						final String fileContents = XmlCatalogHeader + importGroupXML + otherGroupsXML
								+ XmlCatalogFooter;
						TextFileHandler.write(fileContents, directoryFilename + "/" + CATALOG_FILENAME);
					}
				} else {
					final File catalogFile = new File(directoryFilename, CATALOG_FILENAME);
					if (catalogFile.exists()) {
						System.out.println("No TTL FILES for " + directoryFilename + " - deleting catalog");
						catalogFile.delete();
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
			handleError("Exception occurred while writing catalogs.");
		}
	}

	/**
	 * Calculate how much deeper 'directory' is in comparison to 'baseDirectory'
	 * 
	 * @param baseDirectory our root
	 * @param directory     the directory to calculate the depth of
	 * @return The subdirectories as a string of concatenated "../"
	 * @throws IOException
	 */
	private static String calculateDepth(final File baseDirectory, final File directory) throws IOException {
		assert (directory.isDirectory());
		String result = "";
		File tempDirectory = new File(directory.getAbsolutePath());
		while (!tempDirectory.getCanonicalPath().equals(baseDirectory.getCanonicalPath())) {
			tempDirectory = tempDirectory.getParentFile();
			result += "../";
		}
		return result;
	}

	/**
	 * Walks the file system to add any new ontologies that aren't listed in
	 * 'ontologies' yet. For ontologies already listed, updates the file information
	 * if needed. Adds an error if two files have the same ontology. Removes
	 * ontologies that no longer exist in the file system from the list.
	 * 
	 * @param groupToAssign catalog group for the file that aren't listed in the
	 *                      ontologies yet
	 * @param dir           the directory to search
	 * @param ontologies    the ontologies to add new files to
	 */
	private static void fetchFiles(String groupToAssign, File dir, Map<String, FileGroupInfo> ontologies) {
		HashSet<String> filesToParse = ModulesHandler.collectTurtleFiles(dir);
		for (final String file : filesToParse) {
			// Now check the ontology IRI in the file
			Model model;
			try {
				model = RDFDataMgr.loadModel(file);
			} catch (Exception e) {
				if (e.getMessage().contains("java.nio.charset.MalformedInputException")) {
					handleError("Error loading model for file " + file
							+ " - please check the character encoding for the file\n       " + e.getMessage());
				} else {
					handleError("Error loading model for file " + file + ": " + e.getMessage());
				}
				continue;
			}
			// Find the ontology's identifier subject
			List<Statement> statements = model
					.listStatements(null, ResourceFactory.createProperty(RDF_TYPE_KEY), OWL.Ontology).toList();

			// Test that we have exactly 1 ontology subject and
			// that the IRIs are identical
			if (statements.size() == 1) {
				String resourceName = statements.get(0).getSubject().toString();
				if (ontologies.containsKey(resourceName)) {
					final String catalogFile = ontologies.get(resourceName).filePath;
					if (!file.equals(catalogFile)) { // TODO this fails
						if (!(new File(catalogFile)).exists()) {
							// The file has been moved, just assign it to the
							// real file
							ontologies.get(resourceName).filePath = file;
							System.out.println("Identified file " + file + " for " + resourceName);
						} else {
							handleError("The same ontology '" + resourceName + "' exists in 2 different files:\n- "
									+ catalogFile + "\n- " + file);
						}
					}
				} else if (resourceName.startsWith("http")) {
					// The file is not listed yet. Add non-anonymous nodes
					handleInfo("Added file: " + file + " - " + resourceName);
					ontologies.put(resourceName, new FileGroupInfo(file, groupToAssign, ""));
				} else {
					handleWarning("Warning: Found anonymous ontology in " + file);
				}
			} else {
				String errorMessage = "File needs to have exactly 1 ontology, but we found:";
				for (Statement statement : statements) {
					errorMessage += "\n" + statement.toString();
				}
				handleError(errorMessage);
			}
		}

		// Ditch files that no longer exist
		Iterator<Map.Entry<String, FileGroupInfo>> iter = ontologies.entrySet().iterator();
		while (iter.hasNext()) {
			Map.Entry<String, FileGroupInfo> ontology = iter.next();
			final FileGroupInfo info = ontology.getValue();
			if (!(new File(info.filePath)).exists()) {
				System.out.println("\nRemoving nonexistent file from catalogs\n- Ontology: " + ontology.getKey()
						+ "\n- File:     " + info.filePath + "\n- Catalog:  " + info.lastCatalog);
				iter.remove();
				System.out.println("Removed nonexisting file " + info.filePath);
			}
		}
	}

	/**
	 * Reads all xml catalogs in basePath. Returns group into for each file listed
	 * in the catalogs. Adds an error if a file has two different groups at the same
	 * time (ignoring imports).
	 */
	private static Map<String, FileGroupInfo> fetchCatalogs(String basePath) {
		Map<String, FileGroupInfo> ontologies = new TreeMap<>();
		try {
			final Map<String, CatalogEntry> catalogs = ModulesHandler.read(basePath, new HashSet<String>());
			for (ModulesHandler.CatalogEntry entry : catalogs.values()) {
				for (String entryName : entry.namesAndIds.keySet()) {
					Main.FileGroupInfo new_info = new Main.FileGroupInfo(entry.ttlFile.trim(), entry.groupName.trim(),
							entry.catalogFile.trim(), entry.namesAndIds.get(entryName));

					if (ontologies.containsKey(entryName)) {
						Main.FileGroupInfo old_info = ontologies.get(entryName);
						if (old_info.equals(new_info)) {
							// We already have it, so nothing to do
							continue;
						}
						if (!old_info.filePath.equals(new_info.filePath)) {
							// Ditch non-existent files
							if ((new File(old_info.filePath)).exists()) {
								// If file is changed here, we also assume that the
								// group for the nonexistent file is less correct
								new_info.filePath = old_info.filePath;
								new_info.group = old_info.group;
							}
						}
					}
					ontologies.put(entryName, new_info);
				}
			}
		} catch (Exception e1) {
			e1.printStackTrace();
			handleError(e1.getMessage());
		}
		return ontologies;
	}

	/**
	 * Reads the imports from the ontologies listed in the FileGroupInfos and
	 * updates the FileGroupInfos with that information
	 * 
	 * @param ontologies List of ontologies to read and update
	 */
	private static void readImports(Map<String, FileGroupInfo> ontologies) {
		for (final String ontologyName : ontologies.keySet()) {
			try {
				FileGroupInfo fileGroupInfo = ontologies.get(ontologyName);
				Model model = RDFDataMgr.loadModel(fileGroupInfo.filePath);

				StmtIterator importsIterator = model.getResource(ontologyName).listProperties(OWL.imports);
				while (importsIterator.hasNext()) {
					final String importedOntology = importsIterator.nextStatement().getObject().toString();
					if (!ontologies.containsKey(importedOntology)) {
						// TODO hard-coded - not good!
						if (importedOntology.contains("allotrope")) {
							handleError("Unknown import " + importedOntology + " in ontology " + ontologyName);
						} else {
							handleWarning("Unknown import " + importedOntology + " in ontology " + ontologyName);
						}
					}
					fileGroupInfo.importedOntologies.add(importedOntology);
				}
			} catch (Exception e) {
				if (e.getMessage().contains("java.nio.charset.MalformedInputException")) {
					handleError("Error parsing import in ontology " + ontologyName
							+ " - please check the character encoding for the file\n       " + e.getMessage());
				} else {
					handleError("Error parsing import in ontology " + ontologyName + ": " + e.getMessage());
				}
			}
		}
	}

	/**
	 * Convenience function for registering an error
	 * 
	 * @param message the message for the error to register
	 */
	public static void handleError(String message) {
		errors.addElement("ERROR! " + message);
	}

	/**
	 * Convenience function for registering a warning
	 * 
	 * @param message the message for the warning to register
	 */
	public static void handleInfo(String message) {
		infos.addElement("Info: " + message);
	}

	/**
	 * Convenience function for registering a warning
	 * 
	 * @param message the message for the warning to register
	 */
	public static void handleWarning(String message) {
		warnings.addElement("WARNING! " + message);
	}
}
