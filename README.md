# Allotrope Ontology QA Tools

This project contains various Java and Python tools that are used to verify and release the Allotrope ontology.

* **[Ontology Catalog Reader](/catalog-reader)**: Reads catalog xml files and prints the output to a tsv file.
* **[Ontology Catalog Generator](/catalog-generator)**: Updates xml catalog files.
* **[Ontology Merger](/merger)**: Merges multiple Turtle files into one global file and will run inferencing on them.
* **[Ontology Modules Verifier](/modules-verifier)**: Checks the catalog xml files against the ttl files to make sure that all entries match.
* **[Ontology Missing Resources Detector](/missing-resources-detector)**: Compares 2 `.ttl` files to detect any missing triples.
* **[Ontology Normalizer](/normalizer)**: Will normalize a Turtle file to minimize merge conflicts.
* **[Ontology QA Common](/java-common)**: Contains common classes that are used by more than one tool.
* **[Ontology Releaser](/releaser)**: Prepares the files for a release.
* **[Ontology Styleguide Checker-checker](/styleguide-checker)**: Various integrity checks on the ontology's triples to help ensure style guide conformity.
* **[Python Tools](/python-tools)**: A collection of tool written in Python. Grouped together for easier code reuse.

* [Documentation](https://gitlab.com/allotrope-open-source/allotrope-devops/wikis/home)