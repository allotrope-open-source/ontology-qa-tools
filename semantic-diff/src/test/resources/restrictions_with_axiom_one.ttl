@prefix : <http://foo/bar/test2#> .
@prefix bfo:   <http://purl.obolibrary.org/obo/BFO> .
@prefix dct:   <http://purl.org/dc/terms/> .
@prefix iao:   <http://purl.obolibrary.org/obo/IAO> .
@prefix owl:   <http://www.w3.org/2002/07/owl#> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos: <http://www.w3.org/2004/02/skos/core#> .
@prefix xml:   <http://www.w3.org/xml/1998/namespace> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

@base <http://foo/bar/testrestrictionsaxiom> .

<http://foo/bar/testrestrictionsaxiom> rdf:type owl:Ontology .

#################################################################
#    Annotation properties
#################################################################

###  http://foo/bar/testannotation1
<http://foo/bar/testannotation1> rdf:type owl:AnnotationProperty ;
                                 skos:prefLabel "test annotation" .


###  http://purl.org/dc/terms/source
dct:source rdf:type owl:AnnotationProperty .


###  http://www.w3.org/2004/02/skos/core#altLabel
skos:altLabel rdf:type owl:AnnotationProperty .


###  http://www.w3.org/2004/02/skos/core#changeNote
skos:changeNote rdf:type owl:AnnotationProperty .


###  http://www.w3.org/2004/02/skos/core#definition
skos:definition rdf:type owl:AnnotationProperty .


###  http://www.w3.org/2004/02/skos/core#example
skos:example rdf:type owl:AnnotationProperty .


###  http://www.w3.org/2004/02/skos/core#note
skos:note rdf:type owl:AnnotationProperty .


###  http://www.w3.org/2004/02/skos/core#prefLabel
skos:prefLabel rdf:type owl:AnnotationProperty .


#################################################################
#    Object Properties
#################################################################

###  http://foo/bar/testprop1
<http://foo/bar/testprop1> rdf:type owl:ObjectProperty ;
                           rdfs:subPropertyOf <http://foo/bar/testprop4> ;
                           owl:inverseOf <http://foo/bar/testprop3> ;
                           rdf:type owl:AsymmetricProperty ,
                                    owl:IrreflexiveProperty ;
                           skos:definition "Has member is a mereological relation between a collection and an item. [RO]" ;
                           skos:prefLabel "has member" .


###  http://foo/bar/testprop2
<http://foo/bar/testprop2> rdf:type owl:ObjectProperty ;
                           rdfs:subPropertyOf <http://foo/bar/testprop1> ;
                           owl:inverseOf <http://foo/bar/testprop5> ;
                           rdf:type owl:AsymmetricProperty ,
                                    owl:IrreflexiveProperty ;
                           rdfs:domain <http://foo/bar/test3> ;
                           rdfs:range <http://foo/bar/test6> ;
                           skos:altLabel "item" ,
                                         "member" ;
                           skos:definition "This property links a collection to a member item. [Allotrope]" ;
                           skos:prefLabel "has item" .


###  http://foo/bar/testprop3
<http://foo/bar/testprop3> rdf:type owl:ObjectProperty ;
                           rdfs:subPropertyOf <http://foo/bar/testprop6> ;
                           rdf:type owl:AsymmetricProperty ,
                                    owl:IrreflexiveProperty ;
                           skos:altLabel "member part of" ;
                           skos:definition "Inverse of has member" ;
                           skos:example "An organism that is a member of a population of organisms. [RO]" ;
                           skos:prefLabel "member of" .


###  http://foo/bar/testprop4
<http://foo/bar/testprop4> rdf:type owl:ObjectProperty ;
                           owl:inverseOf <http://foo/bar/testprop6> ;
                           rdf:type owl:TransitiveProperty ;
                           <http://foo/bar/testannotation1> <http://semanticscience.org/resource/SIO_000053> ;
                           skos:definition "Has proper part is an antisymmetric, irreflexive (normally transitive) relation between a whole and a distinct part. [SIO]" ;
                           skos:example "An atom has subatomic particles as its proper parts." ;
                           skos:prefLabel "has proper part" .


###  http://foo/bar/testprop5
<http://foo/bar/testprop5> rdf:type owl:ObjectProperty ;
                           rdfs:subPropertyOf <http://foo/bar/testprop3> ;
                           rdf:type owl:AsymmetricProperty ,
                                    owl:IrreflexiveProperty ;
                           rdfs:range <http://foo/bar/test3> ;
                           skos:altLabel "in collection" ,
                                         "is item in" ,
                                         "member of" ;
                           skos:changeNote "2017-11-16 Changed pref label [OSTHUS]" ,
                                           "2018-07-23 Change hierarchy, subproperty of member of [Allotrope]" ;
                           skos:definition "In collection relates an item to a collection entity. [Allotrope]" ;
                           skos:prefLabel "item of" .


###  http://foo/bar/testprop6
<http://foo/bar/testprop6> rdf:type owl:ObjectProperty ;
                           skos:altLabel "is proper part of" ;
                           skos:definition "Inverse of has proper part. [Allotrope]" ;
                           skos:prefLabel "proper part of" .


#################################################################
#    Classes
#################################################################

###  http://foo/bar/test1
<http://foo/bar/test1> rdf:type owl:Class ;
                       rdfs:subClassOf [ owl:intersectionOf ( <http://foo/bar/test3>
                                                              [ rdf:type owl:Restriction ;
                                                                owl:onProperty <http://foo/bar/testprop1> ;
                                                                owl:someValuesFrom <http://foo/bar/test4>
                                                              ]
                                                            ) ;
                                         rdf:type owl:Class
                                       ] .


###  http://foo/bar/test2
<http://foo/bar/test2> rdf:type owl:Class ;
                       rdfs:subClassOf [ owl:intersectionOf ( <http://foo/bar/test3>
                                                              [ rdf:type owl:Restriction ;
                                                                owl:onProperty <http://foo/bar/testprop1> ;
                                                                owl:someValuesFrom <http://foo/bar/test4>
                                                              ]
                                                            ) ;
                                         rdf:type owl:Class
                                       ] .


###  http://foo/bar/test3
<http://foo/bar/test3> rdf:type owl:Class ;
                       rdfs:subClassOf <http://foo/bar/test5> ,
                                       [ owl:intersectionOf ( [ rdf:type owl:Restriction ;
                                                                owl:onProperty <http://foo/bar/testprop1> ;
                                                                owl:allValuesFrom <http://foo/bar/test6>
                                                              ]
                                                              [ rdf:type owl:Restriction ;
                                                                owl:onProperty <http://foo/bar/testprop2> ;
                                                                owl:allValuesFrom <http://foo/bar/test6>
                                                              ]
                                                            ) ;
                                         rdf:type owl:Class
                                       ] ;
                       skos:definition "An aggregation of data items for a purpose. [Allotrope]" ;
                       skos:note """A collection itself is never an object aggregate, but an abstraction
					   and might be about an object aggregate.""" ;
                       skos:prefLabel "collection" .


###  http://foo/bar/test4
<http://foo/bar/test4> rdf:type owl:Class ;
                       skos:definition "A condition is an information content entity that is about the portion of reality under which something occurs or is valid. The condition is limited by its scope and may have preconditions. It restricts the possible realizations. [Allotrope]" ;
                       skos:prefLabel "condition" .


###  http://foo/bar/test5
<http://foo/bar/test5> rdf:type owl:Class .


###  http://foo/bar/test6
<http://foo/bar/test6> rdf:type owl:Class ;
                       owl:equivalentClass [ rdf:type owl:Restriction ;
                                             owl:onProperty <http://foo/bar/testprop3> ;
                                             owl:someValuesFrom <http://foo/bar/test3>
                                           ] ;
                       rdfs:subClassOf <http://foo/bar/test5> ;
                       skos:altLabel "element" ;
                       skos:definition "An item is an individual member of some collection. [Allotrope]" ;
                       skos:note "The item is not a physical object that is member of an object aggregate, but is an abstraction that might be about such an object." ;
                       skos:prefLabel "item" .


###  http://foo/bar/test7
<http://foo/bar/test7> rdf:type owl:Class ;
                       dct:source <https://en.wikipedia.org/wiki/Mathematical_object> ;
                       skos:altLabel "abstract object" ;
                       skos:definition "A mathematical object is an information content entity that is an abstraction in the area of mathematics. It has a formal definition that allows for deductive reasoning and mathematical proofs. [Allotrope]" ;
                       skos:prefLabel "mathematical object" .

[ a                      owl:Axiom ;
  iao:_0010000           <http://purl.obolibrary.org/obo/bfo/axiom/095-001> ;
  owl:annotatedProperty  iao:_0000600 ;
  owl:annotatedSource    bfo:_0000011 ;
  owl:annotatedTarget    "A spatiotemporal region is an occurrent entity that is part of spacetime. (axiom label in BFO2 Reference: [095-001])"
] .


###  Generated by the OWL API (version 4.2.8.20170104-2310) https://github.com/owlcs/owlapi
