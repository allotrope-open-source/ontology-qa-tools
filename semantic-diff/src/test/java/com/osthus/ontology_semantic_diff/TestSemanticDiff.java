package com.osthus.ontology_semantic_diff;

/*
 *    Copyright 2021 OSTHUS
 *
 *      https://www.allotrope.org/
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;

import org.junit.AfterClass;
import org.junit.Test;

public class TestSemanticDiff {

	private static final String TEST_DIR = System.getProperty("user.dir") + "/src/test/resources/";
	private static final String OUTPUT_DIR = TEST_DIR + "diff" + File.separator;

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		System.out.println("======================================================================================");
		System.out.println("Cleaning up");
		File dir = new File(OUTPUT_DIR);
		for (File file : dir.listFiles()) {
			if (!file.isDirectory()) {
				if (file.delete()) {
					System.out.println("Deleted " + file);
				} else {
					System.out.println("Failed to delete " + file);
				}
			}
		}
		System.out.println("Done");
		System.out.println("======================================================================================");
	}

	@Test
	public void testSameFile() {
		System.out.println("======================================================================================");
		System.out.println("TEST: Comparing same file");
		File file1 = new File(TEST_DIR + "one.ttl");
		File file2 = new File(TEST_DIR + "one.ttl");
		assertTrue("Files should have same semantics", SemanticDiffMain.compareFiles(file1, file2, OUTPUT_DIR, null, false));
	}
	

	@Test
	public void testAddedOntologyMetadata() {
		System.out.println("======================================================================================");
		System.out.println("TEST: Removing ontology metadata additions");
		File file1 = new File(TEST_DIR + "one.ttl");
		File file2 = new File(TEST_DIR + "ontology_metadata.ttl");
		assertTrue("Ontology metadata should be ignored", SemanticDiffMain.compareFiles(file1, file2, OUTPUT_DIR, null, true));
	}
	

	@Test
	public void testAddedOntologyMetadataFailure() {
		System.out.println("======================================================================================");
		System.out.println("TEST: Leaving ontology metadata additions in");
		File file1 = new File(TEST_DIR + "one.ttl");
		File file2 = new File(TEST_DIR + "ontology_metadata.ttl");
		assertFalse("Files should have different ontology metadata", SemanticDiffMain.compareFiles(file1, file2, OUTPUT_DIR, null, false));
	}

	@Test
	public void testRestrictionsSameFile() {
		System.out.println("======================================================================================");
		System.out.println("TEST: Comparing same file with restrictions");
		File file1 = new File(TEST_DIR + "restrictions.ttl");
		File file2 = new File(TEST_DIR + "restrictions.ttl");
		assertTrue("Files should have same restriction nodes",
				SemanticDiffMain.compareFiles(file1, file2, OUTPUT_DIR, null, false));
	}

	@Test
	public void testRestrictionsDifferentFiles() {
		System.out.println("======================================================================================");
		System.out.println("TEST: Comparing same file with restrictions");
		File file1 = new File(TEST_DIR + "restrictions.ttl");
		File file2 = new File(TEST_DIR + "restrictions_two.ttl");
		assertFalse("Files should have different restriction nodes",
				SemanticDiffMain.compareFiles(file1, file2, OUTPUT_DIR, null, false));
	}

	@Test
	public void testAxiomsSameFile() {
		System.out.println("======================================================================================");
		System.out.println("TEST: Comparing same file with axiom");
		File file1 = new File(TEST_DIR + "axiom_one.ttl");
		File file2 = new File(TEST_DIR + "axiom_one.ttl");
		assertTrue("Files should have same axioms", SemanticDiffMain.compareFiles(file1, file2, OUTPUT_DIR, null, false));
	}

	@Test
	public void testAxiomsDifferentFiles() {
		System.out.println("======================================================================================");
		System.out.println("TEST: Comparing different files with axiom");
		File file1 = new File(TEST_DIR + "axiom_one.ttl");
		File file2 = new File(TEST_DIR + "axiom_two.ttl");
		assertFalse("Files should have different axioms",
				SemanticDiffMain.compareFiles(file1, file2, OUTPUT_DIR, null, false));
	}

	@Test
	public void testRestrictionsWithAxiomSameFile() {
		System.out.println("======================================================================================");
		System.out.println("TEST: Comparing same file with restrictions and axiom");
		File file1 = new File(TEST_DIR + "restrictions_with_axiom_one.ttl");
		File file2 = new File(TEST_DIR + "restrictions_with_axiom_one.ttl");
		assertTrue("Files should have same restrictions and axioms",
				SemanticDiffMain.compareFiles(file1, file2, OUTPUT_DIR, null, false));
	}

	@Test
	public void testRestrictionsWithDifferentAxiom() {
		System.out.println("======================================================================================");
		System.out.println("TEST: Comparing same file with restrictions and different axiom");
		File file1 = new File(TEST_DIR + "restrictions_with_axiom_one.ttl");
		File file2 = new File(TEST_DIR + "restrictions_with_axiom_two.ttl");
		assertFalse("Files should have same restrictions but a different axiom",
				SemanticDiffMain.compareFiles(file1, file2, OUTPUT_DIR, null, false));
	}

	@Test
	public void testOneLinerSameFile() {
		System.out.println("======================================================================================");
		System.out.println("TEST: Comparing same file with one-liner");
		File file1 = new File(TEST_DIR + "foaf_one.ttl");
		File file2 = new File(TEST_DIR + "foaf_one.ttl");
		assertTrue("Files should have same one-liner", SemanticDiffMain.compareFiles(file1, file2, OUTPUT_DIR, null, false));
	}

	@Test
	public void testOneLinerDifferentFiles() {
		System.out.println("======================================================================================");
		System.out.println("TEST: Comparing different files with one-liner");
		File file1 = new File(TEST_DIR + "foaf_one.ttl");
		File file2 = new File(TEST_DIR + "foaf_two.ttl");
		assertFalse("Files should have differenr one-liner",
				SemanticDiffMain.compareFiles(file1, file2, OUTPUT_DIR, null, false));
	}

	@Test
	public void testRestrictionsAxiomsOneLinerSameFile() {
		System.out.println("======================================================================================");
		System.out.println("TEST: Comparing same file with restrictions, axiom and one-liner");
		File file1 = new File(TEST_DIR + "restrictions_axioms_foaf_one.ttl");
		File file2 = new File(TEST_DIR + "restrictions_axioms_foaf_one.ttl");
		assertTrue("Files should have the same restrictions, axiom and one-liner",
				SemanticDiffMain.compareFiles(file1, file2, OUTPUT_DIR, null, false));
	}

	@Test
	public void testRestrictionsAxiomsOneLinerDifferentFiles() {
		System.out.println("======================================================================================");
		System.out.println("TEST: Comparing same file with restrictions and axiom but a different one-liner");
		File file1 = new File(TEST_DIR + "restrictions_axioms_foaf_one.ttl");
		File file2 = new File(TEST_DIR + "restrictions_axioms_foaf_two.ttl");
		assertFalse("Files should have the same restrictions and axiom but a different one-liner",
				SemanticDiffMain.compareFiles(file1, file2, OUTPUT_DIR, null, false));
	}

	@Test
	public void testSameFileOWL() {
		System.out.println("======================================================================================");
		System.out.println("TEST: Comparing ttl to owl");
		File file1 = new File(TEST_DIR + "one.ttl");
		File file2 = new File(TEST_DIR + "one.owl"); // TODO problem reading
														// file
		assertTrue("Files should have same semantics", SemanticDiffMain.compareFiles(file1, file2, OUTPUT_DIR, null, false));
	}

	@Test
	public void testDifferentFiles() {
		System.out.println("======================================================================================");
		System.out.println("TEST: Comparing different files");
		File file1 = new File(TEST_DIR + "one.ttl");
		File file2 = new File(TEST_DIR + "two.ttl");
		assertFalse("Files should have different semantics",
				SemanticDiffMain.compareFiles(file1, file2, OUTPUT_DIR, null, false));
	}

	@Test
	public void testDifferentFiles2() {
		System.out.println("======================================================================================");
		System.out.println("TEST: Comparing different files2");
		File file1 = new File(TEST_DIR + "one.ttl");
		File file2 = new File(TEST_DIR + "three.ttl");
		assertFalse("Files should have different semantics",
				SemanticDiffMain.compareFiles(file1, file2, OUTPUT_DIR, null, false));
	}

	@Test
	public void testMultipleOntologies() {
		System.out.println("======================================================================================");
		System.out.println("TEST: Comparing with multiple ontologies in same file");
		File file1 = new File(TEST_DIR + "one.ttl");
		File file2 = new File(TEST_DIR + "multiple.ttl");
		assertFalse("Expected failure due tu multiple ontologies in same file",
				SemanticDiffMain.compareFiles(file1, file2, OUTPUT_DIR, null, false));
	}

	@Test
	public void testNotIgnoreProperty() {
		System.out.println("======================================================================================");
		System.out.println("TEST: Comparing files with added property");
		File file1 = new File(TEST_DIR + "restrictions_with_axiom_two.ttl");
		File file2 = new File(TEST_DIR + "restrictions_with_axiom_and_properties.ttl");
		assertFalse("Files should have a property difference",
				SemanticDiffMain.compareFiles(file1, file2, OUTPUT_DIR, null, false));
	}

	@Test
	public void testIgnoreProperty() {
		System.out.println("======================================================================================");
		System.out.println("TEST: Comparing files with added but ignored property");
		File file1 = new File(TEST_DIR + "restrictions_with_axiom_two.ttl");
		File file2 = new File(TEST_DIR + "restrictions_with_axiom_and_properties.ttl");

		String[] properties = {
				"http://www.w3.org/ns/prov#wasDerivedFrom", "http://www.w3.org/1999/02/22-rdf-syntax-ns#type"
		};
		assertTrue("Property difference should be ignored",
				SemanticDiffMain.compareFiles(file1, file2, OUTPUT_DIR, properties, false));
	}
}
