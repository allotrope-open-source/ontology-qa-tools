package com.osthus.ontology_semantic_diff;

/*
 *    Copyright 2020-2021 OSTHUS
 *
 *      https://www.allotrope.org/
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import java.util.Map;
import java.util.TreeMap;

import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;

/**
 * Wrapper for statements that share the same subject, e.g. axioms
 */
public class BundledNode {
	private Resource subject;
	private Map<String, Statement> statements;

	public BundledNode(Statement statement) {
		this.subject = statement.getSubject();
		// TreeMap for sorting
		this.statements = new TreeMap<>();
		this.statements.put(makeKey(statement), statement);
	}

	public void addStatement(Statement statement) {
		statements.put(makeKey(statement), statement);
	}

	public static String makeKey(Statement statement) {
		return statement.getPredicate().toString() + "||" + statement.getObject().toString();
	}

	public Resource getSubject() {
		return subject;
	}

	public Map<String, Statement> getStatements() {
		return statements;
	}

	/**
	 * Counts how many statements were bundled in this node
	 * 
	 * @return The number of statements
	 */
	public int noOfStatements() {
		return statements.size();
	}

	/**
	 * This is safe for equality
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((statements == null) ? 0 : statements.size());
		if (statements != null) {
			for (String key : statements.keySet()) {
				result = prime * result + key.hashCode();
			}
		}
		return result;
	}

	/**
	 * Equal if each statement has a corresponding statement with same
	 * predicate/object. Subjects are ignored.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		BundledNode other = (BundledNode) obj;
		if (this.statements.size() != other.statements.size()) {
			return false;
		}

		for (String key : statements.keySet()) {
			if (!other.statements.containsKey(key)) {
				return false;
			}
		}

		return true;
	}

}
