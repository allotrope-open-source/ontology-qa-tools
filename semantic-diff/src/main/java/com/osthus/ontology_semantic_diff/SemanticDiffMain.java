package com.osthus.ontology_semantic_diff;

/*
 *    Copyright 2020-2021 OSTHUS
 *
 *      https://www.allotrope.org/
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.util.FileUtils;
import org.apache.jena.vocabulary.OWL;
import org.apache.jena.vocabulary.RDF;

import com.osthus.ontology_qa_common.TextFileHandler;

/**
 * Tool for comparing 2 ontologies. Writes diff Turtle files for any differences
 * found and prints the statements in the diffs to console.
 * 
 * Can handle any files that the Jena API can load (Turtle, RDF/XML, ...). File
 * type is detected from the file extension.
 *
 */
public class SemanticDiffMain {

	private static final int PROGRESS_INTERVAL = 1000;
	private static final int TRIPLES_TO_PRINT = 20;
	private static final String VISUAL_SEPARATOR1 = "======================================================================================";
	private static final String VISUAL_SEPARATOR2 = "--------------------------------------------------------------------------------------";
	private static final String OPTION_SKIP_PROPERTIES = "--skip-properties=";
	private static final String OPTION_SKIP_ONTOLOGY = "--skip-ontology";

	public static void main(String[] args) {
		System.out.println(
				"°°°°°°°°°°°°°°°°°°°°°°°°°°°°°\n°  Allotrope Semantic Diff  °\n°                           °\n°°°°°°°°°°°°°°°°°°°°°°°°°°°°°\n");
		System.out.println("This tool will create diff files for 2 ontology files.");
		if (args.length < 3 || args.length > 5) {
			System.out.println("Wrong number of arguments. Expected 3-5 but got " + args.length);
			printHelp();
			System.exit(1);
		}

		System.out.println(VISUAL_SEPARATOR1);
		System.out.println("Comparing files:");
		System.out.println("\t- " + args[0]);
		System.out.println("\t- " + args[1]);
		System.out.println("Output: " + args[2]);

		boolean skipOntology = false;
		String[] propertiesToSkip = null;

		if (args.length > 3) {
			for (int i = 3; i < args.length; i++) {

				if (OPTION_SKIP_ONTOLOGY.equals(args[i])) {
					System.out.println("Ignoring ontology metadata");
					skipOntology = true;
				} else if (args[i].startsWith(OPTION_SKIP_PROPERTIES)) {
					propertiesToSkip = args[i].substring(OPTION_SKIP_PROPERTIES.length()).split(",");

					System.out.println("Ignoring properties:");
					for (int j = 0; j < propertiesToSkip.length; j++) {
						System.out.println("\t- " + propertiesToSkip[j]);
					}
				} else {
					System.out.println("Unknown option: " + args[i]);
					printHelp();
					System.exit(1);
				}
			}
		}

		System.out.println(VISUAL_SEPARATOR2);

		if (SemanticDiffMain.compareFiles(new File(args[0]), new File(args[1]), args[2], propertiesToSkip,
				skipOntology)) {
			System.out.println("Done. The 2 ontologies are identical");
			System.out.println(VISUAL_SEPARATOR1);
			System.exit(0);
		} else {
			System.out.println("Done. The 2 ontologies differ.");
			System.out.println(VISUAL_SEPARATOR1);
			System.exit(1);
		}
	}

	private static void printHelp() {
		System.out.println("\nUsage:   java -jar ontology-semantic-diff.jar <file1> <file2> <output-dir> ["
				+ OPTION_SKIP_PROPERTIES + "<list,of,skipped,properties>] [" + OPTION_SKIP_ONTOLOGY + "]\n");
		System.out.println(
				"\nExample: java -jar ontology-semantic-diff.jar an_ontology.ttl another/ontology.owl output/files/with/differences "
						+ OPTION_SKIP_PROPERTIES
						+ "http://www.w3.org/ns/prov#wasDerivedFrom,http://purl.org/dc/terms/issued ["
						+ OPTION_SKIP_ONTOLOGY + "]\n");
	}

	/**
	 * Compares 2 Ontology files. Accepts Turtle, RDF/XML, ... anything
	 * autodetected by the Jena API
	 * 
	 * @param file1
	 *            an ontology file
	 * @param file2
	 *            another ontology file
	 * @param outputDir
	 *            Destination file for writing any differences found
	 * @param propertiesToSkip
	 *            Properties to be ignored in the diff
	 * @param skipOntology
	 *            Set to <code>true</code> to skip comparison of ontology
	 *            metadata
	 * @return <code>true</code> if the files were identical, <code>false</code>
	 *         otherwise or if an exception was caught
	 */
	public static boolean compareFiles(File file1, File file2, String outputDir, String[] propertiesToSkip,
			boolean skipOntology) {
		ensureDirectoryExists(new File(outputDir));

		if (!fileExists(file1)) {
			System.out.println("ERROR: File '" + file1 + "' does not exist");
			return false;
		}
		if (!fileExists(file2)) {
			System.out.println("ERROR: File '" + file2 + "' does not exist");
			return false;
		}

		// Compare ontology names
		try {
			Model model1 = loadModel(file1, outputDir);
			String firstOntologyName = getOntologyIRI(model1, file1);
			if (firstOntologyName == null) {
				return false;
			}

			Model model2 = loadModel(file2, outputDir);
			String secondOntologyName = getOntologyIRI(model2, file2);
			if (secondOntologyName == null) {
				return false;
			}

			System.out.println(VISUAL_SEPARATOR2);

			// Ontology names are clear, let's look at the contents in detail
			if (createDiffs(file1, file2, model1, model2, outputDir, propertiesToSkip, skipOntology)) {
				System.out.println("The two files are identical.");
				return true;
			}
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Gets the ontology IRI from the given file
	 * 
	 * @param model
	 *            Model loaded from file
	 * @param file
	 *            For messaging
	 * @return The Ontology IRI, or <code>null</code> if there was an error.
	 */
	private static String getOntologyIRI(Model model, File file) {
		List<Statement> statements = model.listStatements(null, RDF.type, OWL.Ontology).toList();
		if (statements.size() > 1) {
			System.out.println("ERROR: File '" + file + "' has multiple ontologies defined:");
			for (Statement ontology : statements) {
				System.out.println("\t- " + ontology.getSubject());
			}
			return null;
		} else if (statements.size() < 1) {
			System.out.println("ERROR: File '" + file + "' has no ontology defined.");
			return null;
		}
		String result = statements.iterator().next().getSubject().toString();
		System.out.println("File '" + file + "' contains ontology '" + result + "'");
		return result;
	}

	/**
	 * This is the heart of our test. Removes identical triples, then analyzes
	 * anonymous nodes and removes the ones that are semantically identical,
	 * irrespective of their random names. If differences are found, the first
	 * few triples are printed to console. The full diff is written into 2
	 * Turtle files.
	 * 
	 * @param file1
	 *            A file containing an ontology
	 * @param file2
	 *            Another file containing an ontology
	 * @param model1
	 *            The model from file1
	 * @param model2
	 *            The model from file2
	 * @param outputDir
	 *            Directory to write any diffs to
	 * @param propertiesToSkip
	 *            Properties to be ignored in the diff
	 * @param skipOntology
	 *            Set to <code>true</code> to skip comparison of ontology
	 *            metadata
	 * @return <code>true</code> if file contents were identical.
	 */
	private static boolean createDiffs(File file1, File file2, Model model1, Model model2, String outputDir,
			String[] propertiesToSkip, boolean skipOntology) {
		try {
			// Collect all objects so we can detect one-liners like foaf:mbox
			// and ensure that they are leaf nodes
			Set<String> objects1 = new HashSet<>();
			Set<String> objects2 = new HashSet<>();

			System.out.println("Processing " + model1.size() + " triples from file '" + file1.getName() + "' and "
					+ model2.size() + " triples from file '" + file2.getName() + "'.");

			// **** Axioms ****
			{
				// Bundle nodes with same subject so that we can group axioms
				ConcurrentHashMap<String, BundledNode> axioms1 = new ConcurrentHashMap<>();
				ConcurrentHashMap<String, BundledNode> axioms2 = new ConcurrentHashMap<>();

				// Delete identical triples and find axiom subjects
				System.out.println("Collecting interesting triples...");
				{
					int deleteCounter = 0;
					for (StmtIterator iterator = model1.listStatements(); iterator.hasNext();) {
						if (deleteCounter % PROGRESS_INTERVAL == 0) {
							System.out.print(".");
						}
						Statement statement = iterator.next();
						objects1.add(statement.getObject().toString());
						if (model2.contains(statement)) {
							// Remove identical triples
							model2.remove(statement);
							iterator.remove();
							deleteCounter++;
						} else {
							// Record subject for axiom
							if (statement.getObject().equals(org.apache.jena.vocabulary.OWL2.Axiom)) {
								axioms1.put(statement.getSubject().toString(), new BundledNode(statement));
							}
						}
					}
					for (Statement statement : model2.listStatements().toList()) {
						objects2.add(statement.getObject().toString());
						// Record subject for axiom
						if (statement.getObject().equals(org.apache.jena.vocabulary.OWL2.Axiom)) {
							axioms2.put(statement.getSubject().toString(), new BundledNode(statement));
						}
					}
					System.out.println();
					System.out.println("Removed " + deleteCounter
							+ (deleteCounter == 1 ? " identical triple." : " identical triples."));
				}

				System.out.println("Finding axioms...");
				fillAxioms(axioms1, model1);
				fillAxioms(axioms2, model2);

				System.out.println("Found " + axioms1.size() + " vs " + axioms2.size() + " axioms.");

				System.out.print("Comparing axioms..");
				compareAxioms(model1, model2, axioms1, axioms2);
				System.out.println();

			}

			// **** Restrictions ****
			{
				// Use tree structures to collect the anonymous nodes and their
				// relationships
				System.out.print("Finding restrictions..");
				Set<ChainedNode> restrictions1 = findRestrictions(model1);
				Set<ChainedNode> restrictions2 = findRestrictions(model2);
				System.out.println();
				System.out.println(
						"Found " + restrictions1.size() + " vs " + restrictions2.size() + " restriction triples.");

				// Compare the remaining nodes and remove identical ones
				System.out.print("Comparing restrictions..");
				compareRestrictions(model1, restrictions1, restrictions2);
				compareRestrictions(model2, restrictions2, restrictions1);
				System.out.println();
			}

			// **** One-liners ****
			{
				System.out.println("Finding one-liners...");
				Set<Statement> oneLiners1 = findOneLiners(model1, objects1);
				Set<Statement> oneLiners2 = findOneLiners(model2, objects2);
				System.out.println("Found " + oneLiners1.size() + " vs " + oneLiners2.size() + " one-liners.");

				System.out.println("Comparing one-liners...");
				compareOneLiners(model1, oneLiners1, oneLiners2);
				compareOneLiners(model2, oneLiners2, oneLiners1);
			}

			// **** Remove properties to skip ****

			if (propertiesToSkip != null) {
				for (int i = 0; i < propertiesToSkip.length; i++) {
					System.out.println("Ignoring property " + propertiesToSkip[i] + "...");
					Property skipme = model1.createProperty(propertiesToSkip[i]);
					model1.removeAll(null, skipme, null);
					model2.removeAll(null, skipme, null);
				}
			}

			// **** Remove Ontology metadata ****

			if (skipOntology) {
				String ontologyName = getOntologyIRI(model1, file1);
				System.out.println("Removing metadata for ontology " + ontologyName + "...");
				model1.getResource(ontologyName).removeProperties();

				ontologyName = getOntologyIRI(model2, file2);
				System.out.println("Removing metadata for ontology " + ontologyName + "...");
				model2.getResource(ontologyName).removeProperties();
			}

			// **** Report ****

			// Create return message if the two files are not identical and
			// write the differing triples to files
			if (!model1.isEmpty() || !model2.isEmpty()) {
				System.out.println(VISUAL_SEPARATOR1);
				System.out.println("The two files differ.");
				printSomeUniqueTriples(file1, model1);
				printSomeUniqueTriples(file2, model2);

				// Write diff files
				if (!model1.isEmpty()) {
					System.out.println(VISUAL_SEPARATOR2);
					File outputFile = new File(outputDir + "/1_" + file1.getName());
					if (!writeModel(outputFile, model1)) {
						System.out.println("ERROR writing to file: " + outputFile);
					}
				}
				if (!model2.isEmpty()) {
					System.out.println(VISUAL_SEPARATOR2);
					File outputFile = new File(outputDir + "/2_" + file2.getName());
					if (!writeModel(outputFile, model2)) {
						System.out.println("\nERROR writing to file: " + outputFile);
					}
				}
				System.out.println(VISUAL_SEPARATOR2);
				return false;
			} else {
				System.out.println(VISUAL_SEPARATOR2);
				return true;
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
			return false;
		}
	}

	// A node is considered anonymous if it doesn't start with "http"
	private static boolean nodeIsAnonymous(Statement statement) {
		return !statement.getSubject().toString().startsWith("http");
	}

	/**
	 * Find all restriction nodes
	 * 
	 * @param model
	 *            The model to parse
	 * @return All nodes parsed into ChainedNodes so they will know about their
	 *         relationships in the tree
	 */
	private static Set<ChainedNode> findRestrictions(Model model) {
		Set<ChainedNode> rootNodes = new HashSet<>();
		Set<ChainedNode> collectedNodes = new HashSet<>();

		// Bootstrap with non-anonymous subjects
		for (StmtIterator iterator = model.listStatements(); iterator.hasNext();) {
			Statement statement = iterator.next();
			if (!nodeIsAnonymous(statement)) {
				ChainedNode node = new ChainedNode(statement);
				collectedNodes.add(node);
				rootNodes.add(node);
				iterator.remove();
			}
		}

		// Now iterate through the subnodes
		boolean newTriplesFound;
		do {
			// Use counter to print visual feedback that the program is not dead
			// for huge ontologies
			int counter = 0;
			newTriplesFound = false;
			for (StmtIterator iterator = model.listStatements(); iterator.hasNext();) {
				if (counter++ % PROGRESS_INTERVAL == 0) {
					System.out.print(".");
				}

				Statement statement = iterator.next();
				// Sets can't handle concurrent modification, so we have to
				// collect them first and add them below
				List<ChainedNode> collectedThisRound = new ArrayList<>();
				// We need to use an iterator, because we modify the ChainedNode
				// objects
				for (Iterator<ChainedNode> it2 = collectedNodes.iterator(); it2.hasNext();) {
					ChainedNode currentNode = it2.next();
					if (currentNode.getStatement().getObject().toString().equals(statement.getSubject().toString())) {
						ChainedNode child = currentNode.addChild(statement);
						collectedThisRound.add(child);
						iterator.remove();
						newTriplesFound = true;
					}
				}
				for (ChainedNode node : collectedThisRound) {
					collectedNodes.add(node);
				}
			}
		} while (newTriplesFound);

		return collectedNodes;
	}

	/**
	 * Compares chained nodes and writes any that don't match back to the //
	 * statement set
	 * 
	 * @param model1
	 *            A model to write the non-matching statements to
	 * @param restrictions1
	 *            The restrictions belonging to model1
	 * @param restrictions2
	 *            Restrictions found in another model
	 */
	private static void compareRestrictions(Model model1, Set<ChainedNode> restrictions1,
			Set<ChainedNode> restrictions2) {
		// Use counter to print visual feedback that the program is not dead for
		// huge ontologies
		int counter = 0;
		for (ChainedNode node1 : restrictions1) {
			if (counter++ % PROGRESS_INTERVAL == 0) {
				System.out.print(".");
			}

			boolean matchFound = false;
			for (ChainedNode node2 : restrictions2) {
				if (node1.equals(node2)) {
					matchFound = true;
					break;
				}
			}

			if (!matchFound) {
				model1.add(node1.getStatement());
			}
		}
	}

	/**
	 * Find statements that belong to axioms
	 * 
	 * @param axioms
	 *            A list of already identified axiom subjects to add the
	 *            statements to
	 * @param model
	 *            The model to parse
	 */
	private static void fillAxioms(ConcurrentHashMap<String, BundledNode> axioms, Model model) {
		for (StmtIterator iterator = model.listStatements(); iterator.hasNext();) {
			Statement statement = iterator.next();
			String subject = statement.getSubject().toString();
			if (axioms.containsKey(subject)) {
				axioms.get(subject).addStatement(statement);
				iterator.remove();
			}
		}
	}

	/**
	 * Finds any mismatching axioms and writes them to the given models
	 * 
	 * @param model1
	 *            First model to write mismatches to
	 * @param model2
	 *            Second model to write mismatches to
	 * @param axioms1
	 *            Axioms that were identified for model1
	 * @param axioms2
	 *            Axioms that were identified for model2
	 */
	private static void compareAxioms(Model model1, Model model2, ConcurrentHashMap<String, BundledNode> axioms1,
			ConcurrentHashMap<String, BundledNode> axioms2) {
		// Use counter to print visual feedback that the program is not dead for
		// huge ontologies
		int counter = 0;
		for (Iterator<String> it1 = axioms1.keySet().iterator(); it1.hasNext();) {
			if (counter++ % PROGRESS_INTERVAL == 0) {
				System.out.print(".");
			}
			boolean matchFound = false;
			String key1 = it1.next();
			BundledNode node1 = axioms1.get(key1);
			for (Iterator<String> it2 = axioms2.keySet().iterator(); it2.hasNext();) {
				String key2 = it2.next();
				BundledNode node2 = axioms2.get(key2);
				if (node1.equals(node2)) {
					matchFound = true;
					it2.remove();
				}
			}

			if (matchFound) {
				it1.remove();
			} else {
				for (String key : node1.getStatements().keySet()) {
					model1.add(node1.getStatements().get(key));
				}
			}
		}

		for (BundledNode newNode : axioms2.values()) {
			if (counter++ % PROGRESS_INTERVAL == 0) {
				System.out.print(".");
			}
			for (String key : newNode.getStatements().keySet()) {
				model2.add(newNode.getStatements().get(key));
			}
		}
	}

	/**
	 * Finds one-liner statements like e.g.
	 * <code>[ foaf:mbox <mailto:somebody@somwhere.com> ] .</code>
	 * 
	 * @param model
	 *            The model to parse
	 * @param allObjects
	 *            List of all objects from the complete model, to ensure that
	 *            the one-liners' subjects are root nodes
	 * @return A set of identified one-liners
	 */
	private static Set<Statement> findOneLiners(Model model, Set<String> allObjects) {
		Set<Statement> result = new HashSet<>();
		ConcurrentHashMap<String, BundledNode> collected = new ConcurrentHashMap<>();
		for (StmtIterator iterator = model.listStatements(); iterator.hasNext();) {
			Statement statement = iterator.next();
			if (!nodeIsAnonymous(statement)) {
				continue;
			}
			String subject = statement.getSubject().toString();
			// If the subject or object is transitive, it's not a one-liner
			if (!allObjects.contains(subject)) {
				if (collected.containsKey(subject)) {
					collected.get(subject).addStatement(statement);
				} else {
					collected.put(subject, new BundledNode(statement));
				}
				iterator.remove();
			}
		}

		// If there are multiple entries for the same subject, they don't
		// qualify as one-liners
		for (Iterator<String> iterator = collected.keySet().iterator(); iterator.hasNext();) {
			String key = iterator.next();
			BundledNode node = collected.get(key);
			if (node.noOfStatements() == 1) {
				result.add(node.getStatements().values().iterator().next());
			} else {
				for (String key2 : node.getStatements().keySet()) {
					model.add(node.getStatements().get(key2));
				}
				iterator.remove();
			}
		}
		return result;
	}

	/**
	 * Compares two sets of one-liners and writes any differences to the model.
	 * A one-liner is e.g.
	 * <code>[ foaf:mbox <mailto:somebody@somwhere.com> ] .</code>
	 * 
	 * @param model1
	 *            The model to write the unique one-liners to
	 * @param oneLiners1
	 *            A set of one-liners belonging to model1
	 * @param oneLiners2
	 *            A set of one-liners belonging to a different model
	 */
	private static void compareOneLiners(Model model1, Set<Statement> oneLiners1, Set<Statement> oneLiners2) {
		for (Statement statement1 : oneLiners1) {
			boolean matchFound = false;
			for (Statement statement2 : oneLiners2) {
				if (statement1.getPredicate().equals(statement2.getPredicate())
						&& statement1.getObject().equals(statement2.getObject())) {
					matchFound = true;
					break;
				}
			}
			if (!matchFound) {
				model1.add(statement1);
			}
		}
	}

	// Helper function to check that a file exists
	private static boolean fileExists(File fileToCheck) {
		return fileToCheck.exists() && !fileToCheck.isDirectory();
	}

	// Helper function to make sure that a directory exists
	private static void ensureDirectoryExists(File fileToCheck) {
		if (!fileToCheck.exists()) {
			fileToCheck.mkdirs();
		}
	}

	// Print the first TRIPLES_TO_PRINT unique triples in the file
	private static void printSomeUniqueTriples(File file, Model model) {
		if (!model.isEmpty()) {
			int counter = 0;
			System.out.println(VISUAL_SEPARATOR2);
			System.out.println("'" + file.getName() + "' has " + model.size() + " unique triples, starting with:");
			for (Statement statement : model.listStatements().toList()) {
				System.out.println(
						"- " + statement.getSubject() + "  " + statement.getPredicate() + "  " + statement.getObject());
				if (counter++ > TRIPLES_TO_PRINT) {
					break;
				}
			}
		}
	}

	// Load model with sanitized Windows line endings from file. Using outputDir
	// for temporary files. Guesses the language.
	/**
	 * Loads a Model from the given file. If the file size is not huge, uses the
	 * outputDir to write a temporary file in order to sanitize Windows line
	 * endings.
	 * 
	 * @param sourceFile
	 *            The file to load the model from
	 * @param outputDir
	 *            Directory for temporary files
	 * @return A Model parsed from the file
	 * @throws IOException
	 *             When a problem with file reading/writing occurs
	 */
	private static Model loadModel(File sourceFile, String outputDir) throws IOException {
		System.out.println("Loading model for file '" + sourceFile.getName() + "'...");
		if (org.apache.commons.io.FileUtils.sizeOf(sourceFile) > 5000000) {
			// TextFileHandler uses too much memory for big files, so skip
			// sanitizing
			Model model = ModelFactory.createDefaultModel();
			model.read(new FileReader(sourceFile), "urn:x-base:default",
					FileUtils.guessLang(sourceFile.getName().toString()));
			return model;
		}
		File tempFile = new File(outputDir + File.separator + System.currentTimeMillis() + "_" + sourceFile.getName());
		try {
			// Windows line endings can cause differences to show up in
			// multiline strings, so we read and write as text first
			org.apache.commons.io.FileUtils.copyFile(sourceFile, tempFile);
			String fileContents = TextFileHandler.read(tempFile.getAbsolutePath());
			TextFileHandler.write(fileContents, tempFile.getAbsolutePath());

			// Now load the model
			Model model = ModelFactory.createDefaultModel();
			model.read(new FileReader(tempFile), "urn:x-base:default",
					FileUtils.guessLang(tempFile.getName().toString()));
			tempFile.delete();
			return model;
		} catch (Exception e) {
			tempFile.delete();
			System.out.println("Error reading model for file: " + sourceFile.getAbsolutePath());
			throw (e);
		}
	}

	/**
	 * Write the given model to Turtle
	 * 
	 * @param outputFile
	 *            The file to write to
	 * @param model
	 *            The model to write
	 * @return Whether writing the file succeeded
	 */
	private static boolean writeModel(File outputFile, Model model) {
		try {
			FileWriter writer = new FileWriter(outputFile);
			model.write(writer, FileUtils.langTurtle);
			writer.close();
			System.out.println("Unique triples written to " + outputFile);
			return true;
		} catch (IOException e2) {
			System.out.println("ERROR writing to file: " + outputFile);
			System.out.println(e2.getMessage());
			return false;
		}
	}
}
