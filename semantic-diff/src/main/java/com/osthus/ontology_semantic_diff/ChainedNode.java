package com.osthus.ontology_semantic_diff;

/*
 *    Copyright 2020-2021 OSTHUS
 *
 *      https://www.allotrope.org/
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import java.util.ArrayList;
import java.util.List;

import org.apache.jena.rdf.model.Statement;

/**
 * A Node in a chain of statements with anonymous subjects/objects, used to
 * compare anonymous nodes in restrictions etc.
 */
public class ChainedNode {
	private Statement statement;
	private List<ChainedNode> children;
	private int level;
	private String ancestorNames;

	/**
	 * Construct a root node
	 * 
	 * @param statement
	 *            The statement to construct the node from
	 */
	public ChainedNode(Statement statement) {
		this(statement.getSubject().toString() + "||" + statement.getPredicate().toString(), statement, 0);
	}

	// Child node constructor
	private ChainedNode(String ancestorNames, Statement statement, int level) {
		this.statement = statement;
		this.children = new ArrayList<>();
		this.level = level;
		this.ancestorNames = ancestorNames;
	}

	/**
	 * Construct and add a child node to this node
	 * 
	 * @param statement
	 *            The statement to construct the child node from
	 * @return The constructed child node
	 */
	ChainedNode addChild(Statement statement) {
		ChainedNode child = new ChainedNode(ancestorNames + "||" + statement.getPredicate().toString(), statement,
				level + 1);
		children.add(child);
		return child;
	}

	/**
	 * Counts this node's children
	 * 
	 * @return The number of direct children
	 */
	public int noOfChildren() {
		return children.size();
	}

	/**
	 * Get this node's children
	 * 
	 * @return A list of this node's direct children
	 */
	public List<ChainedNode> getChildren() {
		return children;
	}

	/**
	 * Get this node's statement
	 * 
	 * @return This node's statement
	 */
	public Statement getStatement() {
		return statement;
	}

	/**
	 * Print information about this node and all its descendants
	 */
	public void printTree() {
		System.out.println(this);
		for (ChainedNode info : children) {
			info.printTree();
		}
	}

	/**
	 * Because equality depends on children that are added dynamically, you can
	 * use this for uniqueness in collections, but not for comparison.
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ancestorNames == null) ? 0 : ancestorNames.hashCode());
		result = prime * result + ((children == null) ? 0 : children.hashCode());
		result = prime * result + level;
		result = prime * result + ((statement == null) ? 0 : statement.hashCode());
		return result;
	}

	/**
	 * Equality depends on the node's ancestors as well as its children
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ChainedNode other = (ChainedNode) obj;
		// If it's not on the same level, don't bother comparing anything else
		if (this.level != other.level) {
			return false;
		} else if (this.level == 0) {
			// Root nodes need to have equal subjects
			if (!this.statement.getSubject().equals(other.statement.getSubject())) {
				return false;
			}
		}
		if (this.noOfChildren() != other.noOfChildren()) {
			return false;
		} else if (children.isEmpty()) {
			// Leaf nodes need to have equal objects
			if (!this.statement.getObject().equals(other.statement.getObject())) {
				return false;
			}
		}

		// Chain of ancestor predicates + the root's subject needs to be equal
		if (!this.ancestorNames.equals(other.ancestorNames)) {
			return false;
		}

		// All children need to be equal
		List<ChainedNode> otherVisited = new ArrayList<>();
		for (ChainedNode thisChild : this.children) {
			boolean found = false;
			for (ChainedNode otherChild : other.children) {
				if (thisChild.equals(otherChild)) {
					found = true;
					otherVisited.add(otherChild);
					break;
				}
			}
			if (!found) {
				return false;
			}
		}
		for (ChainedNode otherChild : other.children) {
			if (!otherVisited.contains(otherChild)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public String toString() {
		return "L: " + level + " | S: " + statement.getSubject() + " | P: " + statement.getPredicate() + " | O: "
				+ statement.getObject() + " | Children: " + children.size();
	}
}
