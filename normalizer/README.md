# Ontology Normalizer

This tool will normalize a Turtle file to minimize merge conflicts.

See our [Devops Wiki](https://gitlab.com/allotrope-open-source/allotrope-devops/wikis/The-Ontology-Normalizer) for documentation.
