package com.osthus.ontology_normalizer;

/*
 *    Copyright 2017-2023 OSTHUS
 *
 *      https://www.osthus.com
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.protege.xmlcatalog.owlapi.XMLCatalogIRIMapper;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.formats.ManchesterSyntaxDocumentFormatFactory;
import org.semanticweb.owlapi.formats.OWLXMLDocumentFormatFactory;
import org.semanticweb.owlapi.formats.PrefixDocumentFormat;
import org.semanticweb.owlapi.formats.RDFXMLDocumentFormatFactory;
import org.semanticweb.owlapi.formats.TurtleDocumentFormatFactory;
import org.semanticweb.owlapi.io.FileDocumentSource;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLDocumentFormat;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyID;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.SetOntologyID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.osthus.ontology_qa_common.TextFileHandler;

public class Normalize {

	public static final Logger log = LoggerFactory.getLogger(Normalize.class);

	private static final String EXTENSION_OWL = "owl";
	private static final String EXTENSION_RDF = "rdf";
	private static final String EXTENSION_XML = "xml";
	private static final String EXTENSION_TTL = "ttl";
	private static final String EXTENSION_MAN = "man";

	public static final void main(String[] args) {
		System.out.println("Tool for normalizing or converting ontology files using the OWL API");
		// Safeguard against execution of random code in log4j in case one of
		// the dependencies should still use it
		System.setProperty("log4j.formatMsgNoLookups", "true");

		if (args.length < 1 || args.length > 2) {
			log.error("Usage: normalize <ontology> [" + EXTENSION_OWL + "|" + EXTENSION_RDF + "|" + EXTENSION_XML + "|"
					+ EXTENSION_TTL + "|" + EXTENSION_MAN + "]");
			System.exit(1);
		}
		String taxonomyFile = args[0];

		String outputFormatString = "";
		if (args.length > 1) {
			outputFormatString = args[1];
		} else {
			switch (FilenameUtils.getExtension(taxonomyFile)) {
			case EXTENSION_OWL:
				outputFormatString = EXTENSION_OWL;
				break;
			case EXTENSION_RDF:
				outputFormatString = EXTENSION_RDF;
				break;
			case EXTENSION_XML:
				outputFormatString = EXTENSION_XML;
				break;
			case EXTENSION_TTL:
				outputFormatString = EXTENSION_TTL;
				break;
			case EXTENSION_MAN:
				outputFormatString = EXTENSION_MAN;
				break;
			}
		}

		Normalize normalize = new Normalize();
		try {
			normalize.normalizeFile(taxonomyFile, outputFormatString);
		} catch (Exception e) {
			log.error("Exception thrown during normalization: ", e);
			System.exit(1);
		}
	}

	public void normalizeFile(String taxonomyFile, String outputFormatString) throws Exception {

		ByteArrayOutputStream baos = new ByteArrayOutputStream();

		String basePath = FilenameUtils.getFullPath(taxonomyFile);
		File catalogFile = new File(basePath + "catalog-v001.xml");

		// Get hold of an ontology manager
		OWLOntologyManager manager = OWLManager.createOWLOntologyManager();

		if (catalogFile.exists()) {
			// Set up a mapping, which maps the ontology to the document IRI
			// based on protege catalog file
			XMLCatalogIRIMapper catalogIRIMapper = new XMLCatalogIRIMapper(catalogFile);
			manager.getIRIMappers().add(catalogIRIMapper);
		} else {
			// Code to run without a catalog file
			log.warn("Normalizing file without Catalog");
		}

		// Create OWLOntology from manager with taxonomyFile
		OWLOntology ontology = manager.loadOntologyFromOntologyDocument(new FileDocumentSource(new File(taxonomyFile)));

		OWLDocumentFormat inputFormat = manager.getOntologyFormat(ontology);

		OWLDocumentFormat outputFormat = inputFormat;
		switch (outputFormatString) {
		case EXTENSION_OWL:
			OWLXMLDocumentFormatFactory owlxmlFormatF = new OWLXMLDocumentFormatFactory();
			outputFormat = owlxmlFormatF.createFormat();
			break;
		case EXTENSION_RDF:
		case EXTENSION_XML:
			RDFXMLDocumentFormatFactory rdfFormatF = new RDFXMLDocumentFormatFactory();
			outputFormat = rdfFormatF.createFormat();
			break;
		case EXTENSION_TTL:
			TurtleDocumentFormatFactory turtleFormat = new TurtleDocumentFormatFactory();
			outputFormat = turtleFormat.createFormat();
			break;
		case EXTENSION_MAN:
			ManchesterSyntaxDocumentFormatFactory manSyntaxFormatF = new ManchesterSyntaxDocumentFormatFactory();
			outputFormat = manSyntaxFormatF.createFormat();
			break;
		default:
			throw new RuntimeException("Unknown document output format: '" + outputFormatString + "'. Expected "
					+ EXTENSION_OWL + ", " + EXTENSION_RDF + ", " + EXTENSION_XML + ", " + EXTENSION_TTL + " or "
					+ EXTENSION_MAN + ".");
		}
		log.info("Input  format: " + inputFormat.getKey());
		log.info("Output format: " + outputFormat.getKey());

		String outputFilename = FilenameUtils.getFullPath(taxonomyFile) + FilenameUtils.getBaseName(taxonomyFile) + '.'
				+ outputFormatString;

		try {
			// Workaround for a bug where loadOntologyFromOntologyDocument
			// replaces the ontology ID (but not the version ID) with an
			// imported ontology's ID. This can happen e.g. when there are
			// circular imports
			OWLOntologyID id = ontology.getOntologyID();
			if (hasOptional(id.getVersionIRI())
					&& !id.getVersionIRI().toString().startsWith(id.getOntologyIRI().toString())) {
				log.warn("Ontology IRI                   " + id.getOntologyIRI().get() + " "
						+ "Does not match the version IRI " + id.getVersionIRI().get() + "\n"
						+ "Overwriting ontology IRI with version IRI");
				manager.applyChange(
						new SetOntologyID(ontology, new OWLOntologyID(id.getVersionIRI(), id.getVersionIRI())));
			}

			// Perform normalization/formatting
			if (inputFormat.isPrefixOWLDocumentFormat() && outputFormat.isPrefixOWLDocumentFormat()) {
				log.info("Copying prefixes");
				PrefixDocumentFormat prefixFormat = outputFormat.asPrefixOWLDocumentFormat();
				prefixFormat.copyPrefixesFrom(inputFormat.asPrefixOWLDocumentFormat());
				manager.saveOntology(ontology, prefixFormat, baos);
			} else {
				manager.saveOntology(ontology, outputFormat, baos);
			}
		} catch (Exception e) {
			log.error("Unable to normalize '" + taxonomyFile
					+ "'. If this is an OWLOntologyDocumentAlreadyExistsException, check for typos in the ontology or version IRI.",
					e);
			baos.close();
			throw e;
		}
		IOUtils.copy(new ByteArrayInputStream(baos.toByteArray()), new FileOutputStream(outputFilename));
		baos.close();

		// Remove empty prefix that gets injected sometimes
		if (outputFormat.isPrefixOWLDocumentFormat()) {
			List<String> lines = new ArrayList<String>();
			boolean needsSaving = false;

			TextFileHandler.read(lines, outputFilename, false);
			Iterator<String> linesIterator = lines.iterator();
			StringBuilder cleanFile = new StringBuilder();

			// Iterate only over the first few lines for efficiency
			for (int i = 0; (needsSaving || i < 10) && linesIterator.hasNext(); ++i) {
				String line = linesIterator.next();
				if (line.contains("@prefix : <http://www.semanticweb.org/owl/owlapi/turtle#> .")) {
					log.warn("Removing extraneous default prefix");
					needsSaving = true;
				} else {
					cleanFile.append(line).append("\r\n");
				}
			}

			if (needsSaving) {
				TextFileHandler.write(cleanFile.toString(), outputFilename);
			}
		}

		if (taxonomyFile.equals(outputFilename)) {
			log.info("Normalized " + taxonomyFile);

		} else {
			log.info("Converted " + taxonomyFile + " to " + outputFilename);
		}

	}

	private boolean hasOptional(java.util.Optional<IRI> optional) {
		return optional != null && optional.isPresent();
	}

}
