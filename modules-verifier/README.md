# Ontology Modules Verifier

This tool will walk the `<base_path>` and check all `*.xml` files for consistency with the `*.ttl` files and double-check the import structure.
Lists of files and directories to exclude from the checks can be provided.

See our [Devops Wiki](https://gitlab.com/allotrope-open-source/allotrope-devops/wikis/The-Ontology-Modules-Verifier) for documentation.
