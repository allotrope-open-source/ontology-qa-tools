package com.osthus.ontology_modules_verifier;

/*
 *    Copyright 2017-2021 OSTHUS
 *
 *      https://www.osthus.com
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import java.io.File;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.vocabulary.OWL;

import com.osthus.ontology_qa_common.ModulesHandler;
import com.osthus.ontology_qa_common.ModulesHandler.CatalogEntry;

public class Main {

	// For getting the ontology
	static final String RDF_TYPE_KEY = "http://www.w3.org/1999/02/22-rdf-syntax-ns#type";
	static final String IMPORTS_KEY = "http://www.w3.org/2002/07/owl#imports";

	private static int errors = 0;

	private static class ImportWithSource {
		public ImportWithSource(String ontology, String source) {
			super();
			this.ontology = ontology;
			this.source = source;
		}

		public final String ontology;
		public final String source;
	}

	public static void main(String[] args) {
		System.out.println(
				"This tool will check all catalog xml files for consistency with the .ttl files and double-check the import structure.");
		if (args.length != 1) {
			System.out.println("Usage: java -jar afo-modules-verifier.jar <base_path>");
			System.exit(1);
		}

		// Base path to search for catalog xml files and .ttl files
		String basePath = args[0];

		try {
			final Collection<CatalogEntry> catalogs = ModulesHandler.read(basePath, new HashSet<String>()).values();
			checkCatalogAndFileMappings(catalogs, ModulesHandler.collectTurtleFiles(new File(basePath)));
			verifyImports(catalogs);
		} catch (Exception e1) {
			e1.printStackTrace();
			handleError(e1.getMessage());
		}

		// Check error and warning states
		if (errors > 0) {
			System.out.println("Checking completed. Found " + errors + " error(s).");
		} else {
			System.out.println("Success.");
		}
		System.exit(errors > 0 ? 1 : 0);
	}

	/**
	 * This method will compare the given catalog entries and the given list of
	 * Turtle files. Generates errors if there is a mismatch between the files
	 * listed in the catalogs and the files present on the file system. Will also
	 * generate an error if the same purl has 2 different files assigned, or if a
	 * file's ontology name does not name the purl in the catalog, or if the same
	 * file has been assigned to 2 different groups. The "internal" and "import"
	 * groups are ignored.
	 * 
	 * @param catalogs         - information collected from all xml catalog files
	 * @param filenamesToCheck - filenames collected from the file system
	 */
	private static void checkCatalogAndFileMappings(Collection<CatalogEntry> catalogs,
			HashSet<String> filenamesToCheck) {
		System.out.println("Verifying mappings ...");
		// <ontology PURL, entry info>
		HashMap<String, ModulesHandler.CatalogEntry> handled = new HashMap<String, ModulesHandler.CatalogEntry>();

		for (ModulesHandler.CatalogEntry catalog : catalogs) {
			for (String name : catalog.namesAndIds.keySet()) {
				try {
					if (!(new File(catalog.ttlFile)).exists()) {
						handleError("File " + catalog.ttlFile + " in catalog " + catalog.catalogFile
								+ " does not exist for ID " + catalog.namesAndIds.get(name) + "!");
					} else {
						if (handled.containsKey(name)) {
							final ModulesHandler.CatalogEntry handledEntry = handled.get(name);
							final String handledTTL = handledEntry.ttlFile;
							final String catalogTTL = catalog.ttlFile;
							if (!isInternalOrImport(catalog) && handledTTL.equals(catalogTTL)
									&& !handledEntry.groupName.equals(catalog.groupName)) {
								handleError("The same file is in 2 different groups! We already had "
										+ handledEntry.groupName + " for " + handledTTL + " in catalog "
										+ handledEntry.catalogFile + ", but " + catalog.catalogFile + ", ID: "
										+ catalog.namesAndIds.get(name) + " has " + catalog.groupName + "!");
							}
							if (!handledTTL.equals(catalog.ttlFile)) {
								// Submissions can have the same ontology as their
								// non-submission counterparts
								if (!isInternalOrImport(catalog)) {
									handleError("File paths for PURL " + name + " do not match! We already had "
											+ handledTTL + ", but " + catalog.catalogFile + ", ID: "
											+ catalog.namesAndIds.get(name) + " has " + catalogTTL + "!");
								}
							} else {
								// Now check the ontology PURL in the file
								System.out.println("Verifying ontology in " + catalog.ttlFile);
								Model model = RDFDataMgr.loadModel(catalog.ttlFile);
								// Find the ontology's identifier subject
								List<Statement> statements = model.listStatements(null,
										ResourceFactory.createProperty(RDF_TYPE_KEY), OWL.Ontology).toList();

								// Test that we have exactly 1 ontology subject
								// and
								// that the purls are identical
								if (statements.size() == 1) {
									if (!isInternalOrImport(catalog)) {
										String resourceName = statements.get(0).getSubject().toString();
										if (resourceName.contains("domain"))
											System.out.println(resourceName);
										if (!resourceName.equals(name)) {
											handleError("File " + catalog.ttlFile + " is listed in catalog "
													+ catalog.catalogFile + " as having " + name
													+ " as its ontology purl for ID " + catalog.namesAndIds.get(name)
													+ ", but the actual ontology has " + resourceName + ".");
										}
									}
								} else {
									String errorMessage = "File needs to have exactly 1 ontology, but we found:";
									for (Statement statement : statements) {
										errorMessage += "\n" + statement.toString();
									}
									handleError(errorMessage);
								}
							}
						}
					}
				} catch (Exception e) {
					Throwable cause = e.getCause();
					if (cause != null) {
						System.out.println(cause.getMessage());
					} else {
						System.out.println(e.getMessage());
					}
					handleError("Error with model in file '" + catalog.ttlFile + "': " + e.getMessage());
					e.printStackTrace();
				}
				if (!isInternalOrImport(catalog)) {
					handled.put(name, catalog);
				}
			}
			// Record the handled entry
			filenamesToCheck.remove(catalog.ttlFile);

		} // catalogs

		// Generate errors for dangling files
		if (!filenamesToCheck.isEmpty()) {
			for (String filename : filenamesToCheck) {
				handleError("File is not included in any catalog.xml: " + filename + ".");
			}
		}
	}

	/**
	 * Recursively collects all files that are listed in the catalogs and makes sure
	 * that all imports are present, and that we have only 1 main ontology in the
	 * result. The "internal" and "import" groups are ignored.
	 * 
	 * @param catalogs catalog entries to parse
	 */
	private static void verifyImports(Collection<CatalogEntry> catalogs) {
		System.out.println("Verifying all imports ...");

		Queue<ImportWithSource> filesToParse = new LinkedList<ImportWithSource>();
		// <ontology PURL, ttl file path>
		HashMap<String, String> allOntologies = new HashMap<String, String>();
		for (ModulesHandler.CatalogEntry catalog : catalogs) {
			for (String name : catalog.namesAndIds.keySet()) {
				allOntologies.put(name, catalog.ttlFile);
			}
			// We do not want to parse template files, because they can cause
			// exceptions. Also, avoid duplicate files by skipping the imports.
			if (!isInternalOrImport(catalog)) {
				filesToParse.add(new ImportWithSource(catalog.ttlFile, catalog.catalogFile));
			}
		}

		while (!filesToParse.isEmpty()) {
			final ImportWithSource importedFile = filesToParse.remove();

			// Load the ontology
			Model model;
			try {
				model = RDFDataMgr.loadModel(importedFile.ontology);
			} catch (Exception e) {
				handleError(
						"Ontology " + importedFile.ontology + " does not exist. Imported from: " + importedFile.source);
				continue;
			}

			// Find the ontology's identifier subject
			List<Statement> statements = model
					.listStatements(null, ResourceFactory.createProperty(RDF_TYPE_KEY), OWL.Ontology).toList();

			// Test that we have exactly 1 subject
			String resourceName = "";
			if (statements.size() == 1) {
				resourceName = statements.get(0).getSubject().toString();
			} else {
				String errorMessage = "File needs to have exactly 1 ontology, but we found:";
				for (Statement statement : statements) {
					errorMessage += "\n" + statement.toString();
				}
				handleError(errorMessage);
			}

			// Now verify our ontology
			Resource ontology = model.getResource(resourceName);
			StmtIterator it = ontology.listProperties();
			HashSet<String> importPurls = new HashSet<String>();
			while (it.hasNext()) {
				Statement statement = it.nextStatement();
				if (statement.getPredicate().toString().equals(IMPORTS_KEY)) {
					importPurls.add(statement.getObject().toString());
				}
			}

			for (String purl : importPurls) {
				if (!allOntologies.containsKey(purl)) {
					handleError(
							"File " + importedFile.ontology + " imports " + purl + ", which is not in our catalogs!");
				}
			}
		}
	}

	public static void handleError(String message) {
		++errors;
		System.out.println("ERROR! " + message);
	}

	private static boolean isInternalOrImport(ModulesHandler.CatalogEntry entry) {
		return entry.groupName.equals("internal") || entry.groupName.equals("import");
	}
}
