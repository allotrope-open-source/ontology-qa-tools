# Ontology Missing Resources Detector

Ontology Missing Resources Detector is a tool to check whether triples that are included in a .ttl file are missing from another .ttl file. 
It will list the missing triples on the command line.

You can use this to make sure that your changes won't accidentally delete existing triples.

Execute from the command line:

    java -jar release/afo-mrd.jar <old>.ttl <new>.ttl

e.g.
`"C:\Program Files\Java\jdk1.8.0_45\bin\java" -jar release/afo-mrd.jar afo.ttl afo-new.ttl`
