package com.osthus.ontology_missing_resources_detector;

/*
 *    Copyright 2017-2018 OSTHUS
 *
 *      http://www.allotrope-framework-architect.com
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import java.io.File;
import java.util.HashSet;

import org.apache.jena.graph.Triple;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.riot.RDFDataMgr;

public class Main {
	/**
	 * This tool will check if all subjects of the first .ttl file exist in the
	 * second .ttl file.
	 * 
	 * @param args
	 *            <path-to-ttl1> <path-to-ttl2>
	 */
	public static void main(String[] args) {
		boolean success = true;
		String oldAFOFile = "";
		String newAFOFile = "";

		if (args.length != 2) {
			System.out.println(
					"This tool will check if all subjects of the first .ttl file exist in the second .ttl file.");
			System.out.println("Usage: <path-to-ttl1> <path-to-ttl2>");
			System.exit(1);
		} else {
			oldAFOFile = args[0];
			newAFOFile = args[1];
			File f = new File(oldAFOFile);
			if (!f.exists() || f.isDirectory()) {
				System.out.println("ERROR! File " + oldAFOFile + " does not exist!");
				System.exit(1);
			}
			f = new File(newAFOFile);
			if (!f.exists() || f.isDirectory()) {
				System.out.println("ERROR! File " + newAFOFile + " does not exist!");
				System.exit(1);
			}
		}
		System.out.println("Checking for subjects missing from " + newAFOFile + ":");

		// Collect the contents of the new file.
		HashSet<String> subjectSet = new HashSet<String>();
		for (Statement statement : RDFDataMgr.loadModel(newAFOFile).listStatements().toList()) {
			String subject = statement.asTriple().getSubject().toString();
			if (subject.startsWith("http://purl")) {
				subjectSet.add(subject);
			}
		}

		// Print error for any subjects in the old file that are not in the new
		// file.
		for (Statement statement : RDFDataMgr.loadModel(oldAFOFile).listStatements().toList()) {
			Triple triple = statement.asTriple();
			String subject = triple.getSubject().toString();
			if (subject.startsWith("http://purl") && !subjectSet.contains(subject)) {
				System.out.println("ERROR! Missing triple: " + triple.toString());
				success = false;
			}
		}
		System.out.println("Done.");
		System.exit(success ? 0 : 1);
	}
}
