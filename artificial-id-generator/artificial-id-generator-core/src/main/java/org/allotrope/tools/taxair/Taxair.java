package org.allotrope.tools.taxair;

/*
 *    Copyright 2015-2021 OSTHUS
 *
 *      https://www.osthus.com
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.SimpleSelector;
import com.hp.hpl.jena.rdf.model.Statement;

public class Taxair {

	private static final String NAMED_INDIVIDUAL = "http://www.w3.org/2002/07/owl#NamedIndividual";
	private static final String DATATYPE_PROPERTY = "http://www.w3.org/2002/07/owl#DatatypeProperty";
	private static final String OBJECT_PROPERTY = "http://www.w3.org/2002/07/owl#ObjectProperty";
	private static final String ANNOTATION_PROPERTY = "http://www.w3.org/2002/07/owl#AnnotationProperty";
	private static final String CLASS = "http://www.w3.org/2002/07/owl#Class";

	private static final String RDF_TYPE = "http://www.w3.org/1999/02/22-rdf-syntax-ns#type";

	protected String outputDir;

	protected List<String> objects = new ArrayList<String>();
	{
		objects.add(NAMED_INDIVIDUAL);
		objects.add(DATATYPE_PROPERTY);
		objects.add(OBJECT_PROPERTY);
		objects.add(ANNOTATION_PROPERTY);
		objects.add(CLASS);
	}

	protected Map<String, Taxonomy> nsToTaxonomyMap = new HashMap<>();
//    {
//	nsToTaxonomyMap.put("http://purl.allotrope.org/ontologies/common#", new Taxonomy("AFC", "work/af-common.ttl"));
//	nsToTaxonomyMap.put("http://purl.allotrope.org/ontologies/equipment#", new Taxonomy("AFE", "work/af-equipment.ttl"));
//	nsToTaxonomyMap.put("http://purl.allotrope.org/ontologies/material#", new Taxonomy("AFM", "work/af-material.ttl"));
//	nsToTaxonomyMap.put("http://purl.allotrope.org/ontologies/process#", new Taxonomy("AFP", "work/af-process.ttl"));
//	nsToTaxonomyMap.put("http://purl.allotrope.org/ontologies/result#", new Taxonomy("AFR", "work/af-result.ttl"));
//    }

	public static void main(String[] args) {
		Taxair taxair = new Taxair();
		// AFC=work/af-common.ttl
		for (String arg : args) {
			String[] argSplit = arg.split("=");
			if (argSplit.length != 2) {
				throw new IllegalArgumentException(
						"Cannot parse argument: " + arg + ". Args should be provided as PREFIX=TTL-FILE");
			}
			String key = argSplit[0];
			String value = argSplit[1];

			if (key.equalsIgnoreCase("DIR")) {
				taxair.setOutputDir(value);
			} else {
				Taxonomy taxonomy = new Taxonomy(key, value);
				taxair.addTaxonomy(taxonomy);
			}
		}

		taxair.replaceArtificialIDs();
	}

	public void addTaxonomy(Taxonomy taxonomy) {
		this.nsToTaxonomyMap.put(taxonomy.getBaseNamespace(), taxonomy);
	}

	public void setOutputDir(String outputDir) {
		this.outputDir = outputDir.endsWith("\\") || outputDir.endsWith("/") ? outputDir : outputDir + "/";
		File f = new File(this.outputDir);
		f.mkdirs();
	}

	private void replaceArtificialIDs() {
		Model masterModel = ModelFactory.createDefaultModel();
		nsToTaxonomyMap.values().stream().forEach(m -> masterModel.add(m.getModel()));

		readRelevantSubjectsIntoTaxonomies(masterModel);

		nsToTaxonomyMap.values().stream().forEach(Taxonomy::serializeIdentifiers);

		SearchReplaceEngine sre = new SearchReplaceEngine();
		sre.runSearchReplaceOnTaxonomyFiles(nsToTaxonomyMap.values(), this.outputDir);
	}

	private void readRelevantSubjectsIntoTaxonomies(Model model) {
		SimpleSelector ss = new SimpleSelector(null, null, (RDFNode) null) {
			@Override
			public boolean selects(Statement s) {
				return nsToTaxonomyMap.keySet().contains(s.getSubject().toString().split("#")[0] + "#")
						&& s.getPredicate().toString().equals(RDF_TYPE) && objects.contains(s.getObject().toString());
			}
		};

		model.listStatements(ss).forEachRemaining(s -> {
			/*
			 * 
			 * Due to a bug in RDF Jena 2.11.2, which would e.g.
			 * http://purl.allotrope.org/ontologies/process#
			 * 7Li_NuclearMagneticResonanceSpectroscopy
			 * 
			 * subject.getNameSpace() --> http://purl.allotrope.org/ontologies/process#7
			 * subject.getLocalName() --> Li_NuclearMagneticResonanceSpectroscopy
			 * 
			 * we cannot use subject.getNamespace() but instead have to parse the namespace
			 * manually
			 * 
			 * 
			 */
			String[] split = s.getSubject().getURI().split("#", 2);
			String ns = split[0] + "#";
			String localName = split[1];
			Taxonomy namespace = nsToTaxonomyMap.get(ns);

			if (namespace == null) {
				System.out.println("WARNING: Namespace not found: " + s.getSubject());
			} else {
				namespace.add(ns + localName);
			}
		});
	}
}
