package org.allotrope.tools.taxair;

/*
 *    Copyright 2015-2021 OSTHUS
 *
 *      https://www.osthus.com
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.apache.commons.collections4.BidiMap;
import org.apache.commons.collections4.bidimap.DualHashBidiMap;
import org.apache.commons.io.FilenameUtils;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;

public class Taxonomy {

	private static final int ID_DIGITS = 7;

	private String prefix;
	private String inputFileName;
	private String mappingFile;
	private String workingDir;
	private int counter;

	private Map<String, String> idMap = new HashMap<>();

	private BidiMap<String, String> prefixes;// = new DualHashBidiMap<>();
//    private Map<String, String> prefixes = new HashMap<>();

	private Model model;

	public Taxonomy(String prefix, String inputFileName) {
		super();
		this.prefix = prefix;
		this.inputFileName = inputFileName;
		this.workingDir = FilenameUtils.getPath(inputFileName);
		this.mappingFile = workingDir + ""
				+ (FilenameUtils.getBaseName(inputFileName) + "_" + prefix + ".properties").toLowerCase();
		this.counter = 1;
		this.model = ModelFactory.createDefaultModel().read(inputFileName);
		initPrefixes();
		initExistingMappings();
	}

	/**
	 * read existing mapping file
	 */
	private void initExistingMappings() {
		Properties existingIdMappings = new Properties();
		File existingIdMappingFile = new File(this.mappingFile);
		if (existingIdMappingFile.exists()) {
			try {
				existingIdMappings.load(new FileInputStream(existingIdMappingFile));
				for (Entry<Object, Object> e : existingIdMappings.entrySet()) {
//		    System.out.println("add existing: " + e.getKey() + " = " + e.getValue());
					this.idMap.put((String) e.getValue(), (String) e.getKey());
					counter++;
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private void initPrefixes() {
		this.prefixes = new DualHashBidiMap<>(this.model.getNsPrefixMap());
//	this.prefixes.entrySet().forEach(e->{
//	    System.out.println(e.getKey() + " = " + e.getValue());	    
//	});
	}

	public String getBaseNamespace() {
		return this.model.getNsPrefixURI("");
	}

	public void setModel(Model model) {
		this.model = model;
	}

	public Model getModel() {
		return model;
	}

	public void addPrefix() {

	}

	public String getInputFileName() {
		return inputFileName;
	}

	public void setCounter(int counter) {
		this.counter = counter;
	}

	public Map<String, String> getIdMap() {
		return idMap;
	}

	public void add(String localName) {
		if (!idMap.containsKey(localName)) {
			System.out.println("add new: " + localName + " = " + this.prefix + "_"
					+ String.format("%0" + ID_DIGITS + "d", this.counter));
			idMap.put(localName, this.prefix + "_" + String.format("%0" + ID_DIGITS + "d", this.counter));
			this.counter++;
		}
	}

	public void serializeIdentifiers() {

		Map<String, String> map = idMap.entrySet().stream()
				.collect(Collectors.toMap(Map.Entry::getValue, Map.Entry::getKey));

		if (idMap.size() != map.size()) {
			throw new RuntimeException("Reverse Map not equal in size!");
		}

		SortedSet<String> sortedIds = new TreeSet<>(map.keySet());
		try (FileWriter fw = new FileWriter(new File(mappingFile))) {
			sortedIds.stream().forEachOrdered(id -> {
				try {
					fw.write(id + "=" + map.get(id) + "\r\n");
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			});
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public String getNsURIPrefix(String namespace) {
		return this.prefixes.getKey(namespace);
	}

}
