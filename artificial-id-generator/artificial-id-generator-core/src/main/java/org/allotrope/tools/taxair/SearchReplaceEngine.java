package org.allotrope.tools.taxair;

/*
 *    Copyright 2015-2021 OSTHUS
 *
 *      https://www.osthus.com
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

public class SearchReplaceEngine {

	public void runSearchReplaceOnTaxonomyFiles(Collection<Taxonomy> taxonomies, String outputDir) {
		Map<String, String> unifiedVirtualIDMaps = unifyVirtualIDMaps(taxonomies);
		runSearchReplaceOnSubjects(taxonomies, unifiedVirtualIDMaps, outputDir);
	}

	private Map<String, String> unifyVirtualIDMaps(Collection<Taxonomy> taxonomies) {
		Map<String, String> unifiedMap = new HashMap<>();
		taxonomies.stream().forEach(t -> {
			unifiedMap.putAll(t.getIdMap());
		});
		return unifiedMap;
	}

	private void runSearchReplaceOnSubjects(Collection<Taxonomy> taxonomies, Map<String, String> unifiedVirtualIDMaps,
			String outputDir) {
		long start = System.currentTimeMillis();
		// for each taxonomy file
		taxonomies.parallelStream().forEach(t -> {
			long startSingle = System.currentTimeMillis();
			System.out.println("Start with replacing " + t.getBaseNamespace() + "");
			try (BufferedReader br = new BufferedReader(new FileReader(new File(t.getInputFileName())));
					BufferedWriter bw = new BufferedWriter(
							new FileWriter(outputDir + new File(FilenameUtils.getName(t.getInputFileName()))))) {
				String content = readFile(t.getInputFileName());
				String replacedContent = replaceIdsInFileContent(t, content, unifiedVirtualIDMaps);
				bw.write(replacedContent);
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
			System.out.println(
					"Done with " + t.getBaseNamespace() + " [" + (System.currentTimeMillis() - startSingle) + "ms]");
			// break;
		});
		System.out.println("Done replacing IDs [" + ((System.currentTimeMillis() - start) / 1000) + "s]");
	}

	private String readFile(String filePath) throws IOException {
		long start = System.currentTimeMillis();
		try {
			return FileUtils.readFileToString(new File(filePath));
		} finally {
			System.out.println("Reading file " + filePath + " took " + (System.currentTimeMillis() - start) + "ms");
		}
	}

	private List<PatternCollection> precompilePatterns(Taxonomy taxonomy, Map<String, String> unifiedVirtualIDMaps) {
		List<PatternCollection> patternCollections = new ArrayList<>();
		for (Entry<String, String> e : unifiedVirtualIDMaps.entrySet()) {
			PatternCollection pc = new PatternCollection();
			pc.namespace = e.getKey().split("#", 2)[0] + "#";
			pc.localName = e.getKey().split("#", 2)[1];
			pc.newLocalName = e.getValue();
			pc.nsShort = taxonomy.getNsURIPrefix(pc.namespace);
			pc.precompilePatterns();
			patternCollections.add(pc);
		}
		return patternCollections;
	}

	private String replaceIdsInFileContent(Taxonomy taxonomy, String fileContent,
			Map<String, String> unifiedVirtualIDMaps) {
		List<PatternCollection> precompiledPatterns = precompilePatterns(taxonomy, unifiedVirtualIDMaps);
		for (PatternCollection pc : precompiledPatterns) {
			String replacement = pc.nsShort + ":" + pc.newLocalName;
			if (pc.nsShort == null) {
				replacement = "<" + pc.namespace + pc.newLocalName + ">";
			}

			if (pc.nsShort != null) {
				// @prefix used
				fileContent = pc.patternShortNs.matcher(fileContent).replaceAll(replacement);
			}

			if (pc.namespace.equals(taxonomy.getBaseNamespace())) {
				// base prefix used
				fileContent = pc.patternBaseNs.matcher(fileContent).replaceAll(replacement);
			}

			// no prefix used (conventional/long non-ns typing)
			fileContent = pc.patternNoNs.matcher(fileContent).replaceAll(replacement);
		}
		return fileContent;
	}

	private static class PatternCollection {
		public String namespace;
		public String localName;
		public String newLocalName;
		public String nsShort;

		public Pattern patternShortNs;
		public Pattern patternBaseNs;
		public Pattern patternNoNs;

		public void precompilePatterns() {
			String regexShortNs = "\\b" + Pattern.quote(this.nsShort + ":" + this.localName) + "\\b";
			// String regexBaseNs = "\\b" + Pattern.quote(":" + this.localName)
			// + "\\b";
			String regexBaseNs = "(?<!\\w)" + Pattern.quote(":" + this.localName) + "\\b";
			String regexNoNS = Pattern.quote("<" + this.namespace + this.localName + ">");

			this.patternShortNs = Pattern.compile(regexShortNs);
			this.patternBaseNs = Pattern.compile(regexBaseNs);
			this.patternNoNs = Pattern.compile(regexNoNS);
		}
	}
}
