package com.osthus.aft;

/*
 *    Copyright 2015-2021 OSTHUS
 *
 *      https://www.osthus.com
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AFTGeneratorServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final ReentrantReadWriteLock lock = new ReentrantReadWriteLock();

	private static final int ID_DIGITS = 7;
	private String counterFolder;

	@Override
	public void init() throws ServletException {
		super.init();
		counterFolder = getServletContext().getInitParameter("counter.folder");
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String servletPath = req.getServletPath();

		if (servletPath == null || servletPath.length() == 0) {
			resp.getOutputStream().write("No taxonomy defined!".getBytes());
			return;
		}

		String[] pathParts = servletPath.trim().split("/");
		if (pathParts == null || pathParts.length == 0) {
			resp.getOutputStream().write("Please append a taxonomy name to the URL".getBytes());
			return;
		}

		String taxonomy = pathParts[pathParts.length - 1];

		int newId = doGenerateId(taxonomy.toLowerCase(), resp);
		if (newId != -1) {
			String sNewId = taxonomy.toUpperCase() + "_" + String.format("%0" + ID_DIGITS + "d", newId);
			resp.getOutputStream().write(sNewId.getBytes());
		}
	}

	public int doGenerateId(String taxonomy, HttpServletResponse resp) throws IOException {
		lock.writeLock().lock();
		try {

			if (counterFolder == null) {
				resp.getOutputStream().write("Context parameter 'counter.folder' not set!".getBytes());
				return -1;
			}

			File fCounterDir = new File(counterFolder);
			if (!fCounterDir.exists()) {
				resp.getOutputStream().write(("Creating new directory '" + Paths.get("").toAbsolutePath().toString()
						+ "/" + counterFolder + "'\n").getBytes());
				boolean dirCreated = fCounterDir.mkdirs();
				if (!dirCreated) {
					resp.getOutputStream().write(("Failed creating directory '" + counterFolder
							+ "' in working directory: " + Paths.get("").toAbsolutePath().toString()).getBytes());
					return -1;
				}
			}

			String filename = counterFolder + "/" + taxonomy + ".counter";
			File counterFile = new File(filename);
			if (!counterFile.exists()) {
				try (FileWriter fw = new FileWriter(counterFile)) {
					fw.write("0");
				}
			}
			Integer newId;
			try (BufferedReader fr = new BufferedReader(new FileReader(counterFile))) {
				String sId = fr.readLine();
				newId = Integer.parseInt(sId) + 1;
			}

			try (FileWriter fw = new FileWriter(counterFile)) {
				fw.write(newId.toString());
			}

			return newId;
		} finally {
			lock.writeLock().unlock();
		}
	}
}
