# ID Generator

Tools for sequential IDs in AFT & AFO IRIs

In `artificial-id-generator-web`, there's a Java Servlet to run an ID generator on a webserver that can be run in Apache Tomcat and then called via `http://<your_internet_domain>:8080/<tool_path>/<prefix>`.

In `artificial-id-generator-core`, there's a WIP Java application that will perform some automated ID replacement.

## Configuration of `artificial-id-generator-web`

The `artificial-id-generator-web` needs a directory defined where the latest ID is recorded in the file system.

1. Run mvn install to create a `.war` file
2. Unzip the `.war` file
3. Edit `WEB-INF/web.xml` to include the following lines:

```
    <context-param>
        <param-name>counter.folder</param-name>
        <param-value>path_to_counters_directory_here</param-value>
    </context-param>
```

The program will create the directory automatically if it doesn't exist, and then write into a `<path_to_counters_directory_here>/<prefix>.counter` file when the address `<domain>/<ig.generator-installation>/<prefix>` is called in the browser, then display the next ID. On directory creation, the absolute path to `<path_to_counters_directory_here>` is also shown in the browser
