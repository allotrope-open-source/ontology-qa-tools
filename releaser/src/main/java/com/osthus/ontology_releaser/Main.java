package com.osthus.ontology_releaser;

/*
 *    Copyright 2017-2018 OSTHUS
 *
 *      http://www.allotrope-framework-architect.com
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.DirectoryIteratorException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.StringTokenizer;
import java.util.TimeZone;
import java.util.Vector;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.jena.datatypes.xsd.XSDDateTime;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.vocabulary.OWL;

import com.osthus.ontology_qa_common.ModulesHandler;
import com.osthus.ontology_qa_common.StringPair;
import com.osthus.ontology_qa_common.TextFileHandler;
import com.osthus.ontology_qa_common.TripleInfo;
import com.osthus.ontology_qa_common.TriplesCollector;

public class Main {

	// The keys for accessing the information that we want to update.
	// The current version, to be incremented
	static final String VERSION_INFO_KEY = "http://www.w3.org/2002/07/owl#versionInfo";
	// Also the current version
	static final String VERSION_IRI_KEY = "http://www.w3.org/2002/07/owl#versionIRI";
	// The previous version, to be replaced with the current version
	static final String PRIOR_VERSION_KEY = "http://www.w3.org/2002/07/owl#priorVersion";
	// Change date. Change this to now
	static final String MODIFIED_DATE_KEY = "http://purl.org/dc/terms/modified";
	// For getting the ontology
	static final String RDF_TYPE_KEY = "http://www.w3.org/1999/02/22-rdf-syntax-ns#type";

	static Options options;

	public static void main(String[] args) {
		System.out.println("========================");
		System.out.println("This is the AFO-Releaser");
		System.out.println("========================");

		options = new Options();

		Option inputOption = new Option("i", "input", true, "input path, relative to base path");
		inputOption.setRequired(false);
		options.addOption(inputOption);

		Option outputOption = new Option("o", "output", true, "output path, relative to base path");
		outputOption.setRequired(false);
		options.addOption(outputOption);

		Option basePathOption = new Option("p", "path", true, "base path");
		basePathOption.setRequired(true);
		options.addOption(basePathOption);

		Option versionOption = new Option("v", "version", true, "new version, e.g. \"2.5.3\"");
		versionOption.setRequired(false);
		options.addOption(versionOption);

		Option excludeOption = new Option("x", "excludes", true, "Path to text file with directories to exclude");
		excludeOption.setRequired(false);
		options.addOption(excludeOption);

		// Input - Output file pairs
		HashMap<File, File> files = new HashMap<File, File>();

		CommandLineParser parser = new DefaultParser();
		CommandLine cmd;
		try {
			cmd = parser.parse(options, args);
			final String basePath = cmd.getOptionValue("p");
			File outputPath = null;
			final String rawOutputPathname = basePath + "/" + (cmd.hasOption("o") ? cmd.getOptionValue("o") : "");

			try {
				outputPath = new File(rawOutputPathname).getCanonicalFile();
			} catch (IOException e2) {
				e2.printStackTrace();
				System.out.println("Invalid output path: " + rawOutputPathname);
			}

			// Walk input directory recursively and pick up all .ttl files
			if (cmd.hasOption("i")) {
				Queue<File> dirsToWalk = new LinkedList<File>();
				HashSet<String> dirsToExclude = new HashSet<String>();
				String inputPath = "";
				try {
					inputPath = new File(basePath + "/" + cmd.getOptionValue("i")).getCanonicalPath();
					dirsToWalk.add(new File(inputPath).getCanonicalFile());
					if (cmd.hasOption("x")) {
						final String dirsToExcludeFile = cmd.getOptionValue("x");
						checkFileExists(dirsToExcludeFile);
						TextFileHandler.read(dirsToExclude, dirsToExcludeFile);
					}
				} catch (IOException e1) {
					e1.printStackTrace();
					handleError("Input path is not valid: " + inputPath);
				}

				while (!dirsToWalk.isEmpty()) {
					File currentDir = dirsToWalk.remove();
					try (DirectoryStream<Path> stream = Files
							.newDirectoryStream(Paths.get(currentDir.getCanonicalPath()))) {
						for (Path path : stream) {
							File file = path.toFile();
							if (file.isDirectory()) {
								boolean addThisDir = true;
								for (String excludeme : dirsToExclude) {
									if (file.getCanonicalPath().endsWith(excludeme)) {
										addThisDir = false;
										break;
									}
								}
								if (addThisDir) {
									dirsToWalk.add(file);
									System.out.println("Adding directory: " + file);
								}
							} else {
								final String filename = file.getCanonicalPath().toString();
								if (filename.endsWith(".ttl")) {
									files.put(file, createOutputFile(inputPath, outputPath, filename));
								}
							}
						}
					} catch (IOException | DirectoryIteratorException e) {
						e.printStackTrace();
						handleError(e.getMessage());
					}
				}
				System.out.println("Done getting the paths");
				if (cmd.getArgs().length > 0) {
					handleError("You can't specify individual filenames when you have used the -i option.");
				}
			} else {
				String[] remainingArgs = cmd.getArgs();
				for (int i = 0; i < remainingArgs.length; ++i) {
					final String filename = remainingArgs[i];
					try {
						files.put(new File(filename), createOutputFile(basePath, outputPath, filename));
					} catch (IOException e) {
						e.printStackTrace();
						handleError("Unable to create output file for " + filename + " in " + outputPath
								+ " starting from " + basePath);
					}
				}
			}

			HashSet<String> groupsToExclude = new HashSet<String>();
			// TODO read from JSON spec
			groupsToExclude.add("external");
			groupsToExclude.add("internal");
			groupsToExclude.add("imports");

			final String ontologyPath = new File(basePath + "/" + cmd.getOptionValue("i")).getCanonicalPath();
			// Read catalogs and squash duplicates to save time
			HashMap<String, StringPair> catalogs = new HashMap<String, StringPair>();
			for (ModulesHandler.CatalogEntry catalog : ModulesHandler.read(ontologyPath, groupsToExclude)) {
				catalogs.put(catalog.ttlFile, new StringPair(catalog.name, catalog.groupName));
			}
			for (String filename : catalogs.keySet()) {
				checkFileExists(filename);
			}

			final Vector<TripleInfo> tripleInfos = TriplesCollector.getAllTriples(catalogs, ontologyPath);
			HashMap<String, String> filenamesOntologies = new HashMap<String, String>();
			for (TripleInfo tripleInfo : tripleInfos) {
				filenamesOntologies.put(tripleInfo.filename, tripleInfo.ontology);
			}

			for (String test : filenamesOntologies.keySet()) {
				System.out.println(test + " - " + filenamesOntologies.get(test));
			}

			if (files.isEmpty()) {
				handleError("Please specify a list of files or an input path");
			}

			Main.VersionInfo versionInfo = new Main.VersionInfo(cmd.hasOption("v") ? cmd.getOptionValue("v") : "");

			for (File file : files.keySet()) {
				String filename = file.getName();
				String ontologyName = "";
				for (String key : filenamesOntologies.keySet()) {
					if (file.getCanonicalPath().endsWith(key)) {
						ontologyName = filenamesOntologies.get(key);
					}
				}
				if (ontologyName.isEmpty()) {
					System.out.println("Unable to find ontology for " + file.getCanonicalPath());
					continue;
				}

				if (!filename.endsWith(".ttl")) {
					handleError("Only .ttl files are supported; " + filename + " is not valid.");
				}
				versionInfo = updateFile(ontologyName, file, files.get(file), versionInfo);
			}
		} catch (Exception e) {
			e.printStackTrace();
			handleError(e.getMessage());
			return;
		}

		System.out.println("Done.");
	}

	// Helper function to get a statement from a model's ontology for the given
	// property key
	private static Statement getStatement(Model model, Resource ontology, String key) {
		final Property property = model.createProperty(key);
		if (!ontology.hasProperty(property)) {
			ontology.addLiteral(property, "");
		}
		return ontology.getProperty(property);
	}

	// Helper function to print information about the triple identified by a
	// property key
	private static void printTriple(Model model, Resource ontology, String key) {
		System.out.println(ontology.getProperty(model.createProperty(key)).asTriple().toString());
	}

	/**
	 * Container for validating and persisting the version information to write
	 * across files
	 */
	private static class VersionInfo {
		public VersionInfo(String newVersion) {
			super();
			this.currentVersion = "";
			this.newVersion = newVersion;
		}

		public String getCurrentVersion() {
			return currentVersion;
		}

		public String getNewVersion() {
			return newVersion;
		}

		// Split version string by .\"
		private static String[] parsedVersionString(String versionString) {
			StringTokenizer versionTokenizer = new StringTokenizer(versionString, ".\"");
			String[] version = new String[versionTokenizer.countTokens()];
			for (int i = 0; i < version.length; ++i) {
				version[i] = versionTokenizer.nextToken();
			}
			return version;
		}

		// Increment the lowest number in the version string
		private static String bumpVersion(String oldVersion) {
			String[] parsedVersion = parsedVersionString(oldVersion);
			int minorVersion = Integer.parseInt(parsedVersion[parsedVersion.length - 1]);
			parsedVersion[parsedVersion.length - 1] = Integer.toString(++minorVersion);
			return versionArrayToString(parsedVersion);
		}

		// Turns string with version info into version string ready for writing
		private static String versionArrayToString(String[] version) {
			String result = "";
			for (int i = 0; i < version.length; ++i) {
				result += version[i] + (i < version.length - 1 ? "." : "");
			}
			return result;
		}

		/**
		 * Exists with an error if this.newVersion is not a valid increment of
		 * this.currentVersion.
		 */
		private void validateVersions() {
			System.out.println("Validating new version \"" + this.newVersion + "\" against current version "
					+ this.currentVersion + ".");

			final String[] currentVersionArray = parsedVersionString(this.currentVersion);
			final String[] newVersionArray = parsedVersionString(this.newVersion);

			if (currentVersionArray.length != newVersionArray.length) {
				handleError(this.newVersion + " is not a valid updated version for " + this.currentVersion);
			}

			boolean wasIncremented = false;
			for (int i = 0; i < currentVersionArray.length; ++i) {
				Integer previous = new Integer(currentVersionArray[i]);
				Integer current = new Integer(newVersionArray[i]);
				if (previous < current) {
					wasIncremented = true;
				} else if (previous > current) {
					handleError("The new version " + this.newVersion + " is smaller than the old version "
							+ this.currentVersion);
				}
			}
			if (!wasIncremented) {
				handleError("The new version " + this.newVersion + " has to be biggger than the old version "
						+ this.currentVersion);
			}
			System.out.println("Version \"" + this.newVersion + "\" is valid.");
		}

		/**
		 * Sets the current version. If newVersion is empty, sets it to
		 * currentVersion with the smallest number incremented.
		 * 
		 * @param currentVersion
		 *            a .-separated version info string
		 */
		public void setVersion(String currentVersion) {
			this.currentVersion = currentVersion;
			if (this.newVersion.isEmpty()) {
				this.newVersion = bumpVersion(this.currentVersion);
			}
			validateVersions();
		}

		private String currentVersion;
		private String newVersion;
	}

	/**
	 * 
	 * Parse the ontology in the given filename and update its release
	 * information. Version numbering will be determined for the first file and
	 * then used identically for the remaining files. If the version wasn't
	 * specified on the command line, it will be bumped automatically.
	 * 
	 * @param filename
	 *            path to the file to update
	 * @param basePath
	 *            path to place the output file in
	 * @param versionInfo
	 *            a versionInfo object to write the prior and current versions.
	 * @return versionInfo with new version information added if it was
	 *         partially empty
	 */
	private static VersionInfo updateFile(String ontologyName, File inputFile, File outputFile, VersionInfo versionInfo) {
		checkFileExists(inputFile.getAbsolutePath());
		checkFileExists(outputFile.getAbsolutePath());
		System.out.println("Updating release info for file: " + inputFile.getAbsolutePath());

		// Load the ontology
		Model model = RDFDataMgr.loadModel(inputFile.getAbsolutePath());

		// Find the ontology's identifier subject
		List<Statement> statements = model
				.listStatements(null, ResourceFactory.createProperty(RDF_TYPE_KEY), OWL.Ontology).toList();

		// Test that we have exactly 1 subject
		String resourceName = "";
		if (statements.size() == 1) {
			resourceName = statements.get(0).getSubject().toString();
		} else {
			String errorMessage = "File needs to have exactly 1 ontology, but we found:";
			for (Statement statement : statements) {
				errorMessage += "\n" + statement.toString();
			}
			handleError(errorMessage);
		}

		// Now update our resources
		Resource ontology = model.getResource(resourceName);

		// Parse version
		if (versionInfo.getCurrentVersion().isEmpty()) {
			if (!ontology.hasProperty(model.createProperty(VERSION_INFO_KEY))) {
				handleError("No " + VERSION_INFO_KEY + " to parse!");
			}
			versionInfo.setVersion(getStatement(model, ontology, VERSION_INFO_KEY).asTriple().getObject().toString());
		}

		Statement statement;
		System.out.println("Changing triples:");

		// New prior version
		statement = getStatement(model, ontology, PRIOR_VERSION_KEY);
		printTriple(model, ontology, PRIOR_VERSION_KEY);
		statement.changeObject(ResourceFactory
				.createResource(ontologyName + "/" + versionInfo.getCurrentVersion().replaceAll("\"", "")));
		printTriple(model, ontology, PRIOR_VERSION_KEY);

		// New current version
		statement = getStatement(model, ontology, VERSION_INFO_KEY);
		printTriple(model, ontology, VERSION_INFO_KEY);
		statement.changeObject(versionInfo.getNewVersion());
		printTriple(model, ontology, VERSION_INFO_KEY);

		// New current version IRI
		statement = getStatement(model, ontology, VERSION_IRI_KEY);
		printTriple(model, ontology, VERSION_IRI_KEY);
		statement.changeObject(ResourceFactory.createResource(ontologyName + "/" + versionInfo.getNewVersion()));
		printTriple(model, ontology, VERSION_IRI_KEY);

		// Dates
		final Literal currentDatetime = ResourceFactory
				.createTypedLiteral(new XSDDateTime(Calendar.getInstance(TimeZone.getTimeZone("GMT"))));
		statement = getStatement(model, ontology, MODIFIED_DATE_KEY);
		printTriple(model, ontology, MODIFIED_DATE_KEY);
		statement.changeObject(currentDatetime);
		printTriple(model, ontology, MODIFIED_DATE_KEY);

		try {
			File parentDir = outputFile.getParentFile();
			if (!parentDir.exists()) {
				if (!parentDir.mkdirs()) {
					handleError("Unable to create output dir: " + parentDir);
				}
			}
			System.out.println("Writing result to: " + outputFile);
			RDFDataMgr.write(new FileOutputStream(outputFile.getAbsolutePath()), model, Lang.TURTLE);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			handleError(e.getMessage());
		}
		System.out.println("---------------------------------------------------------");
		return versionInfo;
	}

	// Helper function to make sure that a file exists
	private static void checkFileExists(String fileToCheck) {
		File f = new File(fileToCheck);
		if (!f.exists() || f.isDirectory()) {
			handleError("File " + fileToCheck + " does not exist!");
		}
	}

	/**
	 * Print error message and commandline help, then exit 1.
	 * 
	 * @param message
	 *            the error message to display
	 */
	public static void handleError(String message) {
		final String helpInfo = "java -jar afo-releaser.jar -p basepath [-i inputpath] [-o outputpath] [-v version] [-x excludes] [FILES]"
				+ "\n\tFILES is a list of .ttl files, relative to the base path. Use this option only if you haven't used -i"
				+ "\n\tIf you have specified -i, all subdirectories except for those listed in the file specified by -x will be searched"
				+ "Version information will be added to the change note automatically.\n\n";

		System.out.println("ERROR! " + message);
		new HelpFormatter().printHelp(helpInfo, options);
		System.exit(1);
	}

	private static File createOutputFile(String inputPath, File outputPath, final String filename) throws IOException {
		final String outputFilename = filename.substring(inputPath.length(), filename.length());
		return new File(outputPath + outputFilename).getCanonicalFile();
	}
}
