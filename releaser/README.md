# Ontology Releaser

This tool will update the change notes, date and version tags on a list of `.ttl` files,
and also update the prior release notes working file.

Run `java -jar afo-releaser.jar` for more documentation and a list of options.