package com.osthus.ontology_catalog_reader;

/*
 *    Copyright 2017-2021 OSTHUS
 *
 *      https://www.osthus.com
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import java.util.HashSet;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.Vector;

import com.osthus.ontology_qa_common.ModulesHandler;
import com.osthus.ontology_qa_common.ModulesHandler.CatalogEntry;

public class Main {

	private static void handle_error() {
		System.out.println(
				"Usage: java -jar afo-catalog-reader.jar <base_path> <output-file-path> [noheaders] [headers=<list,of,headers>] [exclude=<list,of,groups,to,exclude>]");
		System.out.println("- noheaders: header row will not be printed to the result file.");
		System.out.println("- headers:   list of desired column headers. If omitted, all columns will be included.");
		System.out.println("             Valid headers are: catalogFile, groupName, id, name, and ttlFile.");
		System.out.println("- exclude:   list of groups to exclude. If omitted, all groups will be included.");
		System.exit(1);
	}

	/**
	 * Tool to read catalog xml files into a tsv file.
	 * 
	 * @param args
	 *            &lt;base_path&gt; &lt;output-file-path&gt; [noheaders]
	 *            [headers=&lt;list,of,headers&gt;]
	 *            [exclude=&lt;list,of,groups,to,exclude&gt;]
	 */
	public static void main(String[] args) {
		System.out.println(
				"This tool will read all catalog xml files from the given directory and its subpaths and write the result to a .tsv file.");
		if (args.length < 2) {
			handle_error();
		}

		// Base path to search for catalog xml files
		String base_path = args[0];
		String output_file = args[1];
		int remaining_args_start_at = 2;

		// Will we print the headers?
		boolean print_headers = true;
		if (args.length > 2 && args[2].equals("noheaders")) {
			System.out.println("Do not print headers");
			print_headers = false;
			remaining_args_start_at++;
		}

		// Initialize headers
		Vector<String> headers = new Vector<String>();
		if (args.length > remaining_args_start_at && args[remaining_args_start_at].startsWith("headers=")) {
			final String headerlist = args[remaining_args_start_at]
					.substring(args[remaining_args_start_at].indexOf("=") + 1);
			StringTokenizer headerTokenzier = new StringTokenizer(headerlist, ",");
			while (headerTokenzier.hasMoreTokens()) {
				headers.add(headerTokenzier.nextToken());
			}
			System.out.println("Including headers: " + headers);
			remaining_args_start_at++;
		}

		// Initialize groups
		HashSet<String> groups_to_exclude = new HashSet<String>();
		if (args.length > remaining_args_start_at && args[remaining_args_start_at].startsWith("exclude=")) {
			final String excludelist = args[remaining_args_start_at]
					.substring(args[remaining_args_start_at].indexOf("=") + 1);
			StringTokenizer tokenzier = new StringTokenizer(excludelist, ",");
			while (tokenzier.hasMoreTokens()) {
				groups_to_exclude.add(tokenzier.nextToken());
			}
			System.out.println("Excluding groups: " + groups_to_exclude);
			remaining_args_start_at++;
		}

		// Now read and write
		try {
			final Map<String, CatalogEntry> catalogs = ModulesHandler.read(base_path, groups_to_exclude);
			ModulesHandler.writeToTSV(catalogs.values(), headers, print_headers, output_file);
		} catch (Exception e) {
			e.printStackTrace();
			handle_error();
		}
	}
}
