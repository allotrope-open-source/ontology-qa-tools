# Ontology Catalog Reader

This tool will read catalog xml files and print the output to a tsv file.

See our [Devops Wiki](https://gitlab.com/allotrope-open-source/allotrope-devops/wikis/The-Ontology-Catalog-Reader) for documentation.
