# Ontology QA Tools - Python - normalize-catalogs

Normalizes all catalog files in the given source directory and its subdirectories, and writes the result to the target directory.

- Ensures that there are no duplicate groups
- Ensures that there are no duplicate ids
- Canonicalizes the document

Usage: `python normalize-catalogs.py <source directory> <target directory>`
