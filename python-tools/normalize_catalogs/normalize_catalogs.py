# Copyright 2017-2018 OSTHUS
#
#     http://www.allotrope-framework-architect.com
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Utility to normalize xml catalog files.

from collections import defaultdict
import sys
import os.path
import xml.dom.minidom


def normalize_catalog(source_file, destination_file):
    """Make sure that this catalog has unique ids, then write it to the
    destination_file."""

    print('Parsing ' + source_file)

    DOMTree = xml.dom.minidom.parse(source_file)
    catalog = DOMTree.documentElement

    # Collect all groups
    group_dict = defaultdict(list)
    for group in catalog.getElementsByTagName('group'):
        if not group.hasAttribute('id'):
            print('ERROR: There is a group without ID in ' +
                  source_file + '\nAborting.')
            sys.exit(1)
        else:
            group_dict[group.getAttribute('id')].append(group)

    # Merge any duplicate groups
    for groupid in group_dict:
        group_list = group_dict[groupid]
        if len(group_list) > 1:
            print("Merging duplicate group '" +
                  group_list[0].getAttribute('id') + "':")
            merge_target = group_list.pop()
            while len(group_list) > 0:
                merge_source = group_list.pop()
                for uri in merge_source.getElementsByTagName('uri'):
                    print('- Shifting ' + uri.getAttribute('id'))
                    merge_source.removeChild(uri)
                    merge_target.appendChild(uri)

    # Now write to file
    with open(destination_file, 'w') as f:
        f.write('<?xml version="1.0" encoding="UTF-8" standalone="no"?>\n')

        # Python default modules don't have canonicalization, and unlinking the empty groups didn't work anyway.
        # So, we do this ourselves...
        # We also assign the unique IDs here to save us a loop
        counter = 1
        f.write(
            '<catalog prefer="public" xmlns="urn:oasis:names:tc:entity:xmlns:xml:catalog">\n')
        for group in catalog.getElementsByTagName('group'):
            uris = group.getElementsByTagName('uri')
            if len(uris) > 0:
                f.write('    <group id="%s" prefer="public" xml:base="">\n' %
                        group.getAttribute('id'))
                for uri in uris:
                    uri.setAttribute('id', 'uri%d' % counter)
                    counter = counter + 1
                    f.write('        <uri id="%s" name="%s" uri="%s"/>\n' %
                            (uri.getAttribute('id'), uri.getAttribute('name'), uri.getAttribute('uri')))
                f.write('    </group>\n')
        f.write('</catalog>\n')


# Main script

print('Normalizing the catalog files.')

if len(sys.argv) != 3:
    print("You need to specify the input and output paths, e.g. 'utils/afo-qa-tools/python-tools/normalize-catalogs.py afo-taxonomy test'")
    sys.exit(1)

# Setting up the paths to read and create
source_path = os.path.abspath(sys.argv[1])
destination_path = os.path.abspath(sys.argv[2])

print('The files will be written to ' + destination_path)

source_prefix_length = len(source_path)
for current_dir, subdirs, files in os.walk(source_path):
    for file in files:
        if file.endswith('.xml'):
            destination_dir = destination_path + \
                current_dir[source_prefix_length:]
            if not os.path.exists(destination_dir):
                os.mkdir(destination_dir)
            normalize_catalog(os.path.join(current_dir, file),
                              os.path.join(destination_dir, file))

print('Done.')
