#!/usr/bin/python

# Copyright 2017-2021 OSTHUS
#
#     https://www.osthus.com
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Utility to print the first few lines of a text file.

import codecs
import sys

if len(sys.argv) != 4:
    print('Usage: python print-lines.py <filename> <number_of_lines> <encoding>')
    sys.exit(1)

filename = sys.argv[1]
no_of_lines = int(sys.argv[2])
encoding = sys.argv[3]

counter = 0
with codecs.open(filename, mode='r', encoding=encoding) as file:
    for line in file:
        print(line.replace('\n', ''))
        counter = counter + 1
        if counter >= no_of_lines:
            break
