#!/usr/bin/python3

# Copyright 2017-2021 OSTHUS
#
#     https://www.osthus.com
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Utility to convert a SPARQL report's JSON query results to TSV.
# Will exit with 1 on parse errors.

import json
import sys

if len(sys.argv) < 3:
    print('Usage: python json2tsv.py <json_input_file> <tsv_output_file>')
    sys.exit(1)


print('\n############# Convert JSON to TSV #############\n')

# Set up filenames
input_file = sys.argv[1]
output_file = sys.argv[2]

print('Loading JSON from %s' % input_file)

# Load the JSON
try:
    with open(input_file, 'r', encoding='utf-8') as f:
        data = json.load(f)
except ValueError:
    print('ERROR in JSON file!\nFile contents:')
    with open(input_file, 'r', encoding='utf-8') as f:
        for line in f:
            print(line)
    sys.exit(1)

print('Writing TSV to %s ' % output_file)

# Grab the header and results
head = data['head']['vars']
bindings = data['results']['bindings']

# Now write it. We use UTF-16 so that Excel can read it.
with open(output_file, 'w', encoding='utf-16') as f:
    # Write header
    f.write('\t'.join(head))
    f.write('\n')

    # Write entries
    for result in bindings:
        line = []
        for key in head:
            entry = ''
            if key in result:
                entry = result[key]['value'].replace(
                    '\r\n', ' ').replace(
                    '\n', ' ').replace('\t', '    ')
            line.append(entry)
        f.write('\t'.join(line))
        f.write('\n')

print('Done writing release report.')
