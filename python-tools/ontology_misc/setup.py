#!/usr/bin/python

"""Ontology Misc package setup."""

import os
from setuptools import setup

# Utility function to read the README file.
# Used for the long_description.  It's nice, because now 1) we have a top level
# README file and 2) it's easier to type in the README file than to put a raw
# string in below ...

def read(fname):
    """Read file."""
    with open(os.path.join(os.path.dirname(__file__), fname)) as file:
        return file.read()


setup(
    name = "ontologymisc",
    version = "0.0.2",
    author = "OSTHUS",
    author_email = "softwarebuildbot@allotrope.org",
    description = ("Miscellaneous ontology QA helper tools"),
    license = "License :: OSI Approved :: Apache Software License",
    keywords = "QA ontology",
    url = "https://allotrope.jfrog.io/allotrope/api/pypi/libs-release-python/simple/ontologymisc/",
    packages=['ontology_misc'],
    long_description=read('README.md'),
    classifiers=[
        "Development Status :: 5 - Production/Stable",
        "Environment :: Console",
        "License :: OSI Approved :: Apache Software License",
        "Programming Language :: Python :: 3 :: Only",
    ],
)
