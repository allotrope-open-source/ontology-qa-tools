# Ontology QA Tools - Python - ontology_misc

## json2tsv.py

Will convert the JSON result delivered by ´fetch_sparql_report.py` into TSV format.

Usage: `python json2tsv.py <json_input_file> <tsv_output_file>`

## print_lines.py

Prints the specified amount of lines at the start of a given plain text file.

Usage: `python print-lines.py <filename> <number_of_lines> <file_encoding>`

Example: `python print-lines.py foo.txt 15 utf-8`
