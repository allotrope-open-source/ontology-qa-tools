# Ontology QA Tools - Python - duplicates_report

Detects whether the same entity has multiple definitions, or multiple entities have the same prefLabel.

Needs cURL and [Apache Jena Fuseki](https://jena.apache.org/documentation/fuseki2/index.html).
