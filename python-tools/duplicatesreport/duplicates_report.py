#!/usr/bin/python3

# Copyright 2017-2023 OSTHUS
#
#     https://www.osthus.com
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""This tool will generate a TSV duplicates report using SPARQL and a Fuseki
server. Will exit with 1 on parse errors or if any duplicates are present.

In order to run this function, a running :doc:`Apache Fuseki Server <https://gitlab.com/allotrope-open-source/allotrope-devops/wikis/Running-Apache-Fuseki>` needs to be present.

Usage: python afo_duplicates_report.py <ontology_dir> <output_path> <host> <dataset> <query_file> [<TSV_CATALOG_FILE> [<comma,separated,excludes>]]

If no list of files is given, all files are used.
"""

import json
import os.path
import sys
from fuseki.fuseki_common import get_fuseki_file_list
from fuseki.fetch_sparql_report import fetch_sparql_report, get_catalog_groups_for_graphs
from fuseki.fill_fuseki_store import fill_fuseki_store

if len(sys.argv) < 6 or len(sys.argv) > 8:
    print(
        'Usage: python afo_duplicates_report.py <ontology_dir> <output_path> <host> <dataset> <query_file> [<TSV_CATALOG_FILE> [<comma,separated,excludes>]]'
    )
    print('       If no list of files is given, all files are used.')
    sys.stdout.flush()
    sys.exit(1)

print('\n############# Creating Duplicates Report #############\n')

# Set up paths
ontology_dir = sys.argv[1]
output_path = sys.argv[2]

# Set up SPARQL parameters and fill the datastore with everything
host = sys.argv[3]
datastore = sys.argv[4]
query_file = sys.argv[5]

# Optional parameters
TSV_CATALOG_FILE = ''
excludes = []
if len(sys.argv) > 6:
    TSV_CATALOG_FILE = sys.argv[6]

    if len(sys.argv) > 7:
        # Parse catalog groups to exclude
        excludes = sys.argv[7].strip().split(',')
        for exclude in excludes:
            print('Excluding group %s' % exclude)

# Get the data
file_list = get_fuseki_file_list(ontology_dir, TSV_CATALOG_FILE, excludes)
fill_fuseki_store(host, datastore, file_list)
graph_prefix = fetch_sparql_report(host, datastore, output_path, query_file,
                                   file_list, 'json')

# Set up filenames
filename_base = os.path.splitext(os.path.basename(query_file))[0]
input_file = os.path.join(output_path, filename_base + '.json')
output_file = os.path.join(output_path, filename_base + '.tsv')

print('Loading duplicates report from JSON: %s' % input_file)

# Load the JSON
try:
    with open(input_file, 'r', encoding='utf-8') as f:
        data = json.load(f)
except ValueError:
    print('ERROR in JSON file!\nFile contents:')
    with open(input_file, 'r', encoding='utf-8') as f:
        for line in f:
            print(line)
    sys.exit(1)

# Record the groups from catalog, since we're not getting the full filenames from SPARQL
group_list = get_catalog_groups_for_graphs(TSV_CATALOG_FILE, excludes)

print('Searching for duplicates')

# Grab the header and results
head = data['head']['vars']
bindings = data['results']['bindings']

# Generate entries to check


class Entry:
    """Entries to check for duplicates."""

    def __init__(self):
        self.graph = ''
        self.entity = ''
        self.label = ''
        self.definition = ''
        self.group = ''

    def __eq__(self, other):
        return self.entity == other.entity and self.label == other.label and self.definition == other.definition and self.graph == other.graph

    def is_duplicate(self, other):
        return self.entity == other.entity and (
            self.label == other.label or self.definition
            == other.definition) and self.graph == other.graph

    def cleaned_up_graph(self):
        return self.graph[len(graph_prefix) + 1:]

    def __str__(self):
        return self.entity + '\t' + self.label + '\t' + self.definition + '\t' + self.cleaned_up_graph(
        ) + '\t' + self.group


entries_to_check = []
for result in bindings:
    entry_for_checks = Entry()
    for key in head:
        entry = ''
        if key in result:
            entry = result[key]['value']
            # Collect data for final analysis
            if key == 'graph':
                entry_for_checks.graph = entry.replace('\n', ' ').replace(
                    '\t', '    ')
                if entry_for_checks.cleaned_up_graph() in group_list:
                    entry_for_checks.group = ', '.join(
                        group_list[entry_for_checks.cleaned_up_graph()])
            elif key == 'entity':
                entry_for_checks.entity = entry.replace('\n', ' ').replace(
                    '\t', '    ')
            elif key == 'label':
                entry_for_checks.label = entry.replace('\n', ' ').replace(
                    '\t', '    ')
            elif key == 'definition':
                entry_for_checks.definition = entry.replace('\n', ' ').replace(
                    '\t', '    ')
    entries_to_check.append(entry_for_checks)

# Now search for duplicates
duplicates = []
duplicates_counter = 0
while len(entries_to_check) > 0:
    entry = entries_to_check.pop()
    # Do not check an identical entry twice
    for compareme in entries_to_check:
        if entry == compareme:
            entries_to_check.remove(compareme)
    # Now check if we have a duplicate
    for compareme in entries_to_check:
        if entry.is_duplicate(compareme):
            duplicates_counter += 1
            duplicates.append(entry)
            duplicates.append(compareme)
            continue

# Parse and write duplicates report
# We use UTF-16 so that Excel can read it.
with open(output_file, 'w', encoding='utf-16') as f:
    if duplicates_counter == 0:
        f.write('No duplicates found :)')
    else:
        # Write header
        f.write('Entity\tprefLabel\tDefinition\tGraph\tMaturity\n')
        for duplicate in duplicates:
            f.write(str(duplicate) + '\n')

print('Duplicates report written to %s ' % output_file)

# Now exit according to whether we found any duplicates
if duplicates_counter > 0:
    print('Error! Found %d duplicates!' % duplicates_counter)
    sys.exit(1)
else:
    print('Result is clean; no duplicates found :)')
