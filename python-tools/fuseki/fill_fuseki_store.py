#!/usr/bin/python3

# Copyright 2017-2023 OSTHUS
#
#     https://www.osthus.com
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Fill an Apache Fuseki Store with graphs from Turtle files."""

import os.path
import sys

import requests


def fill_fuseki_store(host, dataset, file_list):
    """This function will add the given files to a Fuseki triple store.

    In order to run this function, a running :doc:`Apache Fuseki Server
    <https://gitlab.com/allotrope-open-source/allotrope-devops/wikis/Running-Apache-Fuseki>`
    needs to be present.


    Parameters
    ----------
    host : string
        Your Fuseki server's host address, e.g. 'localhost'
    dataset : string
        The name of your Fuseki server's datastore, e.g. 'ontology'
    file_list : a list of file paths to add to the fuseki store.
        Must not be empty.
    """

    if not file_list:
        print('ERROR: File list is empty!')
        sys.exit(1)

    # We need an admin password in the CI pipeline
    # to authenticate with the Fuseki service
    admin_password = os.getenv('ADMIN_PASSWORD', '')

    # Set up SPARQL parameters
    sparql_endpoint = 'http://' + host + ':3030/' + dataset
    sparql_base = sparql_endpoint + '/data'

    print(
        f'\n############# Filling Fuseki Store at {sparql_endpoint} #############\n'
    )
    sys.stdout.flush()

    # Create named graphs for all files
    errors = []
    for file in file_list:
        graph_name = file.replace(':', '').replace('\\', '/')
        graph_name = os.path.splitext(graph_name)[0]

        named_graph = sparql_base + '/' + graph_name
        print(f'Creating: {named_graph}\nFor file: {file}')
        sys.stdout.flush()

        with open(file, 'rb') as ttl_file:
            data = ttl_file.read()
            headers = {
                'Content-Type': 'text/turtle;charset=utf-8',
                'graph': graph_name
            }
            params = [('graph', named_graph)]

            try:
                if admin_password:
                    response = requests.post(sparql_endpoint,
                                             params=params,
                                             data=data,
                                             headers=headers,
                                             timeout=1000,
                                             auth=('admin', admin_password))
                else:
                    response = requests.post(sparql_endpoint,
                                             params=params,
                                             data=data,
                                             headers=headers,
                                             timeout=1000)

                response.raise_for_status()

                output_json = response.json()
                if 'count' in output_json:
                    print(f"Loaded {output_json['count']} items")
                else:
                    print(response.text)
            except requests.exceptions.RequestException as request_error:
                errors.append(
                    f'Error loading graph: {graph_name}\n{request_error}')

        sys.stdout.flush()

    sys.stdout.flush()
    if errors:
        print('Found ', len(errors), 'errors:')
        for error in errors:
            print(error)
        sys.exit(1)
    print('Done filling Fuseki store.')
