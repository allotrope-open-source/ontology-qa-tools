#!/usr/bin/python3

# Copyright 2017-2023 OSTHUS
#
#     https://www.osthus.com
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Common functions for use by Fuseki package."""

import os.path
import json
import sys


def _process_file_path(ontology_dir, ontology_absdir, file_path):
    """Turn absolute path into relative path."""
    file_path = os.path.abspath(file_path)
    if file_path.startswith(ontology_absdir):
        file_path = file_path.replace(ontology_absdir, ontology_dir)
    return file_path


def get_fuseki_file_list(ontology_dir, catalog_file, excludes):
    """This function will return a set of .ttl files.

    Parameters
    ----------
    ontology_dir : string
        The path to your ontology's Turtle files
    catalog_file : string
        This can be TSV, JSON or empty
        * A TSV representation of the catalog files. Expected column headers are
          * 'ttlFile' for the Turtle files
          * 'groupName' for the groups they belong to
          Additional column headers will be ignored.
        * JSON just lists the relative file paths. Format:
          { "files" : [ "file1.ttl", "file2.ttl" ] }
        * Use an empty string to simply process all files from the ontology_dir.
    excludes : string[]
        If catalog_file is in TSV format, list of groups to exclude from the results.
        Can be empty.
    """

    print('\n############# Fetching file list #############\n')
    sys.stdout.flush()
    ontology_absdir = os.path.abspath(ontology_dir)
    print('Directory:', ontology_absdir)

    # Get the source filenames
    file_list = set()
    if catalog_file:
        if catalog_file.lower().endswith('.tsv'):
            print(f'Reading files from catalog {catalog_file}')
            with open(catalog_file, 'r', encoding='utf-8') as catalog_tsv:
                group_index = -1
                filename_index = -1
                for line_count, file_info in enumerate(catalog_tsv):
                    file_info_array = file_info.strip().split('\t')
                    # Find out which columns to use
                    if line_count == 0:
                        for header_count, header in enumerate(file_info_array):
                            if header == 'groupName':
                                group_index = header_count
                            elif header == 'ttlFile':
                                filename_index = header_count
                    # Add new entries that are not listed as excludes
                    elif file_info_array[group_index] not in excludes:
                        file_list.add(
                            _process_file_path(
                                ontology_dir, ontology_absdir,
                                file_info_array[filename_index]))
        elif catalog_file.lower().endswith('.json'):
            print('Reading file list from JSON:', catalog_file)
            if excludes:
                print(
                    "The 'excludes' parameter is not used when parsing JSON, ignoring:",
                    excludes)
            with open(catalog_file, 'r', encoding='utf-8') as catalog_json:
                json_object = json.load(catalog_json)
                if 'files' not in json_object:
                    print(
                        'JSON key \'files\' not found. '
                        'Expected format: { "files" : [ "file1.ttl", "file2.ttl" ] } '
                    )
                    sys.exit(1)
                for filename in json_object['files']:
                    file_list.add(
                        _process_file_path(
                            ontology_dir, ontology_absdir,
                            os.path.join(ontology_dir, filename)))
        else:
            print('ERROR: Only tsv and json files are supported, but',
                  catalog_file, 'given')

    else:
        # No catalog file was given, so parsing the whole lot
        print(f'Walking all files in {ontology_dir}')
        for current_dir, _, files in os.walk(ontology_dir):
            for file in files:
                if file.endswith('.ttl'):
                    file_list.add(
                        _process_file_path(ontology_dir, ontology_absdir,
                                           os.path.join(current_dir, file)))

    if len(file_list) < 1:
        print('No files found.')
    else:
        print('Done compiling file list.')
        for filename in file_list:
            print(filename)
        print('')
    return file_list
