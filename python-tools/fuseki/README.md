# Ontology QA Tools - Python Fuseki

Helper scripts for pushing data to an Apache Fuseki server and for running SPARQL queries.

## fetch_sparql_report.py

Will use cURL and [Apache Jena Fuseki](https://jena.apache.org/documentation/fuseki2/index.html) to run a [SPARQL](https://www.w3.org/TR/rdf-sparql-query/) query.
The result is delivered as UFT-8 encoded JSON.
Can be run both as a stand-alone script of by importing its functions. 

## fill_fuseki_store.py

This function will collect all .ttl files and add them to a Fuseki triple store. In order to run this function, a running [Apache Fuseki Server](https://jena.apache.org/documentation/fuseki2/index.html) needs to be present.


## fuseki_common.py

Contains helper function `get_fuseki_file_list(ontology_dir, catalog_file, excludes)` to fetch the files to process.
The `catalog_file` parameter expects either a JSON file with the format `{ "files" : [ "file1.ttl", "file2.ttl" ] }`
or a TSV file as created by the [Ontology Catalog Reader](https://gitlab.com/allotrope-open-source/allotrope-devops/-/wikis/The-Ontology-Catalog-Reader).
When using a TSV catalog, catalog groups can be excluded with the `excludes` parameter.
If only `ontology_dir` is given, walks the directory and picks up all files there.
