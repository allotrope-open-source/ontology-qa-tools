#!/usr/bin/python
"""Fuseki package setup."""

import os
from setuptools import setup

# Utility function to read the README file.
# Used for the long_description.  It's nice, because now 1) we have a top level
# README file and 2) it's easier to type in the README file than to put a raw
# string in below ...


def read(fname):
    """Read file."""
    with open(os.path.join(os.path.dirname(__file__), fname)) as file:
        return file.read()


setup(
    name='fuseki',
    version='3.0.0',
    author='OSTHUS',
    author_email='softwarebuildbot@allotrope.org',
    description=('Helper scripts for pushing data to an Apache Fuseki'
                 'server and for running SPARQL queries.'),
    license='License :: OSI Approved :: Apache Software License',
    keywords='QA ontology fuseki',
    url=
    'https://allotrope.jfrog.io/allotrope/api/pypi/libs-release-public-python/simple/fuseki/',
    packages=['fuseki'],
    long_description=read('README.md'),
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: Console',
        'License :: OSI Approved :: Apache Software License',
        'Programming Language :: Python :: 3 :: Only',
    ],
    install_requires=['requests>=2.31.0'],
    python_requires='>=3.10',
)
