#!/usr/bin/python3

# Copyright 2017-2023 OSTHUS
#
#     https://www.osthus.com
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""This script will collect .ttl files and run SPARQL queries on them to
collect data for a SPARQL report. The result is delivered in the user specified
format.

It can be run as a stand-alone, or you can import its functions.

Output files will be written to <output_path>
Add {FROM_NAMED_GRAPHS} to your query file to determine where the "from named graphs"
statements will be placed.

Usage: python fetch_sparql_report.py <ontology_dir> <output_path>
       <host> <dataset> <query_file> [<tsv_catalog_file> <comma,separated,excludes>]

If <tsv_catalog_file> is specified, will read .ttl files from that list.

In order to run this script, a running :doc:`Apache Fuseki Server
<https://gitlab.com/allotrope-open-source/allotrope-devops/wikis/Running-Apache-Fuseki>`
needs to be present.

To convert a JSON result to TSV, you can run e.g. json2tsv.py
"""

import json
import os.path
import sys

from collections import defaultdict
from xml.etree import ElementTree

import requests

from fuseki.fuseki_common import get_fuseki_file_list
from fuseki.fill_fuseki_store import fill_fuseki_store


def graph_name_from_file(filename):
    """Replaces characters in the filename in order to be suitable for
    Fuseki."""
    return filename.replace(':', '').replace('\\', '/')


def handle_output_error(accept_format, process_result):
    """Print error message and exit 1."""
    print(
        '======================================================================='
    )
    if len(process_result) > 500:
        print(
            f'Error fetching result with Accept: \'{accept_format}\':'
            '\n-----------------------------------------------------------------------\n'
            f'{process_result[:250]}\n...\n{process_result[-250:]}')
    else:
        print(
            f'Error fetching result with Accept: \'{accept_format}\':'
            '\n-----------------------------------------------------------------------\n'
            f'{process_result}')
    sys.exit(1)


def get_catalog_groups_for_graphs(tsv_catalog_filename, excludes):
    """Maps SPARQL graphs to catalog groups, using tsv_catalog_filename.
    Expects 'groupName' and 'ttlFile' as column headers. Additional column
    headers will be ignored.

    Parameters
    ----------
    tsv_catalog_filename : string
        A TSV representation of the catalog files. Expected column headers are
        * 'ttlFile' for the Turtle files
        * 'groupName' for the groups they belong to
        Additional column headers will be ignored.
        Returns empty list if 'tsv_catalog_filename' is empty.
    excludes : string[]
        List of groups to exclude from the results. Can be empty.
    """

    print('Reading groups')

    group_list = defaultdict(list)
    if len(tsv_catalog_filename) > 0:
        with open(tsv_catalog_filename, 'r', encoding='utf-8') as catalog_tsv:
            group_index = -1
            filename_index = -1
            for line_count, file_info in enumerate(catalog_tsv):
                file_info_array = file_info.strip().split('\t')
                # Find out which columns to use
                if line_count == 0:
                    for header_count, header in enumerate(file_info_array):
                        if header == 'groupName':
                            group_index = header_count
                        elif header == 'ttlFile':
                            filename_index = header_count
                # Add new entries that are not listed as excludes
                else:
                    raw_filename = graph_name_from_file(
                        file_info_array[filename_index])
                    if not file_info_array[group_index] in excludes \
                            and not file_info_array[group_index] in group_list[raw_filename]:
                        group_list[raw_filename].append(
                            file_info_array[group_index])
    return group_list


def fetch_sparql_report(host, datastore, output_path, query_file, file_list,
                        output_format):
    """Run a sparql query on a Fuseki server to retrieve a UTF-8 encoded file.
    The generated file will be called:

    <output_path>/<query_file_basename>.<output_format>.

    Returns the named graphs' prefix, e.g. 'http://localhost:3030/ontology/data'

    In order to run this function, a running :doc:`Apache Fuseki Server
    <https://gitlab.com/allotrope-open-source/allotrope-devops/wikis/Running-Apache-Fuseki>`
    needs to be present.

    Parameters
    ----------
    host : string
        Your Fuseki server's host address, e.g. 'localhost'
    datastore : string
        The name of your Fuseki server's datastore, e.g. 'ontology'
    output_path : string
        The JSON report will be written to this directory
    query_file : string
        The query to run. Include '{FROM_NAMED_GRAPHS}' as a placeholder in your query
        to pick up the named graphs of your ontology files
    file_list : a list of file paths to add to the fuseki store. Must not be empty.
    output_format : string
        Optional. The format of the result data, e.g. json or csv. Default is json.
    """

    if not file_list:
        print('ERROR: File list is empty!')
        sys.exit(1)

    if not output_format:
        output_format = 'json'

    # Ensure that the output directory exists
    if not os.path.exists(output_path):
        os.mkdir(output_path)

    sparql_endpoint = 'http://' + host + ':3030/' + datastore
    sparql_base = sparql_endpoint + '/data'

    print(f'\n############# Fetching report for {query_file} #############\n')

    # Query header
    print('\nAssembling and running query...\n')
    query = ''
    with open(query_file, 'r', encoding='utf-8') as source:
        for source_line in source:
            query = query + source_line

    # Create named graphs and add query statements
    named_graphs_query_part = ''
    # Squash named graphs into set to avoid duplicates that SPARQL will barf at
    named_graph_iris = set()
    for file in file_list:
        named_graph_iris.add(graph_name_from_file(file))

    for graph_name in named_graph_iris:
        # Append FROM NAMED graph statements to query
        # Example: FROM NAMED <http://localhost:3030/afo-public/data/lc-uv>
        graph_name = os.path.splitext(graph_name)[0]
        named_graphs_query_part = named_graphs_query_part + \
            'FROM NAMED <' + sparql_base + '/' + graph_name + '>\n'

    query = query.replace('{FROM_NAMED_GRAPHS}', named_graphs_query_part)

    print(query)
    sys.stdout.flush()

    match output_format:
        case 'csv':
            accept_format = 'text/csv; charset=utf-8'
        case 'tsv':
            accept_format = 'text/tab-separated-values; charset=utf-8'
        case _:
            accept_format = f'application/sparql-results+{output_format}; charset=utf-8'

    headers = {'Accept': accept_format}
    params = [('query', query)]

    try:
        response = requests.post(sparql_endpoint + '/query',
                                 params=params,
                                 headers=headers,
                                 timeout=1000)
        response.raise_for_status()

        response.encoding = 'utf-8'
        response_result = response.text

        match output_format:
            case 'json':
                # Simple test to ensure we are getting valid JSON
                try:
                    json.loads(response_result)
                except json.decoder.JSONDecodeError:
                    handle_output_error(accept_format, response_result)
            case 'xml':
                # Simple test to ensure we are getting valid XML
                try:
                    ElementTree.fromstring(response_result)
                except ElementTree.ParseError:
                    handle_output_error(accept_format, response_result)
            case 'tsv':
                # Remove ? prefix from header labels
                lines = response_result.split('\n')
                lines[0] = lines[0].replace('?', '')
                response_result = '\n'.join(lines)
            case 'csv':
                # Remove ? prefix from header labels
                lines = response_result.splitlines()
                lines[0] = lines[0].replace('?', '')
                # Strip blank lines
                response_result = '\n'.join([s for s in lines if s])

        result_file = os.path.normpath(
            os.path.join(
                output_path,
                os.path.splitext(os.path.basename(query_file))[0] + '.' +
                output_format))
        print(f'\nWriting report results to {result_file}')
        with open(result_file, 'w', encoding='utf-8') as dest:
            dest.write(response_result)

    except requests.exceptions.RequestException as request_error:
        handle_output_error(
            accept_format,
            f'Status code: {request_error.response.status_code}\n{request_error.response.text}'
        )

    print('Done fetching sparql report.')

    return sparql_base


def main():
    """Calls fill_fuseki_store and then fetch_sparql_report."""

    # Set up paths
    ontology_dir = sys.argv[1]
    output_path = sys.argv[2]

    # Set up SPARQL parameters and fill the datastore with everything
    host = sys.argv[3]
    datastore = sys.argv[4]

    # Our query to run
    query_file = sys.argv[5]

    # Optional parameters
    output_format = 'json'
    tsv_catalog_file = ''
    excludes = []
    if len(sys.argv) > 6:
        output_format = sys.argv[6]

        if len(sys.argv) > 7:
            tsv_catalog_file = sys.argv[7]

        if len(sys.argv) > 8:
            excludes = sys.argv[8].strip().split(',')
            for exclude in excludes:
                print(f'Excluding group {exclude}')

    # Fill the datastore with everything
        file_list = get_fuseki_file_list(ontology_dir, tsv_catalog_file,
                                         excludes)
        fill_fuseki_store(host, datastore, file_list)
    fetch_sparql_report(host, datastore, output_path, query_file, file_list,
                        output_format)


if __name__ == '__main__':
    if len(sys.argv) < 6 or len(sys.argv) > 9:
        print(
            'Usage: python fetch_sparql_report.py <ontology_dir> <output_path> '
            '<host> <dataset> <query_file> [<output_format> <tsv_catalog_file> '
            '<comma,separated,excludes>]')
        print('       If no list of files is given, all files are used.')
        sys.exit(1)
    main()
