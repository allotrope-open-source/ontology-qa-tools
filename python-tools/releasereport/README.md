# Ontology QA Tools - Python - release_report

Creates a release report that lists all entities in scope.

Needs cURL and [Apache Jena Fuseki](https://jena.apache.org/documentation/fuseki2/index.html).
