#!/usr/bin/python3

# Copyright 2017-2023 OSTHUS
#
#     https://www.osthus.com
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""This tool will generate a release report using SPARQL and a Fuseki
server.
If the output format is JSON,
- will exit with 1 on parse errors or if any duplicates are present
- will generate a tsv export

In order to run this function, a running :doc:`Apache Fuseki Server
<https://gitlab.com/allotrope-open-source/allotrope-devops/wikis/Running-Apache-Fuseki>`
needs to be present.

Usage: python afo_release_report.py <ontology_dir> <output_path> <host> <dataset>
       <query_file> [<output_format> <tsv_catalog_file> [<comma,separated,excludes>]]

If no list of files is given, all files are used.
"""

from collections import defaultdict
import csv
import json
import os.path
import sys

from fuseki.fuseki_common import get_fuseki_file_list
from fuseki.fetch_sparql_report import fetch_sparql_report, get_catalog_groups_for_graphs
from fuseki.fill_fuseki_store import fill_fuseki_store

if len(sys.argv) < 6 or len(sys.argv) > 9:
    print(
        'Usage: python afo_release_report.py <ontology_dir> <output_path> <host> <dataset> '
        '<query_file> [<output_format> <tsv_catalog_file> [<comma,separated,excludes>]]'
    )
    print('       If no list of files is given, all files are used.')
    sys.stdout.flush()
    sys.exit(1)

print('\n############# Creating Release Report #############\n')

# Set up paths
ONTOLOGY_DIR = sys.argv[1]
output_path = sys.argv[2]

# Set up SPARQL parameters and fill the datastore with everything
host = sys.argv[3]
datastore = sys.argv[4]
query_file = sys.argv[5]

# Optional parameters
OUTPUT_FORMAT = 'json'
TSV_CATALOG_FILE = ''
excludes = []
if len(sys.argv) > 6:
    OUTPUT_FORMAT = sys.argv[6]

    if len(sys.argv) > 7:
        TSV_CATALOG_FILE = sys.argv[7]

        if len(sys.argv) > 8:
            # Parse catalog groups to exclude
            excludes = sys.argv[8].strip().split(',')
            for exclude in excludes:
                print(f'Excluding group {exclude}')
            print()

print('Ontology dir: ', ONTOLOGY_DIR)
print('Host:         ', host)
print('Datastore:    ', datastore)
print('Output format:', OUTPUT_FORMAT)
print('Excludes:     ', excludes)
print('Query file:   ', query_file)
print('Catalog file: ', TSV_CATALOG_FILE)
print('Output path:  ', output_path)

# Get the data
file_list = get_fuseki_file_list(ONTOLOGY_DIR, TSV_CATALOG_FILE, excludes)
fill_fuseki_store(host, datastore, file_list)
graph_prefix = fetch_sparql_report(host, datastore, output_path, query_file,
                                   file_list, OUTPUT_FORMAT)


class Entry:
    """Entries to list in report."""

    def __init__(self):
        self.group = ''
        self.data = defaultdict(list)


# Set up filenames
filename_base = os.path.splitext(os.path.basename(query_file))[0]

if OUTPUT_FORMAT == 'json':
    input_file = os.path.join(output_path, filename_base + '.json')
    output_file = os.path.join(output_path, filename_base + '.tsv')

    print(f'Loading release report from JSON: {input_file}')

    # Load the JSON
    try:
        with open(input_file, 'r', encoding='utf-8') as f:
            data = json.load(f)
    except ValueError:
        print('ERROR in JSON file!\nFile contents:')
        with open(input_file, 'r', encoding='utf-8') as f:
            for line in f:
                print(line)
        sys.exit(1)

    # Grab the header and results
    head = data['head']['vars']

    # Now write release report
    # We use UTF-16 so that Excel can read it.
    with open(output_file, 'w', newline='', encoding='utf-16') as f:
        tsv_writer = csv.writer(f, delimiter='\t')
        tsv_writer.writerow(head)  # write the header

        for row in data['results']['bindings']:  # write data rows
            columns = []
            for key in head:
                if key in row:
                    columns.append(row[key]['value'].replace('\r\n',
                                                             '\\r\\n').replace(
                                                                 '\n', '\\n'))
                else:
                    columns.append('')
            tsv_writer.writerow(columns)

else:
    output_file = os.path.join(output_path,
                               filename_base + '.' + OUTPUT_FORMAT)

print(f'Release report written to {output_file}')
