# Ontology QA Tools - Python

A collection of Python tools. Most of these tools are written in Python 3.

## duplicates_report.py

Detects whether the same entity has multiple definitions, or multiple entities have the same prefLabel.

See our [Devops Wiki](https://gitlab.com/allotrope-open-source/allotrope-devops/wikis/Duplicates-Reports-Python-Fuseki-Tool) for documentation.

## release_report.py

Creates a release report that lists all entities in scope.

See our [Devops Wiki](https://gitlab.com/allotrope-open-source/allotrope-devops/wikis/Release-Report-Python-Fuseki-Tool) for documentation.

## spellcheck_report.py

Will run a spell check on all entities in scope.

See our [Devops Wiki](https://gitlab.com/allotrope-open-source/allotrope-devops/wikis/Spellcheck-Report-Python-Fuseki-Tool) for documentation.

## json2tsv.py

Will convert the JSON result delivered by ´fetch_sparql_report.py` into TSV format.

Usage: `python json2tsv.py <json_input_file> <tsv_output_file>`

## normalize-catalogs.py

https://gitlab.com/allotrope-open-source/allotrope-devops/wikis/Normalize-Catalogs-Python-Tool

## print_lines.py

Prints the specified amount of lines at the start of a given plain text file.

Usage: `python print-lines.py <filename> <number_of_lines> <file_encoding>`

Example: `python print-lines.py foo.txt 15 utf-8`

## fuseki

Te Fuseki package contains helper scripts for pushing data to an Apache Fuseki server and for running SPARQL queries.