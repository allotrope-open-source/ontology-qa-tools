# Ontology QA Tools - Python - spellcheck_report

Will run a spell check on all entities in scope.

Needs cURL, [Apache Jena Fuseki](https://jena.apache.org/documentation/fuseki2/index.html) and [pyenchant](https://pypi.python.org/pypi/pyenchant/).