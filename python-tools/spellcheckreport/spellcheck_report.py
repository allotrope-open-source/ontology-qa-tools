#!/usr/bin/python3

# Copyright 2017-2023 OSTHUS
#
#     https://www.osthus.com
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

"""Utility to generate a TSV spell check report from a SPARQL report's JSON.

Will exit with 1 on parse errors or if any spelling errors were found.
"""

import json
import os.path
import re
import sys

import enchant

from fuseki.fuseki_common import get_fuseki_file_list
from fuseki.fetch_sparql_report import fetch_sparql_report
from fuseki.fill_fuseki_store import fill_fuseki_store

if len(sys.argv) < 8 or len(sys.argv) > 10:
    print(
        'Usage: python afo_spellcheck_report.py <ontology_dir> <output_path> '
        '<host> <dataset> <query_file> <user_phrases_file> <user_dictionary_file> [<TSV_CATALOG_FILE> [<comma,separated,excludes>]]')
    print('       If no list of files is given, all files are used.')
    sys.stdout.flush()
    sys.exit(1)

print('\n############# Creating Spellcheck Report #############\n')

# Set up paths
ontology_dir = sys.argv[1]
output_path = sys.argv[2]

# Set up SPARQL parameters and fill the datastore with everything
host = sys.argv[3]
datastore = sys.argv[4]
query_file = sys.argv[5]
user_phrases_file = sys.argv[6]
user_dictionary_file = sys.argv[7]

# Optional parameters
TSV_CATALOG_FILE = ''
excludes = []
if len(sys.argv) > 8:
    TSV_CATALOG_FILE = sys.argv[8]

    if len(sys.argv) > 9:
        # Parse catalog groups to exclude
        excludes = sys.argv[9].strip().split(',')
        for exclude in excludes:
            print('Excluding group %s' % exclude)
        print()

print('Ontology dir:   ', ontology_dir)
print('Host:           ', host)
print('Datastore:      ', datastore)
print('Excludes:       ', excludes)
print('Query file:     ', query_file)
print('Catalog file:   ', TSV_CATALOG_FILE)
print('Phrases file:   ', user_phrases_file)
print('Dictionary file:', user_dictionary_file)
print('Output path:    ', output_path)


# Get the data
file_list = get_fuseki_file_list(ontology_dir, TSV_CATALOG_FILE, excludes)
fill_fuseki_store(host, datastore, file_list)
graph_prefix = fetch_sparql_report(
    host,
    datastore,
    output_path,
    query_file,
    file_list,
    'json')

# Set up filenames
filename_base = os.path.splitext(os.path.basename(query_file))[0]
input_file = os.path.join(output_path, filename_base + '.json')
output_file = os.path.join(output_path, filename_base + '.tsv')

print('Loading data to check from JSON: %s' % input_file)

# Load the JSON
try:
    with open(input_file, 'r', encoding='utf-8') as f:
        data = json.load(f)
except ValueError:
    print('ERROR in JSON file!\nFile contents:')
    with open(input_file, 'r', encoding='utf-8') as f:
        for line in f:
            print(line)
    sys.exit(1)

print('Loading user phrases from %s' % user_phrases_file)
user_phrases = []
with open(user_phrases_file, 'r', encoding='utf-8') as f:
    for line in f:
        if not line.startswith('#'):
            user_phrases.append(line.strip())
print('We have %d user phrases' % len(user_phrases))
user_phrases.sort(key=len, reverse=True)

print('Loading user dictionary from %s' % user_dictionary_file)
user_dictionary = []
with open(user_dictionary_file, 'r', encoding='utf-8') as f:
    for line in f:
        if not line.startswith('#'):
            user_dictionary.append(line.strip())
print('We have %d entries in the user dictionary' % len(user_dictionary))

# Entry storage


class Entry:
    """Entries to spellcheck."""

    def __init__(self):
        self.graph = ''
        self.entity = ''
        self.label = ''
        self.altLabels = ''
        self.definition = ''
        self.errormessage = ''


# Have the spellchecking function dump pre-formatted results into the error_entries array
error_entries = []

regex_escape_pattern = re.compile(r'([().*+\/\[\]])')


def spellcheck(graph, entity, header, sentence):
    if sentence == '':
        return
    original_sentence = sentence
    # Exclude items from user dictionary from the check, including items that contain whitespace
    for greenlit in user_phrases:
        if sentence == '':
            return
        greenlit = re.sub(regex_escape_pattern, r'\\\1', greenlit)
        if greenlit == '':
            continue
        pattern = r'(^|.*\W+)(' + greenlit + r')(\W+.*|$)'
        regex = re.compile(pattern)
        while True:
            match = regex.match(sentence)
            if match:
                sentence = match.groups(0)[0] + match.groups(0)[2]
            else:
                break

    if sentence == '':
        return
    # Split into sentences
    for segment in sentence.split('. '):
        # Split into words
        for index, word in enumerate(segment.strip().replace(' – ', ' ').replace(' + ', ' ').replace(' - ', ' ').replace('—', ' ').split()):
            stripped_word = word.strip().strip(',.;:()[]\'"')
            # Special treatment for start of sentence because of sentence case.
            if index == 0 and stripped_word.lower() in user_dictionary:
                continue
            try:
                if stripped_word and not (spellchecker.check(word) or spellchecker.check(stripped_word)) and (not stripped_word in user_dictionary):
                    error_entries.append(graph + '\t' + entity + '\t' + header + '\t' + original_sentence +
                                         '\t' + word.replace('"', '') +
                                         '\t' + ', '.join(spellchecker.suggest(stripped_word)) + '\n')
            except enchant.errors.Error:
                error_entries.append(graph + '\t' + entity + '\t' + header +
                                     '\t' + original_sentence +
                                     '\t' + word.replace('"', '') +
                                     '\tNo suggestions - maybe an extra whitespace?\n')


# Grab the header and results
head = data['head']['vars']
bindings = data['results']['bindings']


# Now do the actual spellcheck
sys.stdout.write('Running spellcheck ')
spellchecker = enchant.Dict('en_US')

for counter, result in enumerate(bindings):
    if counter % 50 == 0:
        sys.stdout.write('.')
        sys.stdout.flush()
    entry = Entry()
    for key in head:
        if key in result:
            current_element = result[key]['value'].replace(
                '\n', ' ').replace('\t', '    ')
            if key == 'graph':
                entry.graph = current_element[len(graph_prefix)+1:]
            elif key == 'entity':
                entry.entity = current_element
            elif key == 'label':
                entry.label = current_element
            elif key == 'altLabels':
                entry.altLabels = current_element
            elif key == 'definition':
                entry.definition = current_element

    spellcheck(entry.graph, entry.entity, 'prefLabel', entry.label)
    spellcheck(entry.graph, entry.entity, 'altLabels', entry.altLabels)
    spellcheck(entry.graph, entry.entity, 'definition', entry.definition)

print('')

# Parse and write spellcheck report
# We use UTF-16 so that Excel can read it.
no_of_errors = len(error_entries)
with open(output_file, 'w', encoding='utf-16') as f:
    if no_of_errors == 0:
        f.write('No spelling errors found :)')
    else:
        # Write header
        f.write('Graph\tEntity\tElement\tContent\tMisspelling\tSuggestions\n')
        for entry in error_entries:
            f.write(entry)
print('Spellcheck report written to %s ' % output_file)

# Now exit according to whether we found any spelling errors
if no_of_errors > 0:
    print('Error! Found %d spelling errors!' % no_of_errors)
    sys.exit(1)
else:
    print('Result is clean; no spelling errors found :)')
