#!/usr/bin/python

"""Spellcheck Report package setup."""

import os
from setuptools import setup

# Utility function to read the README file.
# Used for the long_description.  It's nice, because now 1) we have a top level
# README file and 2) it's easier to type in the README file than to put a raw
# string in below ...


def read(fname):
    """Read file."""
    with open(os.path.join(os.path.dirname(__file__), fname)) as file:
        return file.read()


setup(
    name='spellcheckreport',
    version='2.0.0',
    author='OSTHUS',
    author_email='softwarebuildbot@allotrope.org',
    description=('Will run a spell check on all entities in scope'),
    license='License :: OSI Approved :: GNU Lesser General Public License v2 or later (LGPLv2+)',
    keywords='QA ontology spellcheck',
    url='https://allotrope.jfrog.io/allotrope/api/pypi/libs-release-python/simple/spellcheckreport/',
    packages=['spellcheckreport'],
    long_description=read('README.md'),
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Environment :: Console',
        'License :: OSI Approved :: GNU Lesser General Public License v2 or later (LGPLv2+)',
        'Programming Language :: Python :: 3 :: Only',
    ],
    python_requires='>=3.10',
)
