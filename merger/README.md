# Ontology Merger

Standard tool to create merged ontologies of AFO files based on OWLAPI

Execute from command line:

java -Dlog4j.configurationFile=resources/log4j2.xml -jar ontology-merger.jar -help

e.g.
"C:\Program Files\Java\jdk1.8.0_45\bin\java" -jar ontology-merger.jar --combine --git --folder c:\path\to\ontology\folder"


--combine tells the merger tool to combine the merged file with inferred triples into one artifact
--folder tells the merger tool the absolute path to the root folder of ontologies (in case of the AFO Git repository e.g. "c:\afo" if you checked out the AFO repository to c:\)

See our [Devops Wiki](https://gitlab.com/allotrope-open-source/allotrope-devops/wikis/The-Ontology-Merger-Tool) for documentation.