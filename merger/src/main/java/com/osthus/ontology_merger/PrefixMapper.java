package com.osthus.ontology_merger;

/*
 *    Copyright 2022 OSTHUS
 *
 *      https://www.osthus.com
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.util.FileUtils;
import org.apache.jena.vocabulary.DCTerms;
import org.apache.jena.vocabulary.OWL;
import org.apache.jena.vocabulary.RDFS;
import org.semanticweb.owlapi.formats.PrefixDocumentFormat;
import org.semanticweb.owlapi.model.OWLDocumentFormat;

/**
 * Use Apache Jena to load prefix map from file
 */
public class PrefixMapper {

	/** Maps prefixes to ontology IRIs */
	private Map<String, String> prefixes = new HashMap<>();
	// For efficient duplicate checking
	private Set<String> values = new HashSet<>();

	/**
	 * Add the given prefix if it's not already known and if it's not obo
	 * 
	 * @param prefix
	 *            The prefix to add
	 * @param iri
	 *            The IRI that the prefix will resolve to
	 */
	private void addPrefix(String prefix, String iri) {
		if ("http://purl.obolibrary.org/obo/".equals(iri) || "obo".equals(prefix)) {
			// We never want the obo prefix
			MergerMain.log.info("   skipping prefix " + prefix + ": " + iri);
			return;
		}
		if (!values.contains(iri) && !prefixes.containsKey(prefix)) {
			prefixes.put(prefix, iri);
			values.add(iri);
			MergerMain.log.info("   adding prefix " + prefix + ": " + iri);
		}
	}

	/**
	 * Initialize the prefix map with some standard prefixes
	 */
	public PrefixMapper() {
		this.prefixes = new HashMap<>();
		this.values = new HashSet<>();

		// Ensure some prefixes uses in metadata are in AFO-style
		addPrefix("dct", DCTerms.getURI());
		addPrefix("rdfs", RDFS.getURI());
		addPrefix("owl", OWL.getURI());
	}

	/**
	 * Parse prefixmap and add new values if they don't conflict
	 * 
	 * @param file
	 *            An Ontology file
	 * @throws FileNotFoundException
	 *             The file does not exist
	 */
	public void addOntology(File file) throws FileNotFoundException {
		MergerMain.log.info("...fetching prefix map from file " + file);
		Model model = ModelFactory.createDefaultModel();
		model.read(new FileReader(file), "urn:x-base:default", FileUtils.guessLang(file.getName().toString()));

		Map<String, String> newPrefixes = model.getNsPrefixMap();
		for (String prefKey : newPrefixes.keySet()) {
			String prefValue = newPrefixes.get(prefKey);
			addPrefix(prefKey, prefValue);
		}
	}

	/**
	 * Add all prefixes for a document format. Conflicts with existing prefixes
	 * will be discarded.
	 * 
	 * @param format
	 *            THis must be a PrefixDocumentFormat
	 */
	public void addPrefixes(OWLDocumentFormat format) {
		PrefixDocumentFormat prefixFormat = format.asPrefixOWLOntologyFormat();
		for (String name : prefixFormat.getPrefixNames()) {
			if (name.isEmpty()) {
				// Overwrite the default prefix
				String iri = prefixFormat.getPrefix(name);
				MergerMain.log.info("   overwriting default prefix with: " + iri);
				values.remove(iri);
				prefixes.put(name, iri);
				values.add(iri);

			} else {
				addPrefix(name, prefixFormat.getPrefix(name));
			}
		}
	}

	/**
	 * Add all prefixes from the given Map. Conflicts with existing prefixes
	 * will be discarded.
	 * 
	 * @param prefixes
	 *            The prefixes to add in prefix, IRI format.
	 */
	public void addPrefixes(Map<String, String> prefixes) {
		for (String name : prefixes.keySet()) {
			addPrefix(name, prefixes.get(name));
		}
	}

	/**
	 * Deregister unwanted obo prefix
	 * 
	 * @param owlDocumentFormat
	 *            The document format to remove the prefix from
	 */
	public static void deregisterOboPrefix(OWLDocumentFormat owlDocumentFormat) {
		if (owlDocumentFormat.asPrefixOWLOntologyFormat().getPrefix("obo") != null) {
			owlDocumentFormat.asPrefixOWLOntologyFormat().unregisterNamespace("http://purl.obolibrary.org/obo/");
		}
	}

	/**
	 * Get all registered prefixes
	 * 
	 * @return &lt;prefix, IRI&gt;
	 */
	public Map<String, String> getPrefixes() {
		return prefixes;
	}

}
