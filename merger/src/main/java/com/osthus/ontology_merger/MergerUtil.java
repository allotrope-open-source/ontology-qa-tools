/*
 *    Copyright 2017-2022 OSTHUS
 *
 *      https://www.osthus.com
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.osthus.ontology_merger;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Pattern;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.vocabulary.OWL;
import org.apache.jena.vocabulary.RDF;
import org.protege.xmlcatalog.owlapi.XMLCatalogIRIMapper;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.formats.TurtleDocumentFormatFactory;
import org.semanticweb.owlapi.io.StringDocumentSource;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLDocumentFormat;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyIRIMapper;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;
import org.semanticweb.owlapi.util.AutoIRIMapper;
import org.semanticweb.owlapi.util.SimpleIRIMapper;

public class MergerUtil {
	public static String HOME = "";
	public static String LOCAL = "src/main/resources/";
	public static String FILE_PREFIX = "file:///";

	public static LinkedHashMap<IRI, IRI> aftIriMap = null;
	public static LinkedHashMap<IRI, IRI> adfIriMap = null;
	public static LinkedHashMap<IRI, IRI> otherIriMap = null;
	public static LinkedHashMap<IRI, IRI> userIriMap = null;

	public static void init() {
		aftIriMap = new LinkedHashMap<IRI, IRI>();
		aftIriMap.put(OntologyIri.AF_PROPERTY, IRI.create(FILE_PREFIX + HOME + "af-x/af-property.ttl"));
		aftIriMap.put(OntologyIri.AF_COMMON_QUALIFIER,
				IRI.create(FILE_PREFIX + HOME + "af-cq/af-common-qualifier.ttl"));
		aftIriMap.put(OntologyIri.AF_COMMON, IRI.create(FILE_PREFIX + HOME + "af-c/af-common.ttl"));
		aftIriMap.put(OntologyIri.AF_MATERIAL, IRI.create(FILE_PREFIX + HOME + "af-m/af-material.ttl"));
		aftIriMap.put(OntologyIri.AF_PROCESS, IRI.create(FILE_PREFIX + HOME + "af-p/af-process.ttl"));
		aftIriMap.put(OntologyIri.AF_RESULT, IRI.create(FILE_PREFIX + HOME + "af-r/af-result.ttl"));
		aftIriMap.put(OntologyIri.AF_EQUIPMENT_CODELIST_001,
				IRI.create(FILE_PREFIX + HOME + "af-e/codelist/af-equipment-001.ttl"));
		aftIriMap.put(OntologyIri.AF_EQUIPMENT_CODELIST_002,
				IRI.create(FILE_PREFIX + HOME + "af-e/codelist/af-equipment-002.ttl"));
		aftIriMap.put(OntologyIri.AF_EQUIPMENT_CODELIST_003,
				IRI.create(FILE_PREFIX + HOME + "af-e/codelist/af-equipment-003.ttl"));
		aftIriMap.put(OntologyIri.AF_EQUIPMENT_CODELIST_004,
				IRI.create(FILE_PREFIX + HOME + "af-e/codelist/af-equipment-004.ttl"));
		aftIriMap.put(OntologyIri.AF_EQUIPMENT_CODELIST_005,
				IRI.create(FILE_PREFIX + HOME + "af-e/codelist/af-equipment-005.ttl"));
		aftIriMap.put(OntologyIri.AF_EQUIPMENT_CODELIST_006,
				IRI.create(FILE_PREFIX + HOME + "af-e/codelist/af-equipment-006.ttl"));
		aftIriMap.put(OntologyIri.AF_EQUIPMENT, IRI.create(FILE_PREFIX + HOME + "af-e/af-equipment.ttl"));

		adfIriMap = new LinkedHashMap<IRI, IRI>();
		adfIriMap.put(OntologyIri.ADF_DC, IRI.create(FILE_PREFIX + HOME + "adf-dc/adf-dc.ttl"));
		adfIriMap.put(OntologyIri.ADF_DC_HDF, IRI.create(FILE_PREFIX + HOME + "adf-dc/adf-dc-hdf-map.ttl"));
		adfIriMap.put(OntologyIri.ADF_CUBE, IRI.create(FILE_PREFIX + HOME + "adf-dc/cube.ttl"));
		adfIriMap.put(OntologyIri.ADF_DP, IRI.create(FILE_PREFIX + HOME + "adf-dp/adf-dp.ttl"));
		adfIriMap.put(OntologyIri.ADF_AUDIT, IRI.create(FILE_PREFIX + HOME + "adf-audit/adf-audit.ttl"));
		adfIriMap.put(OntologyIri.ADF_GRAPH, IRI.create(FILE_PREFIX + HOME + "adf-graph/adf-graph.ttl"));

		otherIriMap = new LinkedHashMap<IRI, IRI>();
		otherIriMap.put(OntologyIri.HDF, IRI.create(FILE_PREFIX + HOME + "hdf/hdf.ttl"));
		otherIriMap.put(OntologyIri.QUDT, IRI.create(FILE_PREFIX + HOME + "qudt-ext/OSG_qudt-(v1.01)-fix.ttl"));
		otherIriMap.put(OntologyIri.QUDT_UNIT,
				IRI.create(FILE_PREFIX + HOME + "qudt-ext/OVG_units-qudt-(v1.1)-fix.ttl"));
		otherIriMap.put(OntologyIri.QUDT_DIM, IRI.create(FILE_PREFIX + HOME + "qudt-ext/OSG_dimension-(v1.1)-fix.ttl"));
		otherIriMap.put(OntologyIri.QUDT_QUANTITY,
				IRI.create(FILE_PREFIX + HOME + "qudt-ext/OSG_quantity-(v1.1)-fix.ttl"));
		otherIriMap.put(OntologyIri.QUDT_DIM_UNIT,
				IRI.create(FILE_PREFIX + HOME + "qudt-ext/OVG_dimensionalunits-qudt-(v1.1)-fix.ttl"));
		otherIriMap.put(OntologyIri.QUDT_EXT, IRI.create(FILE_PREFIX + HOME + "qudt-ext/qudt-ext.ttl"));
		// otherIriMap.put(OntologyIri.QUDT_SHAPES, IRI.create(FILE_PREFIX +
		// HOME + "qudt-ext/qudt-shapes.ttl"));
		otherIriMap.put(OntologyIri.ADF_CUBE, IRI.create(FILE_PREFIX + HOME + "adf-dc/cube.ttl"));
		otherIriMap.put(OntologyIri.ADF_DP, IRI.create(FILE_PREFIX + HOME + "adf-dp/adf-dp.ttl"));
		otherIriMap.put(OntologyIri.AF_BP, IRI.create(FILE_PREFIX + HOME + "af-bp/af-blueprint.ttl"));
		otherIriMap.put(OntologyIri.DCT, IRI.create(FILE_PREFIX + HOME + "dct/dcterms.ttl"));
		otherIriMap.put(OntologyIri.FOAF, IRI.create(FILE_PREFIX + HOME + "foaf/foaf.ttl"));
		otherIriMap.put(OntologyIri.ORE, IRI.create(FILE_PREFIX + HOME + "ore/ore-terms.ttl"));
		otherIriMap.put(OntologyIri.ORG, IRI.create(FILE_PREFIX + HOME + "org/org.ttl"));
		otherIriMap.put(OntologyIri.TIME, IRI.create(FILE_PREFIX + HOME + "time/time.ttl"));
		otherIriMap.put(OntologyIri.PAV, IRI.create(FILE_PREFIX + HOME + "pav/pav.ttl"));
		otherIriMap.put(OntologyIri.PROV, IRI.create(FILE_PREFIX + HOME + "prov/prov-o.ttl"));
		// otherIriMap.put(OntologyIri.SKOS, IRI.create(FILE_PREFIX + HOME +
		// "skos/skos-owl1-dl.ttl"));
		otherIriMap.put(OntologyIri.TIMEZONE, IRI.create(FILE_PREFIX + HOME + "time/timezone.ttl"));
	}

	public static ByteArrayOutputStream createOutputStream() {
		ByteArrayOutputStream output = new ByteArrayOutputStream() {
			private StringBuilder string = new StringBuilder();

			@Override
			public void write(int b) {
				string.append((char) b);
			}
		};
		return output;
	}

	public static OWLOntologyManager initIriMappersForGitFolders(OWLOntologyManager manager) {
		for (Entry<IRI, IRI> entry : aftIriMap.entrySet()) {
			manager.getIRIMappers().add(new SimpleIRIMapper(entry.getKey(), entry.getValue()));
		}

		for (Entry<IRI, IRI> entry : adfIriMap.entrySet()) {
			manager.getIRIMappers().add(new SimpleIRIMapper(entry.getKey(), entry.getValue()));
		}

		for (Entry<IRI, IRI> entry : otherIriMap.entrySet()) {
			manager.getIRIMappers().add(new SimpleIRIMapper(entry.getKey(), entry.getValue()));
		}

		File localFolder = new File(MergerUtil.HOME);
		manager.getIRIMappers().add(new AutoIRIMapper(localFolder, true));

		return manager;
	}

	public static void initIriMappersForXmlCatalog(OWLOntologyManager manager, String catalogFile) throws Exception {
		manager.getIRIMappers().add(new XMLCatalogIRIMapper(new File(catalogFile)));
	}

	public static OWLOntologyManager initIriMappersForUserSpecifiedOntologies(OWLOntologyManager manager,
			String[] inputOntologies) throws NoSuchFieldException, SecurityException {
		userIriMap = new LinkedHashMap<IRI, IRI>();

		File rootFolder = null;
		if (!MergerMain.useGitFolderStructure && !MergerMain.useSpecificFolder) {
			// not merging AFO from GIT and no other root folder was specified
			// --> take local folder
			rootFolder = Paths.get(".").toFile();
		} else if (MergerMain.useSpecificFolder) {
			// merging from user specified root folder
			rootFolder = Paths.get(HOME).toFile();
		}

		if (!MergerMain.isInput) {
			// no loading of defined ontologies from defined files
			if ((!MergerMain.useGitFolderStructure && !MergerMain.useSpecificFolder) || MergerMain.useSpecificFolder) {
				MergerAutoIRIMapper autoIRIMapper = new MergerAutoIRIMapper(
						Paths.get(rootFolder.getAbsolutePath().replaceAll("%20", " ")).toFile(), true);
				Set<String> fileExtensions = new HashSet<>();
				fileExtensions.add("ttl");
				autoIRIMapper.setFileExtensions(fileExtensions);
				autoIRIMapper.update();
				manager.getIRIMappers().add(autoIRIMapper);

				for (int i = 0; i < inputOntologies.length; i++) {
					String ontologyIriString = inputOntologies[i].trim();

					IRI ontologyIri = IRI.create(ontologyIriString);

					IRI documentIri = autoIRIMapper.getDocumentIRI(ontologyIri);

					// Sometimes, XML catalogs contain additional IRIs for a
					// file that are not present in the file itself.
					// We simply skip them.
					if (documentIri != null) {
						MergerMain.log.warn("...skipping ontology IRI: {}", ontologyIri);
						userIriMap.put(ontologyIri, autoIRIMapper.getDocumentIRI(ontologyIri));
					}
				}
				return manager;
			}
		}

		for (int i = 0; i < inputOntologies.length; i = i + 2) {
			String ontologyIriString = inputOntologies[i].trim();
			String fileIriString = inputOntologies[i + 1].trim().replaceAll(Pattern.quote("\\"), "/");

			if (!fileIriString.startsWith(FILE_PREFIX)) {
				fileIriString = FILE_PREFIX + fileIriString;
			}

			IRI ontologyIri = IRI.create(ontologyIriString);
			IRI fileIri = IRI.create(fileIriString);

			manager.getIRIMappers().add(new SimpleIRIMapper(ontologyIri, fileIri));

			userIriMap.put(ontologyIri, fileIri);
		}

		return manager;
	}

	public static OWLOntologyManager initAutoIriMapper(OWLOntologyManager manager) {
		File rootFolder = Paths.get(".").toFile();
		if (MergerMain.useSpecificFolder) {
			rootFolder = Paths.get(HOME).toFile();
		}
		OWLOntologyIRIMapper autoIRIMapper = new AutoIRIMapper(rootFolder, false);
		manager.getIRIMappers().add(autoIRIMapper);
		return manager;
	}

	public static void writeModelViaOwlApi(Model model, Path path, List<String> ignoredIris)
			throws OWLOntologyCreationException, OWLOntologyStorageException, IOException {
		ByteArrayOutputStream output = new ByteArrayOutputStream() {
			private StringBuilder string = new StringBuilder();

			@Override
			public void write(int b) {
				string.append((char) b);
			}
		};

		model.write(output, "TTL");

		Resource ontologyResource = model.listStatements((Resource) null, RDF.type, OWL.Ontology).next()
				.getSubject();
		List<Statement> importedOntologies = model.listStatements(ontologyResource, OWL.imports, (RDFNode) null)
				.toList();
		List<String> importedOntologyIris = new ArrayList<>();
		if (importedOntologies != null && !importedOntologies.isEmpty()) {
			for (Iterator<Statement> iterator = importedOntologies.iterator(); iterator.hasNext();) {
				Statement statement = iterator.next();
				Resource importedOntology = statement.getResource();
				importedOntologyIris.add(importedOntology.getURI());
			}
		}

		StringDocumentSource stringDocumentSource = new StringDocumentSource(output.toString("UTF-8"));

		OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
		if (ignoredIris != null && !ignoredIris.isEmpty()) {
			for (Iterator<String> iterator = ignoredIris.iterator(); iterator.hasNext();) {
				String iri = iterator.next();
				manager.setOntologyLoaderConfiguration(
						manager.getOntologyLoaderConfiguration().addIgnoredImport(IRI.create(iri)));
				MergerMain.log.debug("...ignoring imported ontology " + iri);
			}
		}
		if (importedOntologyIris != null && !importedOntologyIris.isEmpty()) {
			for (Iterator<String> iterator = importedOntologyIris.iterator(); iterator.hasNext();) {
				String iri = iterator.next();
				manager.setOntologyLoaderConfiguration(
						manager.getOntologyLoaderConfiguration().addIgnoredImport(IRI.create(iri)));
				// MergerMain.log.debug("...ignoring imported ontology " + iri);
			}
		}

		OWLOntology ontology = manager.loadOntologyFromOntologyDocument(stringDocumentSource);
		OWLDocumentFormat owlDocumentFormat = new TurtleDocumentFormatFactory().createFormat();
		MergerMain.prefixMapper.addPrefixes(model.getNsPrefixMap());
		owlDocumentFormat.asPrefixOWLOntologyFormat().copyPrefixesFrom(MergerMain.prefixMapper.getPrefixes());
		PrefixMapper.deregisterOboPrefix(owlDocumentFormat);

		Files.deleteIfExists(path);
		manager.saveOntology(ontology, owlDocumentFormat, IRI.create(path.toFile().toURI()));
	}

	public static Set<Statement> addNestedBlankNode(Model model, Statement statementWithBlankObject,
			Set<Statement> extractedStatements, String iri) {
		StmtIterator stmtIterator = model.listStatements(statementWithBlankObject.getObject().asResource(),
				(Property) null, (RDFNode) null);
		while (stmtIterator.hasNext()) {
			Statement statement = stmtIterator.next();
			extractedStatements.add(statement);
			if (statement.getObject().isAnon() && !statement.getObject().toString().equals(iri)) {
				extractedStatements = addNestedBlankNode(model, statement, extractedStatements, iri);
			}
		}
		return extractedStatements;
	}

	public static Set<Statement> extractStatementsWithSelectedTermAsObject(Resource subject, Model model,
			Set<Statement> extractedStatements) {
		StmtIterator statementsWithSelectedObjectIterator = model.listStatements((Resource) null, (Property) null,
				subject);
		while (statementsWithSelectedObjectIterator.hasNext()) {
			Statement statementWithSelectedObject = statementsWithSelectedObjectIterator.next();
			extractedStatements.add(statementWithSelectedObject);
			if (statementWithSelectedObject.getSubject().isAnon()) {
				String blankIri = statementWithSelectedObject.getSubject().toString();
				extractedStatements = extractStatementsWithBlankNodeAsSubject(statementWithSelectedObject.getSubject(),
						model, extractedStatements, blankIri);
				extractedStatements = extractStatementsWithBlankNodeAsObject(statementWithSelectedObject.getSubject(),
						model, extractedStatements, blankIri);
			}
		}

		return extractedStatements;
	}

	public static Set<Statement> extractStatementsWithSelectedTermAsProperty(Resource subject, Model model,
			Set<Statement> extractedStatements) {
		StmtIterator statementsWithSelectedPropertyIterator = model.listStatements((Resource) null,
				ResourceFactory.createProperty(subject.getURI()), (RDFNode) null);
		while (statementsWithSelectedPropertyIterator.hasNext()) {
			Statement statementWithSelectedProperty = statementsWithSelectedPropertyIterator.next();
			extractedStatements.add(statementWithSelectedProperty);
			if (statementWithSelectedProperty.getObject().isAnon()) {
				String blankIri = statementWithSelectedProperty.getObject().toString();
				extractedStatements = extractStatementsWithBlankNodeAsSubject(
						statementWithSelectedProperty.getResource(), model, extractedStatements, blankIri);
			}
			if (statementWithSelectedProperty.getSubject().isAnon()) {
				String blankIri = statementWithSelectedProperty.getSubject().toString();
				extractedStatements = extractStatementsWithBlankNodeAsSubject(
						statementWithSelectedProperty.getSubject(), model, extractedStatements, blankIri);
			}
		}

		return extractedStatements;
	}

	private static Set<Statement> extractStatementsWithBlankNodeAsObject(Resource blankNode, Model model,
			Set<Statement> extractedStatements, String blankIri) {
		StmtIterator stmtIterator = model.listStatements((Resource) null, (Property) null, blankNode);
		while (stmtIterator.hasNext()) {
			Statement statement = stmtIterator.next();
			extractedStatements.add(statement);
			if (statement.getSubject().isAnon() && !statement.getSubject().toString().equals(blankIri)) {
				extractedStatements = extractStatementsWithBlankNodeAsObject(statement.getSubject(), model,
						extractedStatements, blankIri);
			}
		}

		return extractedStatements;
	}

	private static Set<Statement> extractStatementsWithBlankNodeAsSubject(Resource blankNode, Model model,
			Set<Statement> extractedStatements, String blankIri) {
		StmtIterator stmtIterator = model.listStatements(blankNode, (Property) null, (RDFNode) null);
		while (stmtIterator.hasNext()) {
			Statement statement = stmtIterator.next();
			extractedStatements.add(statement);
			if (statement.getObject().isAnon() && !statement.getObject().toString().equals(blankIri)) {
				extractedStatements = extractStatementsWithBlankNodeAsSubject(statement.getResource(), model,
						extractedStatements, blankIri);
			}
		}

		return extractedStatements;
	}

}
