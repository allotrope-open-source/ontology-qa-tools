/*
 *    Copyright 2017-2021 OSTHUS
 *
 *      https://www.osthus.com
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.osthus.ontology_merger;

import java.io.PrintWriter;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.Nonnull;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.text.translate.NumericEntityEscaper;
import org.semanticweb.HermiT.Configuration;
import org.semanticweb.owlapi.debugging.BlackBoxOWLDebugger;
import org.semanticweb.owlapi.debugging.OWLDebugger;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLException;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.reasoner.OWLReasonerFactory;

@SuppressWarnings("javadoc")
public class MergerDebugger {

	@Nonnull
	private final OWLOntology ontology;
	@Nonnull
	private final OWLDebugger debugger;
	private final OWLReasoner checker;
	@Nonnull
	private final OWLClass bottom;

	public MergerDebugger(@Nonnull OWLOntologyManager manager, @Nonnull OWLOntology ontology,
			@Nonnull OWLReasonerFactory reasonerFactory) {
		this.ontology = ontology;
		Configuration configuration = new Configuration();
		configuration.ignoreUnsupportedDatatypes = false;
		configuration.throwInconsistentOntologyException = false;
		checker = reasonerFactory.createReasoner(ontology, configuration);
		debugger = new BlackBoxOWLDebugger(manager, ontology, reasonerFactory);
		bottom = manager.getOWLDataFactory().getOWLNothing();
	}

	public void report(@Nonnull PrintWriter writer, String existingContent) throws OWLException {
		SyntaxObjectRenderer renderer = new SyntaxObjectRenderer(writer);
		renderer.header();
		Set<OWLClass> unsatisfiables = new HashSet<>();
		for (OWLClass clazz : ontology.getClassesInSignature()) {
			assert clazz != null;
			if (!checker.isSatisfiable(clazz) && !clazz.equals(bottom)) {
				unsatisfiables.add(clazz);
			}
		}
		writer.println("<h1>Ontology Debugging Report</h1>");
		writer.println("<br>Ontology: " + NumericEntityEscaper.below(0x20)
				.translate(StringEscapeUtils.escapeXml11(ontology.getOntologyID().toString())) + "<br>");
		if (unsatisfiables.isEmpty()) {
			writer.println("No unsatisfiable classes found<br>");
		} else {
			for (OWLClass unsatisfiable : unsatisfiables) {
				writer.println("<div class='box'>\n");
				writer.println("<h2 class='cl'>");
				unsatisfiable.accept(renderer);
				writer.println("</h2>");
				writer.println("<br>Axioms causing inconsistency:<br>");
				writer.println("<ul>");
				/*
				 * Find the set of support for the inconsistency. This will
				 * return us a collection of axioms
				 */
				try {
					Set<OWLAxiom> sos = debugger.getSOSForInconsistentClass(unsatisfiable);
					for (OWLAxiom axiom : sos) {
						writer.println("<li>");
						axiom.accept(renderer);
						writer.println("</li>");
					}
				} catch (Exception e) {
					writer.println("<li>" + e.getMessage() + "</li>");
				}
				writer.println("</ul>");
				writer.println("</div>\n");
			}
		}

		if (!existingContent.isEmpty()) {
			writer.print(existingContent);
		}

		renderer.footer();
		writer.flush();
		writer.close();
	}
}