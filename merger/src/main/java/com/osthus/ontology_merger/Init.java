/*
 *    Copyright 2017-2021 OSTHUS
 *
 *      https://www.osthus.com
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.osthus.ontology_merger;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Properties;
import java.util.regex.Pattern;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

public class Init {
	private static String version = "x.y.z";

	public static void init(String[] args) throws ParseException, NoSuchFieldException, NoSuchMethodException,
			IllegalAccessException, InvocationTargetException, IOException {

		printTitle();
		printVersion();

		MergerMain.log.info(
				"For logging on debug level, use the command line switch -Dlogback.configurationFile=logback-debug.xml\n");

		Option help = Option.builder("h").longOpt("help").required(false).desc("Show list of command line arguments.")
				.build();
		Option useXMLCatalog = Option.builder("u").longOpt("useXMLCatalog").required(false)
				.desc("Indicates an XML catalog file should be used for IRI mapping.").build();
		Option catalog = Option.builder("cat").longOpt("catalog").required(false).argName("cat").hasArgs()
				.desc("Catalog file to be used when \"useXMLCatalog\" mode enabled.").build();
		Option externalOntology = Option.builder("ont").longOpt("external").required(false).argName("file")
				.valueSeparator().hasArgs().desc("TTL files of external ontologies.").build();
		Option explain = Option.builder("e").longOpt("explain").required(false).desc("Compute explanations.").build();
		Option save = Option.builder("s").longOpt("save").required(false)
				.desc("Save original unchanged ontologies via owlapi for comparison.\r\n").build();
		Option progress = Option.builder("p").longOpt("progress").required(false)
				.desc("Output status message during inferencing (if supported by reasoner).\r\n").build();
		Option merge = Option.builder("mg").longOpt("mergeonly").required(false).desc(
				"Save a merged file that contains all triples of provided ontology files. No inferencing included.")
				.build();
		Option combine = Option.builder("c").longOpt("combine").required(false)
				.desc("Save a combined file that contains all triples of merged files as well as inferred triples.")
				.build();
		Option report = Option.builder("t").longOpt("report").required(false)
				.desc("Create a report of inferencing results for combined file.").build();
		Option reasoner = Option.builder("r").longOpt("reasoner").required(false).argName("r").hasArgs()
				.desc("Select reasoner to use. HermiT or Pellet.").build();
		Option folder = Option.builder("f").longOpt("folder").required(false).hasArg()
				.desc("Specifies the root folder of ontology files as absolute path. E.g.\"C:\\path\\to\\ontologies\"")
				.build();
		Option outFolder = Option.builder("o").longOpt("out").required(false).hasArg()
				.desc("Specifies the output folder as absolute path. E.g.\"C:\\path\\to\\output\"").build();
		Option input = Option.builder("i").longOpt("input").required(false).argName("iri1=file1").valueSeparator()
				.hasArgs().desc("Specify input ontologies (iri) and their local files to merge. ").build();
		Option loadontologies = Option.builder("l").longOpt("load").required(false).argName("iri1").hasArgs().desc(
				"Specify input ontologies (iri). The ontologies will be loaded from the specified folder (-f) or from the local folder. ")
				.build();
		Option inferencingoption = Option.builder().longOpt("inference").required(false).argName("num").hasArgs().desc(
				"Number specifies inferencing option(s) to use. Default is inferencing of subclass hierarchy (=1). Available options:\nSUBCLASS=1\n"
						+ "CLASS_ASSERTION=2,\nDISJOINT_CLASS=3\nEQUIVALENT_CLASS=4\nDATA_PROPERTY_CHARACTERISTIC=5\n"
						+ "EQUIVALENT_DATAPROPERTY=6\nOBJECTPROPERTY_CHARACTERISTIC=7\nEQUIVALENT_OBJECTPROPERTY=8\nINVERSE_OBJECTPROPERTY=9\n"
						+ "PROPERTY_ASSERTION=10\nSUB_DATAPROPERTY=11\nSUB_OBJECTPROPERTY=12")
				.build();

		Options infoOptions = new Options();
		infoOptions.addOption(help);

		Options appOptions = new Options();
		appOptions.addOption(folder);
		appOptions.addOption(useXMLCatalog);
		appOptions.addOption(catalog);
		appOptions.addOption(reasoner);
		appOptions.addOption(input);
		appOptions.addOption(inferencingoption);
		appOptions.addOption(explain);
		appOptions.addOption(save);
		appOptions.addOption(combine);
		appOptions.addOption(merge);
		appOptions.addOption(loadontologies);
		appOptions.addOption(progress);
		appOptions.addOption(report);
		appOptions.addOption(externalOntology);
		appOptions.addOption(outFolder);

		Options allOptions = new Options();
		allOptions.addOption(help);
		allOptions.addOption(folder);
		allOptions.addOption(useXMLCatalog);
		allOptions.addOption(catalog);
		allOptions.addOption(reasoner);
		allOptions.addOption(input);
		allOptions.addOption(inferencingoption);
		allOptions.addOption(explain);
		allOptions.addOption(save);
		allOptions.addOption(combine);
		allOptions.addOption(merge);
		allOptions.addOption(loadontologies);
		allOptions.addOption(progress);
		allOptions.addOption(report);
		allOptions.addOption(externalOntology);
		allOptions.addOption(outFolder);

		CommandLineParser infoParser = new DefaultParser();
		CommandLine cmd = infoParser.parse(infoOptions, args, true);

		if (cmd.hasOption("help")) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("java -jar afo-merger-" + version + ".jar", allOptions, true);
			System.exit(0);
		}

		CommandLineParser parser = new DefaultParser();
		cmd = parser.parse(appOptions, args, true);

		checkParameters(cmd);

		if (cmd.hasOption("reasoner")) {
			MergerMain.reasonerName = cmd.getOptionValue("reasoner").trim().toUpperCase();
		}

		if (cmd.hasOption("input")) {
			MergerMain.inputOntologies = cmd.getOptionValues("input");
			MergerMain.isInput = true;
		}

		if (cmd.hasOption("useXMLCatalog")) {
			MergerMain.useXMLCatalog = true;
		}

		if (cmd.hasOption("catalog")) {
			MergerMain.catalogFile = cmd.getOptionValue("catalog");
		}

		if (cmd.hasOption("load")) {
			MergerMain.inputOntologies = cmd.getOptionValues("load");
			MergerMain.isLoad = true;
		}

		if (cmd.hasOption("inference")) {
			String[] inferencingOptions = cmd.getOptionValues("inference");
			Arrays.sort(inferencingOptions, new Comparator<String>() {

				@Override
				public int compare(String o1, String o2) {
					Integer i1 = Integer.parseInt(o1);
					Integer i2 = Integer.parseInt(o2);
					return i1.compareTo(i2);
				}

			});

			for (int i = 0; i < inferencingOptions.length; i++) {
				if (Integer.valueOf(inferencingOptions[i]) < 1 || Integer.valueOf(inferencingOptions[i]) > 12) {
					throw new IllegalArgumentException("Only inferencing options 1 to 12 are supported.");
				}
				InferencingOption[] possibleValues = InferencingOption.values();
				for (int j = 0; j < possibleValues.length; j++) {
					if (possibleValues[j].toString().equals(inferencingOptions[i])) {
						MergerMain.inferencingOptions.add(possibleValues[j]);
						break;
					}
				}
			}
		} else {
			MergerMain.inferencingOptions.add(InferencingOption.INFERRED_SUBCLASS_HIERARCHY);
		}

		if (cmd.hasOption("explain")) {
			MergerMain.calculateExplanations = true;
		}

		if (cmd.hasOption("save")) {
			MergerMain.saveOriginal = true;
		}

		if (cmd.hasOption("combine")) {
			MergerMain.combineTriples = true;
		}

		if (cmd.hasOption("mergeonly")) {
			MergerMain.mergeOnly = true;
		}

		if (cmd.hasOption("report")) {
			if (!cmd.hasOption("combine")) {
				throw new IllegalArgumentException(
						"Missing combine option. In the current version, reports are only created for combined ontologies.");
			}
			MergerMain.createReport = true;
		}

		if (cmd.hasOption("external")) {
			MergerMain.externalOntologies = cmd.getOptionValues("external");
		}

		if (cmd.hasOption("progress")) {
			MergerMain.showInferencingProgress = true;
		}

		String rootPath = cmd.getOptionValue("folder");

		if (rootPath != null) {
			MergerMain.useSpecificFolder = true;
			if (rootPath.endsWith("\"")) {
				rootPath = rootPath.trim().replace("\"", "");
			}
			if (!rootPath.endsWith("\\")) {
				rootPath = rootPath.trim() + "\\";
			}
			rootPath = rootPath.trim();
			rootPath = rootPath.replaceAll(Pattern.quote(" "), "%20");
			rootPath = rootPath.replaceAll(Pattern.quote("\\\\"), "/");
			rootPath = rootPath.replaceAll(Pattern.quote("\\"), "/");
			if (rootPath != null && !rootPath.isEmpty() && !rootPath.equals(MergerUtil.HOME)) {
				Field homePath = MergerUtil.class.getDeclaredField("HOME");
				homePath.setAccessible(true);
				Method parseMethod = homePath.getType().getMethod("valueOf", new Class[] {
						Object.class
				});
				homePath.set(homePath, parseMethod.invoke(homePath, rootPath));
				MergerMain.log.info("Loading from root path: " + MergerUtil.HOME);
			} else if (MergerMain.inputOntologies == null) {
				MergerMain.log.info("Loading from root path: " + MergerUtil.HOME);
			}
		}

		String outPathName = cmd.getOptionValue("out");

		if (outPathName != null) {
			if (outPathName.endsWith("\"")) {
				outPathName = outPathName.trim().replace("\"", "");
			}
			if (!outPathName.endsWith("\\")) {
				outPathName = outPathName.trim() + "\\";
			}
			outPathName = outPathName.trim();
			outPathName = outPathName.replaceAll(Pattern.quote("\\\\"), "/");
			outPathName = outPathName.replaceAll(Pattern.quote("\\"), "/");
			Path outPath = Paths.get(outPathName);

			MergerMain.log.info("Writing output to folder: " + outPath.toAbsolutePath().toString());

			MergerMain.mergedFile = Paths.get(outPathName + MergerMain.mergedFile.getFileName());
			MergerMain.mergedRealizedFile = Paths.get(outPathName + MergerMain.mergedRealizedFile.getFileName());
			MergerMain.hermitInferredMergedFile = Paths
					.get(outPathName + MergerMain.hermitInferredMergedFile.getFileName());
			MergerMain.hermitInferredMergedCombinedFile = Paths
					.get(outPathName + MergerMain.hermitInferredMergedCombinedFile.getFileName());
			MergerMain.hermitModuleFile = Paths.get(outPathName + MergerMain.hermitModuleFile.getFileName());
			MergerMain.hermitReport = Paths.get(outPathName + MergerMain.hermitReport.getFileName());
			MergerMain.pelletInferredMergedFile = Paths
					.get(outPathName + MergerMain.pelletInferredMergedFile.getFileName());
			MergerMain.pelletInferredMergedCombinedFile = Paths
					.get(outPathName + MergerMain.pelletInferredMergedCombinedFile.getFileName());
			MergerMain.pelletModuleFile = Paths.get(outPathName + MergerMain.pelletModuleFile.getFileName());
			MergerMain.pelletReport = Paths.get(outPathName + MergerMain.pelletReport.getFileName());

			MergerMain.mergedAfoFile = Paths.get(outPathName + MergerMain.mergedAfoFile.getFileName());
			MergerMain.mergedRealizedAfoFile = Paths.get(outPathName + MergerMain.mergedRealizedAfoFile.getFileName());
			MergerMain.hermitInferredMergedAfoFile = Paths
					.get(outPathName + MergerMain.hermitInferredMergedAfoFile.getFileName());
			MergerMain.hermitInferredMergedCombinedAfoFile = Paths
					.get(outPathName + MergerMain.hermitInferredMergedCombinedAfoFile.getFileName());
			MergerMain.hermitModuleAfoFile = Paths.get(outPathName + MergerMain.hermitModuleAfoFile.getFileName());
			MergerMain.pelletInferredMergedAfoFile = Paths
					.get(outPathName + MergerMain.pelletInferredMergedAfoFile.getFileName());
			MergerMain.pelletInferredMergedCombinedAfoFile = Paths
					.get(outPathName + MergerMain.pelletInferredMergedCombinedAfoFile.getFileName());
			MergerMain.pelletModuleAfoFile = Paths.get(outPathName + MergerMain.pelletModuleAfoFile.getFileName());

		}

		if (MergerMain.calculateExplanations || MergerMain.createReport) {
			// remove report of previous execution
			if (MergerMain.reasonerName.equals(MergerMain.HERMIT)) {
				Files.deleteIfExists(MergerMain.hermitReport);
			} else if (MergerMain.reasonerName.equals(MergerMain.PELLET)) {
				Files.deleteIfExists(MergerMain.pelletReport);
			}
		}

		MergerUtil.init();

		System.setProperty("file.encoding", "UTF-8");
		try {
			Field charset = Charset.class.getDeclaredField("defaultCharset");
			charset.setAccessible(true);
			charset.set(null, null);
			if (!Charset.defaultCharset().displayName().equals("UTF-8")) {
				MergerMain.log.info("Current character set: " + Charset.defaultCharset().displayName());
			}
		} catch (RuntimeException e) {
			MergerMain.log.warn("Unable to get current character set information");
		}
	}

	private static void checkParameters(CommandLine cmd) {
		if (cmd.hasOption("input") && cmd.hasOption("load")) {
			MergerMain.log.error("Command switches --input and --load cannot be combined.");
			System.exit(1);
		}

		if ((cmd.hasOption("useXMLCatalog") && !cmd.hasOption("catalog"))
				|| (!cmd.hasOption("useXMLCatalog") && cmd.hasOption("catalog"))) {
			MergerMain.log.error(
					"Command switch --useXMLCatalog necessitates the provision of a --catalog option and vice versa.");
			System.exit(1);
		}

		if (cmd.hasOption("useXMLCatalog") && !cmd.hasOption("input")) {
			MergerMain.log.error(
					"A single input file must be provided (via the \"--input\" parameter) when in \"useXMLCatalog\" mode.");
			System.exit(1);
		}

		if (cmd.hasOption("load") && !cmd.hasOption("folder")) {
			MergerMain.log.error("Command switch --load requires --folder to be specified.");
			System.exit(1);
		}
	}

	public static void printTitle() {
		// @formatter:off
		MergerMain.log.info("\n" + //
				"   ___   ________    __  ___                       ______          __\r\n"
				+ "  / _ | / __/ __ \\  /  |/  /__ _______ ____ ____  /_  __/__  ___  / /\r\n"
				+ " / __ |/ _// /_/ / / /|_/ / -_) __/ _ `/ -_) __/   / / / _ \\/ _ \\/ / \r\n"
				+ "/_/ |_/_/  \\____/ /_/  /_/\\__/_/  \\_, /\\__/_/     /_/  \\___/\\___/_/  \r\n"
				+ "                                 /___/                               \r\n"); //
		// @formatter:on
	}

	private static void printVersion() throws IOException {
		final Properties properties = new Properties();
		properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("merger.properties"));
		version = properties.getProperty("version");
		MergerMain.log.info("version " + version + " © OSTHUS 2017-2022");
	}
}
