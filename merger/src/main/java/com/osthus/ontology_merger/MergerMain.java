/*
 *    Copyright 2017-2022 OSTHUS
 *
 *      https://www.osthus.com
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.osthus.ontology_merger;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.translate.NumericEntityEscaper;
import org.semanticweb.HermiT.Configuration;
import org.semanticweb.HermiT.Reasoner;
import org.semanticweb.HermiT.ReasonerFactory;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.formats.TurtleDocumentFormatFactory;
import org.semanticweb.owlapi.io.StringDocumentTarget;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.MissingImportHandlingStrategy;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLDisjointClassesAxiom;
import org.semanticweb.owlapi.model.OWLDocumentFormat;
import org.semanticweb.owlapi.model.OWLException;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyLoaderConfiguration;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;
import org.semanticweb.owlapi.model.OWLSubClassOfAxiom;
import org.semanticweb.owlapi.reasoner.ConsoleProgressMonitor;
import org.semanticweb.owlapi.reasoner.IndividualNodeSetPolicy;
import org.semanticweb.owlapi.reasoner.InferenceType;
import org.semanticweb.owlapi.reasoner.Node;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.reasoner.OWLReasonerConfiguration;
import org.semanticweb.owlapi.util.InferredAxiomGenerator;
import org.semanticweb.owlapi.util.InferredClassAssertionAxiomGenerator;
import org.semanticweb.owlapi.util.InferredDataPropertyCharacteristicAxiomGenerator;
import org.semanticweb.owlapi.util.InferredDisjointClassesAxiomGenerator;
import org.semanticweb.owlapi.util.InferredEquivalentClassAxiomGenerator;
import org.semanticweb.owlapi.util.InferredEquivalentDataPropertiesAxiomGenerator;
import org.semanticweb.owlapi.util.InferredEquivalentObjectPropertyAxiomGenerator;
import org.semanticweb.owlapi.util.InferredInverseObjectPropertiesAxiomGenerator;
import org.semanticweb.owlapi.util.InferredObjectPropertyCharacteristicAxiomGenerator;
import org.semanticweb.owlapi.util.InferredOntologyGenerator;
import org.semanticweb.owlapi.util.InferredPropertyAssertionGenerator;
import org.semanticweb.owlapi.util.InferredSubClassAxiomGenerator;
import org.semanticweb.owlapi.util.InferredSubDataPropertyAxiomGenerator;
import org.semanticweb.owlapi.util.InferredSubObjectPropertyAxiomGenerator;
import org.semanticweb.owlapi.util.OWLOntologyMerger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.clarkparsia.owlapi.explanation.BlackBoxExplanation;
import com.clarkparsia.owlapi.explanation.HSTExplanationGenerator;
import com.clarkparsia.pellet.owlapiv3.PelletReasoner;
import com.clarkparsia.pellet.owlapiv3.PelletReasonerFactory;

public class MergerMain {
	public static final String HERMIT = "HERMIT";
	public static final String PELLET = "PELLET";

	public static final Logger log = LoggerFactory.getLogger(MergerMain.class);

	public static Path mergedFile = Paths.get("merged.ttl");
	public static Path mergedRealizedFile = Paths.get("merged-realized.ttl");
	public static Path hermitInferredMergedFile = Paths.get("merged-inferred-HermiT.ttl");
	public static Path hermitInferredMergedCombinedFile = Paths.get("merged-combined-HermiT.ttl");
	public static Path hermitModuleFile = Paths.get("modularized-HermiT.ttl");
	public static Path hermitReport = Paths.get("report-HermiT.html");
	public static Path pelletInferredMergedFile = Paths.get("merged-inferred-Pellet.ttl");
	public static Path pelletInferredMergedCombinedFile = Paths.get("merged-combined-Pellet.ttl");
	public static Path pelletModuleFile = Paths.get("modularized-Pellet.ttl");
	public static Path pelletReport = Paths.get("report-Pellet.html");

	public static Path mergedAfoFile = Paths.get("afo-merged.ttl");
	public static Path mergedRealizedAfoFile = Paths.get("afo-merged-realized.ttl");
	public static Path hermitInferredMergedAfoFile = Paths.get("afo-merged-inferred-HermiT.ttl");
	public static Path hermitInferredMergedCombinedAfoFile = Paths.get("afo-combined-HermiT.ttl");
	public static Path hermitModuleAfoFile = Paths.get("afo-modularized-HermiT.ttl");
	public static Path pelletInferredMergedAfoFile = Paths.get("afo-merged-inferred-Pellet.ttl");
	public static Path pelletInferredMergedCombinedAfoFile = Paths.get("afo-combined-Pellet.ttl");
	public static Path pelletModuleAfoFile = Paths.get("afo-modularized-Pellet.ttl");

	private static final IRI mergedIri = IRI.create("http://www.example.com/ontologies/merged");
	private static final IRI mergedRealizedIri = IRI.create("http://www.example.com/ontologies/merged/realized");
	private static final IRI hermitInferredMergedIri = IRI
			.create("http://www.example.com/ontologies/merged/inferred/hermit");
	private static final IRI hermitInferredMergedCombinedIri = IRI
			.create("http://purl.allotrope.org/ontologies/merged/inferred/hermit/combined");
	private static final IRI hermitModuleIri = IRI.create("http://www.example.com/ontologies/modularized/hermit");
	private static final IRI pelletInferredMergedIri = IRI
			.create("http://purl.allotrope.org/ontologies/merged/inferred/pellet");
	private static final IRI pelletInferredMergedCombinedIri = IRI
			.create("http://www.example.com/ontologies/merged/inferred/pellet/combined");
	private static final IRI pelletModuleIri = IRI.create("http://www.example.com/ontologies/modularized/pellet");

	public static String reasonerName = "HERMIT";
	public static boolean calculateMetrics = false;
	public static String[] inputOntologies = null;
	public static boolean isInput = false;
	public static boolean isLoad = false;
	public static LinkedHashSet<InferencingOption> inferencingOptions = new LinkedHashSet<InferencingOption>();
	public static boolean calculateExplanations = false;
	public static boolean saveOriginal = false;
	public static boolean combineTriples = false;
	public static boolean printHierarchy = false;
	public static boolean useGitFolderStructure = false;
	public static boolean useSpecificFolder = false;
	public static Path sparqlDlQuery = null;
	public static IRI modularizeSeed = null;
	public static boolean showInferencingProgress = false;
	public static boolean createReport = false;
	public static boolean extractTaxonomy = false;
	public static boolean realizeMapping = false;
	public static String[] externalOntologies = null;
	public static boolean includeSubclasses = false;
	public static boolean includeSuperclasses = false;
	public static boolean executeFunction = false;
	public static boolean extractDeprecatedTerms = false;
	public static String[] devString = null;
	// The Protege catalog file location if loading an ontology file with
	// related catalog file
	public static String catalogFile = null;
	// An indicator of if we should use an XML catalog file or IRI mapper.
	public static boolean useXMLCatalog = false;
	public static boolean handleDanglingTerms = false;
	public static boolean isStrictHandlingOfBfoAlignment = false;
	public static Path deprecatedPath = null;
	public static boolean mergeOnly = false;

	// Ensure we get a decent prefix map.
	public static PrefixMapper prefixMapper = new PrefixMapper();

	public static void main(String[] args) throws Exception {
		// Safeguard against execution of random code in log4j in case one of
		// the dependencies should still use it
		System.setProperty("log4j.formatMsgNoLookups", "true");

		try {
			Init.init(args);

			OWLDocumentFormat owlDocumentFormat = new TurtleDocumentFormatFactory().createFormat();

			MergeTask mergeTask = null;
			if (inputOntologies == null || inputOntologies.length == 0) {
				log.error("There were no input ontologies specified.");
				System.exit(1);
			}

			OWLOntologyManager manager = OWLManager.createOWLOntologyManager();

			OWLOntologyLoaderConfiguration config = new OWLOntologyLoaderConfiguration().setFollowRedirects(false)
					.setConnectionTimeout(10).setStrict(false)
					.setMissingImportHandlingStrategy(MissingImportHandlingStrategy.THROW_EXCEPTION);
			manager.setOntologyLoaderConfiguration(config);

			// handle user specified ontologies
			if (useXMLCatalog) {
				MergerUtil.initIriMappersForXmlCatalog(manager, catalogFile);
			} else {
				manager = MergerUtil.initIriMappersForUserSpecifiedOntologies(manager, inputOntologies);
			}
			mergeTask = new MergeTask( //
					MergerUtil.userIriMap, //
					mergedIri, //
					mergedFile, //
					mergedRealizedIri, //
					mergedRealizedFile, //
					hermitInferredMergedIri, //
					hermitInferredMergedFile, //
					pelletInferredMergedIri, //
					pelletInferredMergedFile, //
					hermitInferredMergedCombinedIri, //
					hermitInferredMergedCombinedFile, //
					pelletInferredMergedCombinedIri, //
					pelletInferredMergedCombinedFile, //
					hermitModuleIri, //
					hermitModuleFile, //
					pelletModuleIri, //
					pelletModuleFile); //

			try {
				final boolean no_unsatisfiable_classes_found = execute(owlDocumentFormat, manager, mergeTask);
				if (!no_unsatisfiable_classes_found) {
					// HTML report
					if (createReport) {
						StringBuilder sb = new StringBuilder();
						create_report(sb);
					}
					log.error("Done - found unsatisfiable classes.");
					System.exit(2);
				}
			} catch (Exception e) {
				log.error("Done - error during merging and inferencing: " + e.getMessage());
				e.printStackTrace();
				System.exit(1);
			}

			log.info("Done.");
		} catch (Exception e) {
			// Command line output
			log.error("Exception occured:");
			log.error("Error Message: \"" + e.getLocalizedMessage() + "\"");
			log.error("Stacktrace: \n" + StringUtils.join(e.getStackTrace(), "\n"));

			// HTML report
			if (createReport) {
				StringBuilder sb = new StringBuilder();
				sb.append("<p><h1>Exception occured</h1>");
				sb.append("Error Message: <pre>" + NumericEntityEscaper.below(0x20)
						.translate(StringEscapeUtils.escapeXml11(e.getLocalizedMessage())) + "</pre>");
				sb.append("Stacktrace: <br><pre>" + StringUtils.join(e.getStackTrace(), "<br/>") + "</pre></p>");
				create_report(sb);
			}
			System.exit(1);
		}
	}

	/**
	 * Writes a report to an HTML file
	 * 
	 * @param sb
	 *            Object to append the report strings to
	 * @throws IOException
	 */
	private static void create_report(StringBuilder sb) {
		try {
			if (reasonerName.equals(HERMIT)) {
				log.info("...writing report to " + hermitReport);
				if (Files.exists(hermitReport)) {
					Files.write(hermitReport, sb.toString().getBytes(), StandardOpenOption.APPEND);
				} else {
					Files.write(hermitReport, sb.toString().getBytes());
				}
			} else if (reasonerName.equals(PELLET)) {
				log.info("...writing report to " + pelletReport);
				if (Files.exists(pelletReport)) {
					Files.write(pelletReport, sb.toString().getBytes(), StandardOpenOption.APPEND);
				} else {
					Files.write(pelletReport, sb.toString().getBytes());
				}
			}
		} catch (Exception e) {
			log.info("Exception while writing report: " + e.getMessage());
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @param owlDocumentFormat
	 * @param manager
	 * @param mergeTask
	 * @return true if there were no unsatisfiable classes found
	 * @throws Exception
	 */
	private static boolean execute(OWLDocumentFormat owlDocumentFormat, OWLOntologyManager manager, MergeTask mergeTask)
			throws Exception {
		loadOntologies(owlDocumentFormat, manager, mergeTask.getIriMap());

		mergeOntologies(owlDocumentFormat, manager, mergeTask.getOntologyIri(), mergeTask.getFilePath());

		if (mergeOnly) {
			return true;
		}

		boolean unsatisfiable_classes_found = false;

		if (reasonerName.equals(HERMIT)) {
			unsatisfiable_classes_found = inferencingOntologiesUsingHermiT(owlDocumentFormat, manager,
					mergeTask.getOntologyIri(), mergeTask.getHermitInferredOntologyIri(),
					mergeTask.getHermitInferredFilePath());
		} else if (reasonerName.equals(PELLET)) {
			unsatisfiable_classes_found = inferencingOntologiesUsingPellet(owlDocumentFormat, manager,
					mergeTask.getOntologyIri(), mergeTask.getPelletInferredOntologyIri(),
					mergeTask.getPelletInferredFilePath());
		} else {
			throw new IllegalStateException(
					"Supported reasoners are HermiT and Pellet. Unknown reasoner found: " + reasonerName);
		}

		log.info("...combining original and inferred triples");

		if (reasonerName.equals(HERMIT)) {
			mergeOntologies(owlDocumentFormat, manager, mergeTask.getHermitInferredCombinedOntologyIri(),
					mergeTask.getHermitInferredCombinedFilePath());
		} else if (reasonerName.equals(PELLET)) {
			mergeOntologies(owlDocumentFormat, manager, mergeTask.getPelletInferredCombinedOntologyIri(),
					mergeTask.getPelletInferredCombinedFilePath());
		}

		if (createReport && combineTriples) {
			log.info("...creating report.");
			if (reasonerName.equals(HERMIT)) {
				MergerDebugger mergerDebugger = new MergerDebugger(manager,
						manager.getOntology(mergeTask.hermitInferredCombinedOntologyIri),
						getConfiguredHermitReasonerFactory());
				String existingContent = "";
				if (Files.exists(hermitReport)) {
					existingContent = StringUtils.join(Files.readAllLines(hermitReport), "\n");
				}
				PrintWriter printWriter = new PrintWriter(hermitReport.toFile());
				mergerDebugger.report(printWriter, existingContent);
			} else if (reasonerName.equals(PELLET)) {
				MergerDebugger mergerDebugger = new MergerDebugger(manager,
						manager.getOntology(mergeTask.pelletInferredCombinedOntologyIri),
						PelletReasonerFactory.getInstance());

				String existingContent = "";
				if (Files.exists(pelletReport)) {
					existingContent = StringUtils.join(Files.readAllLines(pelletReport), "\n");
				}
				PrintWriter printWriter = new PrintWriter(pelletReport.toFile());
				mergerDebugger.report(printWriter, existingContent);
			}
		}
		return unsatisfiable_classes_found;
	}

	/**
	 * 
	 * @param owlDocumentFormat
	 * @param manager
	 * @param ontologyIri
	 * @param inferredOntologyIri
	 * @param inferredFilePath
	 * @return true if no unsatisfiable classes were found
	 * @throws OWLException
	 * @throws IOException
	 */
	private static boolean inferencingOntologiesUsingHermiT(OWLDocumentFormat owlDocumentFormat,
			OWLOntologyManager manager, IRI ontologyIri, IRI inferredOntologyIri, Path inferredFilePath)
			throws OWLException, IOException {
		log.info("...inferencing class hierarchy using HermiT");
		ReasonerFactory owlReasonerFactory = getConfiguredHermitReasonerFactory();
		OWLReasoner reasoner = owlReasonerFactory.createReasoner(manager.getOntology(ontologyIri));
		log.info("...HermiT Version " + Reasoner.class.getPackage().getImplementationVersion());
		reasoner.precomputeInferences();

		List<InferredAxiomGenerator<? extends OWLAxiom>> gens = determineInferencingOptions();

		OWLOntology infOnt = manager.createOntology(inferredOntologyIri);
		InferredOntologyGenerator iog = new InferredOntologyGenerator(reasoner, gens);
		iog.fillOntology(manager.getOWLDataFactory(), infOnt);

		final boolean result = findUnsatisfiableClasses(reasoner);

		OWLDocumentFormat format = manager.getOntologyFormat(infOnt);
		if (format.isPrefixOWLOntologyFormat()) {
			prefixMapper.addPrefixes(format);
			owlDocumentFormat.asPrefixOWLOntologyFormat().copyPrefixesFrom(prefixMapper.getPrefixes());
			PrefixMapper.deregisterOboPrefix(owlDocumentFormat);
		}

		IRI inferredMergedFileIri = IRI.create(inferredFilePath.toFile().toURI());
		manager.saveOntology(infOnt, owlDocumentFormat, inferredMergedFileIri);
		log.info("...written inferred ontology to " + inferredFilePath.toFile().toURI());

		if (calculateExplanations) {
			explainingInferredAxioms(manager, owlReasonerFactory, reasoner, gens, infOnt);
		}
		return result;
	}

	/**
	 * 
	 * @param reasoner
	 *            The reasoner being used
	 * @return true if no unsatisfiable classes were found
	 * @throws IOException
	 */
	private static boolean findUnsatisfiableClasses(OWLReasoner reasoner) throws IOException {
		Node<OWLClass> bottomNode = reasoner.getUnsatisfiableClasses();
		Set<OWLClass> unsatisfiable = bottomNode.getEntitiesMinusBottom();

		StringBuilder sb = new StringBuilder();
		if (!unsatisfiable.isEmpty()) {
			log.info("...found unsatisfiable classes: ");

			sb.append("<p><h2>Inconsistent Ontology</h2>");
			sb.append("Unsatisfiable classes found: </br>");
			for (OWLClass cls : unsatisfiable) {
				log.info(" " + cls);
				sb.append(NumericEntityEscaper.below(0x20)
						.translate(StringEscapeUtils.escapeXml11(cls.getIRI().toQuotedString())) + "<br/>");
			}
			sb.append("</p>");
		} else {
			log.info("...consistent ontology!");
			sb.append("<p><h2>Consistent Ontology</h2>");
			sb.append("No unsatisfiable classes found </p>");
		}

		if (createReport) {
			if (reasonerName.equals(HERMIT)) {
				if (Files.exists(hermitReport)) {
					Files.write(hermitReport, sb.toString().getBytes(), StandardOpenOption.APPEND);
				} else {
					Files.write(hermitReport, sb.toString().getBytes());
				}
			} else if (reasonerName.equals(PELLET)) {
				if (Files.exists(pelletReport)) {
					Files.write(pelletReport, sb.toString().getBytes(), StandardOpenOption.APPEND);
				} else {
					Files.write(pelletReport, sb.toString().getBytes());
				}
			}
		}
		return unsatisfiable.isEmpty();
	}

	public static ReasonerFactory getConfiguredHermitReasonerFactory() {
		ReasonerFactory owlReasonerFactory = new ReasonerFactory() {
			@Override
			public OWLReasoner createReasoner(OWLOntology ontology, OWLReasonerConfiguration config) {
				Configuration configuration = new Configuration();
				configuration.ignoreUnsupportedDatatypes = true;
				configuration.throwInconsistentOntologyException = false;
				configuration.individualNodeSetPolicy = IndividualNodeSetPolicy.BY_SAME_AS;
				if (showInferencingProgress) {
					ConsoleProgressMonitor progressMonitor = new ConsoleProgressMonitor();
					configuration.reasonerProgressMonitor = progressMonitor;
				}

				return super.createReasoner(ontology, configuration);
			}
		};
		return owlReasonerFactory;
	}

	/**
	 * 
	 * @param owlDocumentFormat
	 * @param manager
	 * @param ontologyIri
	 * @param inferredOntologyIri
	 * @param inferredFilePath
	 * @return true if no unsatisfiable classes were found
	 * @throws OWLOntologyCreationException
	 * @throws OWLOntologyStorageException
	 * @throws IOException
	 */
	private static boolean inferencingOntologiesUsingPellet(OWLDocumentFormat owlDocumentFormat,
			OWLOntologyManager manager, IRI ontologyIri, IRI inferredOntologyIri, Path inferredFilePath)
			throws OWLOntologyCreationException, OWLOntologyStorageException, IOException {
		log.info("...inferencing class hierarchy using Pellet");

		PelletReasonerFactory pelletReasonerFactory = PelletReasonerFactory.getInstance();
		PelletReasoner reasoner = pelletReasonerFactory.createReasoner(manager.getOntology(ontologyIri));
		log.info("..." + reasoner.getReasonerName() + " " + reasoner.getKB().getInfo());

		reasoner.precomputeInferences();

		reasoner.getKB().realize();

		if (printHierarchy) {
			reasoner.getKB().printClassTree();
		}

		List<InferredAxiomGenerator<? extends OWLAxiom>> gens = determineInferencingOptions();

		OWLOntology infOnt = manager.createOntology(inferredOntologyIri);
		InferredOntologyGenerator iog = new InferredOntologyGenerator(reasoner, gens);
		iog.fillOntology(manager.getOWLDataFactory(), infOnt);

		final boolean result = findUnsatisfiableClasses(reasoner);

		OWLDocumentFormat format = manager.getOntologyFormat(infOnt);
		if (format.isPrefixOWLOntologyFormat()) {
			prefixMapper.addPrefixes(format);
			owlDocumentFormat.asPrefixOWLOntologyFormat().copyPrefixesFrom(prefixMapper.getPrefixes());
			PrefixMapper.deregisterOboPrefix(owlDocumentFormat);
		}

		IRI inferredMergedFileIri = IRI.create(inferredFilePath.toFile().toURI());
		manager.saveOntology(infOnt, owlDocumentFormat, inferredMergedFileIri);
		log.info("...written inferred ontology to " + inferredFilePath.toFile().toURI());

		if (calculateExplanations) {
			explainingInferredAxioms(manager, pelletReasonerFactory, reasoner, gens, infOnt);
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	private static void explainingInferredAxioms(OWLOntologyManager manager, Object owlReasonerFactory,
			OWLReasoner reasoner, List<InferredAxiomGenerator<? extends OWLAxiom>> gens, OWLOntology infOnt)
			throws IOException {
		BlackBoxExplanation explain = null;
		if (owlReasonerFactory instanceof ReasonerFactory) {
			log.info("...explaining HermiT inferred axioms");
			explain = new BlackBoxExplanation(infOnt, (ReasonerFactory) owlReasonerFactory, reasoner);
		} else if (owlReasonerFactory instanceof PelletReasonerFactory) {
			log.info("...explaining Pellet inferred axioms");
			explain = new BlackBoxExplanation(infOnt, (PelletReasonerFactory) owlReasonerFactory, reasoner);
		} else {
			throw new IllegalStateException("Unhandled reasoner factory.");
		}
		OWLDataFactory dataFactory = manager.getOWLDataFactory();
		HSTExplanationGenerator multiEx = new HSTExplanationGenerator(explain);

		Set<OWLSubClassOfAxiom> subClass = (Set<OWLSubClassOfAxiom>) gens.get(0).createAxioms(dataFactory, reasoner);

		StringBuilder sb = new StringBuilder();
		sb.append("<p><h2>Explanations from " + reasonerName + " for subclass axioms</h2><br/>");
		for (OWLSubClassOfAxiom ax : subClass) {
			sb.append(NumericEntityEscaper.below(0x20).translate(StringEscapeUtils.escapeXml11(ax.toString())));
			sb.append(NumericEntityEscaper.below(0x20)
					.translate(StringEscapeUtils.escapeXml11(
							(reasoner.isEntailed(ax) ? " is entailed in reasoner." : " is NOT entailed in reasoner.")))
					+ "<br/>");
			sb.append(NumericEntityEscaper.below(0x20).translate(StringEscapeUtils.escapeXml11(ax.toString())));
			sb.append(NumericEntityEscaper.below(0x20).translate(StringEscapeUtils.escapeXml11(
					(infOnt.containsAxiom(ax) ? " is contained in ontology." : " is NOT contained in ontology.")))
					+ "<br/>");
		}

		Set<OWLClass> classes = infOnt.getClassesInSignature();
		if (classes != null && !classes.isEmpty()) {
			sb.append("</p><p><h2>Explanations from " + reasonerName + " for all classes</h2><br/>");

			for (OWLClass cls : classes) {
				sb.append(NumericEntityEscaper.below(0x20).translate(StringEscapeUtils.escapeXml11(cls.toStringID())));
				Set<Set<OWLAxiom>> explanations = multiEx.getExplanations(cls);
				if (explanations != null && !explanations.isEmpty()) {
					// Each explanation is one possible set of axioms that cause
					// the unsatisfiability.
					for (Set<OWLAxiom> explanation : explanations) {
						sb.append("<p>");
						sb.append("axioms causing the unsatisfiability: <br>");
						for (OWLAxiom causingAxiom : explanation) {
							sb.append(NumericEntityEscaper.below(0x20)
									.translate(StringEscapeUtils.escapeXml11(causingAxiom.toString())) + "<br/>");
						}
						sb.append("</p>");
					}
				} else {
					sb.append(" is satisfiable</br>");
				}
			}
			sb.append("</p>");
		}

		if (owlReasonerFactory instanceof ReasonerFactory) {
			if (Files.exists(hermitReport)) {
				Files.write(hermitReport, sb.toString().getBytes(), StandardOpenOption.APPEND);
			} else {
				Files.write(hermitReport, sb.toString().getBytes());
			}
		} else if (owlReasonerFactory instanceof PelletReasonerFactory) {
			if (Files.exists(pelletReport)) {
				Files.write(pelletReport, sb.toString().getBytes(), StandardOpenOption.APPEND);
			} else {
				Files.write(pelletReport, sb.toString().getBytes());
			}
		}
	}

	private static void mergeOntologies(OWLDocumentFormat owlDocumentFormat, OWLOntologyManager manager,
			IRI mergedOntologyIri, Path mergedFilePath)
			throws OWLOntologyCreationException, OWLOntologyStorageException {
		log.info("...merging");
		OWLOntologyMerger merger = new OWLOntologyMerger(manager);
		OWLOntology mergedOntology = merger.createMergedOntology(manager, mergedOntologyIri);
		IRI mergedFileIri = IRI.create(mergedFilePath.toFile().toURI());
		OWLDocumentFormat format = manager.getOntologyFormat(mergedOntology);
		if (format.isPrefixOWLOntologyFormat()) {
			prefixMapper.addPrefixes(format);
			owlDocumentFormat.asPrefixOWLOntologyFormat().copyPrefixesFrom(prefixMapper.getPrefixes());
			PrefixMapper.deregisterOboPrefix(owlDocumentFormat);
		}
		manager.saveOntology(mergedOntology, owlDocumentFormat, mergedFileIri);
	}

	private static void loadOntologies(OWLDocumentFormat owlDocumentFormat, OWLOntologyManager manager,
			LinkedHashMap<IRI, IRI> iriMap) throws OWLOntologyCreationException, OWLOntologyStorageException {
		if (useXMLCatalog) {
			if (inputOntologies.length != 1) {
				throw new IllegalArgumentException(
						"Expected exactly 1 input ontology in \"useXMLCatalog\" mode but received "
								+ inputOntologies.length);
			}
			OWLOntology o = manager.loadOntologyFromOntologyDocument(new File(inputOntologies[0]));
			StringDocumentTarget target = new StringDocumentTarget();
			manager.saveOntology(o, owlDocumentFormat, target);
			log.debug(target.toString());
			if (saveOriginal) {
				// TODO: Implement saveOriginal functionality
				throw new IllegalArgumentException(
						"Attempting to save original in \"useXMLCatalog\" mode which is unimplemented.");
			}
		} else {
			for (Entry<IRI, IRI> entry : iriMap.entrySet()) {
				log.info("...loading " + ((entry.getKey() != null) ? entry.getKey() : "undefined ontology") + " from "
						+ ((entry.getValue() != null) ? entry.getValue() : "undefined source"));
				OWLOntology o = manager.loadOntology(entry.getKey());
				StringDocumentTarget target = new StringDocumentTarget();
				manager.saveOntology(o, owlDocumentFormat, target);
				log.debug(target.toString());
				if (saveOriginal) {
					OWLDocumentFormat format = manager.getOntologyFormat(o);
					if (format.isPrefixOWLOntologyFormat()) {
						owlDocumentFormat.asPrefixOWLOntologyFormat()
								.copyPrefixesFrom(format.asPrefixOWLOntologyFormat());
					}
					String savePath = "";
					if (manager.getIRIMappers().size() == 1
							&& manager.getIRIMappers().iterator().next() instanceof MergerAutoIRIMapper) {
						savePath = "saved-" + Paths
								.get(manager.getIRIMappers().iterator().next().getDocumentIRI(entry.getKey()).toURI())
								.getFileName().toString();
					} else {
						savePath = "saved-" + Paths.get(entry.getValue().toURI()).getFileName().toString();
					}
					manager.saveOntology(o, owlDocumentFormat, IRI.create(Paths.get(savePath).toFile().toURI()));
					log.info("...written original ontology to file " + savePath);
				}
			}
		}
	}

	private static List<InferredAxiomGenerator<? extends OWLAxiom>> determineInferencingOptions() {
		List<InferredAxiomGenerator<? extends OWLAxiom>> gens = new ArrayList<InferredAxiomGenerator<? extends OWLAxiom>>();

		Iterator<InferencingOption> inferencingOptionIterator = inferencingOptions.iterator();
		while (inferencingOptionIterator.hasNext()) {
			InferencingOption inferencingOption = inferencingOptionIterator.next();

			switch (inferencingOption) {
			case INFERRED_SUBCLASS_HIERARCHY:
				gens.add(new InferredSubClassAxiomGenerator()); // rdfs
																// entailment
				break;
			case INFERRED_CLASS_ASSERTION:
				gens.add(new InferredClassAssertionAxiomGenerator()); // rdfs
																		// level
				break;
			case INFERRED_DATA_PROPERTY_CHARACTERISTIC:
				gens.add(new InferredDataPropertyCharacteristicAxiomGenerator()); //
				break;
			case INFERRED_DISJOINT_CLASS:
				// gens.add(new InferredDisjointClassesAxiomGenerator()); // owl
				gens.add(new InferredDisjointClassesAxiomGenerator() {
					boolean precomputed = false;

					@Override
					protected void addAxioms(OWLClass entity, OWLReasoner reasoner, OWLDataFactory dataFactory,
							Set<OWLDisjointClassesAxiom> result) {
						if (!precomputed) {
							reasoner.precomputeInferences(InferenceType.DISJOINT_CLASSES);
							precomputed = true;
						}
						for (OWLClass cls : reasoner.getDisjointClasses(entity).getFlattened()) {
							result.add(dataFactory.getOWLDisjointClassesAxiom(entity, cls));
						}
					}
				});
				break;
			case INFERRED_EQUIVALENT_CLASS:
				gens.add(new InferredEquivalentClassAxiomGenerator()); // owl
				break;
			case INFERRED_EQUIVALENT_DATAPROPERTY:
				gens.add(new InferredEquivalentDataPropertiesAxiomGenerator()); // owl
				break;
			case INFERRED_EQUIVALENT_OBJECTPROPERTY:
				gens.add(new InferredEquivalentObjectPropertyAxiomGenerator()); // owl
				break;
			case INFERRED_INVERSE_OBJECTPROPERTY:
				gens.add(new InferredInverseObjectPropertiesAxiomGenerator()); // owl
				break;
			case INFERRED_OBJECTPROPERTY_CHARACTERISTIC:
				gens.add(new InferredObjectPropertyCharacteristicAxiomGenerator()); // owl
				break;
			case INFERRED_PROPERTY_ASSERTION:
				gens.add(new InferredPropertyAssertionGenerator()); // rdfs
				break;
			case INFERRED_SUBDATAPROPERTY:
				gens.add(new InferredSubDataPropertyAxiomGenerator()); //
				break;
			case INFERRED_SUBOBJECTPROPERTY:
				gens.add(new InferredSubObjectPropertyAxiomGenerator()); //
				break;
			default:
				throw new IllegalStateException("Unknown inference option.");
			}
		}
		return gens;
	}
}
