/*
 *    Copyright 2017-2021 OSTHUS
 *
 *      https://www.osthus.com
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.osthus.ontology_merger;

import java.nio.file.Path;
import java.util.LinkedHashMap;

import org.semanticweb.owlapi.model.IRI;

public class MergeTask {
	LinkedHashMap<IRI, IRI> iriMap = null;
	IRI ontologyIri = null;
	Path filePath = null;
	IRI realizedOntologyIri = null;
	Path realizedFilePath = null;
	IRI hermitInferredOntologyIri = null;
	Path hermitInferredFilePath = null;
	IRI pelletInferredOntologyIri = null;
	Path pelletInferredFilePath = null;
	IRI hermitInferredCombinedOntologyIri = null;
	Path hermitInferredCombinedFilePath = null;
	IRI pelletInferredCombinedOntologyIri = null;
	Path pelletInferredCombinedFilePath = null;
	IRI hermitModuleIri = null;
	Path hermitModuleFilePath = null;
	IRI pelletModuleIri = null;
	Path pelletModuleFilePath = null;

	public MergeTask( //
			LinkedHashMap<IRI, IRI> iriMap, //
			IRI ontologyIri, //
			Path filePath, //
			IRI realizedOntologyIri, //
			Path realizedFilePath, //
			IRI hermitInferredOntologyIri, //
			Path hermitInferredFilePath, //
			IRI pelletInferredOntologyIri, //
			Path pelletInferredFilePath, //
			IRI hermitInferredCombinedOntologyIri, //
			Path hermitInferredCombinedFilePath, //
			IRI pelletInferredCombinedOntologyIri, //
			Path pelletInferredCombinedFilePath, //
			IRI hermitModuleIri, //
			Path hermitModuleFilePath, //
			IRI pelletModuleIri, //
			Path pelletModuleFilePath) //
	{
		super();
		this.iriMap = iriMap;
		this.ontologyIri = ontologyIri;
		this.filePath = filePath;
		this.realizedOntologyIri = realizedOntologyIri;
		this.realizedFilePath = realizedFilePath;
		this.hermitInferredOntologyIri = hermitInferredOntologyIri;
		this.hermitInferredFilePath = hermitInferredFilePath;
		this.pelletInferredOntologyIri = pelletInferredOntologyIri;
		this.pelletInferredFilePath = pelletInferredFilePath;
		this.hermitInferredCombinedOntologyIri = hermitInferredCombinedOntologyIri;
		this.hermitInferredCombinedFilePath = hermitInferredCombinedFilePath;
		this.pelletInferredCombinedOntologyIri = pelletInferredCombinedOntologyIri;
		this.pelletInferredCombinedFilePath = pelletInferredCombinedFilePath;
		this.hermitModuleIri = hermitModuleIri;
		this.hermitModuleFilePath = hermitModuleFilePath;
		this.pelletModuleIri = pelletModuleIri;
		this.pelletModuleFilePath = pelletModuleFilePath;
	}

	public LinkedHashMap<IRI, IRI> getIriMap() {
		return iriMap;
	}

	public void setIriMap(LinkedHashMap<IRI, IRI> iriMap) {
		this.iriMap = iriMap;
	}

	public IRI getOntologyIri() {
		return ontologyIri;
	}

	public void setOntologyIri(IRI ontologyIri) {
		this.ontologyIri = ontologyIri;
	}

	public IRI getHermitInferredOntologyIri() {
		return hermitInferredOntologyIri;
	}

	public void setHermitInferredOntologyIri(IRI hermitInferredOntologyIri) {
		this.hermitInferredOntologyIri = hermitInferredOntologyIri;
	}

	public Path getHermitInferredFilePath() {
		return hermitInferredFilePath;
	}

	public void setHermitInferredFilePath(Path hermitInferredFilePath) {
		this.hermitInferredFilePath = hermitInferredFilePath;
	}

	public IRI getPelletInferredOntologyIri() {
		return pelletInferredOntologyIri;
	}

	public void setPelletInferredOntologyIri(IRI pelletInferredOntologyIri) {
		this.pelletInferredOntologyIri = pelletInferredOntologyIri;
	}

	public Path getPelletInferredFilePath() {
		return pelletInferredFilePath;
	}

	public void setPelletInferredFilePath(Path pelletInferredFilePath) {
		this.pelletInferredFilePath = pelletInferredFilePath;
	}

	public Path getFilePath() {
		return filePath;
	}

	public void setFilePath(Path filePath) {
		this.filePath = filePath;
	}

	public IRI getHermitInferredCombinedOntologyIri() {
		return hermitInferredCombinedOntologyIri;
	}

	public void setHermitInferredCombinedOntologyIri(IRI hermitInferredCombinedOntologyIri) {
		this.hermitInferredCombinedOntologyIri = hermitInferredCombinedOntologyIri;
	}

	public Path getHermitInferredCombinedFilePath() {
		return hermitInferredCombinedFilePath;
	}

	public void setHermitInferredCombinedFilePath(Path hermitInferredCombinedFilePath) {
		this.hermitInferredCombinedFilePath = hermitInferredCombinedFilePath;
	}

	public IRI getPelletInferredCombinedOntologyIri() {
		return pelletInferredCombinedOntologyIri;
	}

	public void setPelletInferredCombinedOntologyIri(IRI pelletInferredCombinedOntologyIri) {
		this.pelletInferredCombinedOntologyIri = pelletInferredCombinedOntologyIri;
	}

	public Path getPelletInferredCombinedFilePath() {
		return pelletInferredCombinedFilePath;
	}

	public void setPelletInferredCombinedFilePath(Path pelletInferredCombinedFilePath) {
		this.pelletInferredCombinedFilePath = pelletInferredCombinedFilePath;
	}

	public IRI getHermitModuleIri() {
		return hermitModuleIri;
	}

	public void setHermitModuleIri(IRI hermitModuleIri) {
		this.hermitModuleIri = hermitModuleIri;
	}

	public Path getHermitModuleFilePath() {
		return hermitModuleFilePath;
	}

	public void setHermitModuleFilePath(Path hermitModuleFilePath) {
		this.hermitModuleFilePath = hermitModuleFilePath;
	}

	public IRI getPelletModuleIri() {
		return pelletModuleIri;
	}

	public void setPelletModuleIri(IRI pelletModuleIri) {
		this.pelletModuleIri = pelletModuleIri;
	}

	public Path getPelletModuleFilePath() {
		return pelletModuleFilePath;
	}

	public void setPelletModuleFilePath(Path pelletModuleFilePath) {
		this.pelletModuleFilePath = pelletModuleFilePath;
	}

	public IRI getRealizedOntologyIri() {
		return realizedOntologyIri;
	}

	public void setRealizedOntologyIri(IRI realizedOntologyIri) {
		this.realizedOntologyIri = realizedOntologyIri;
	}

	public Path getRealizedFilePath() {
		return realizedFilePath;
	}

	public void setRealizedFilePath(Path realizedFilePath) {
		this.realizedFilePath = realizedFilePath;
	}
}
