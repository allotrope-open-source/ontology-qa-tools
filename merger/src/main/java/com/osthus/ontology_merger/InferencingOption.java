/*
 *    Copyright 2017-2021 OSTHUS
 *
 *      https://www.osthus.com
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.osthus.ontology_merger;

public enum InferencingOption {
	INFERRED_SUBCLASS_HIERARCHY("1"), //
	INFERRED_CLASS_ASSERTION("2"), //
	INFERRED_DISJOINT_CLASS("3"), //
	INFERRED_EQUIVALENT_CLASS("4"), //
	INFERRED_DATA_PROPERTY_CHARACTERISTIC("5"), //
	INFERRED_EQUIVALENT_DATAPROPERTY("6"), //
	INFERRED_OBJECTPROPERTY_CHARACTERISTIC("7"), //
	INFERRED_EQUIVALENT_OBJECTPROPERTY("8"), //
	INFERRED_INVERSE_OBJECTPROPERTY("9"), //
	INFERRED_PROPERTY_ASSERTION("10"), //
	INFERRED_SUBDATAPROPERTY("11"), //
	INFERRED_SUBOBJECTPROPERTY("12"); //

	private final String text;

	private InferencingOption(final String text) {
		this.text = text;
	}

	@Override
	public String toString() {
		return text;
	}
}