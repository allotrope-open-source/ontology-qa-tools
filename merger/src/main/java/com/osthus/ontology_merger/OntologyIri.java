/*
 *    Copyright 2017-2021 OSTHUS
 *
 *      https://www.osthus.com
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.osthus.ontology_merger;

import org.semanticweb.owlapi.model.IRI;

public class OntologyIri {
	public static IRI AF_COMMON = IRI.create("http://purl.allotrope.org/ontologies/common");
	public static IRI AF_COMMON_QUALIFIER = IRI.create("http://purl.allotrope.org/ontologies/common/qualifier");
	public static IRI AF_EQUIPMENT = IRI.create("http://purl.allotrope.org/ontologies/equipment");
	public static IRI AF_MATERIAL = IRI.create("http://purl.allotrope.org/ontologies/material");
	public static IRI AF_PROCESS = IRI.create("http://purl.allotrope.org/ontologies/process");
	public static IRI AF_RESULT = IRI.create("http://purl.allotrope.org/ontologies/result");
	public static IRI AF_PROPERTY = IRI.create("http://purl.allotrope.org/ontologies/property");
	public static IRI AF_EQUIPMENT_CODELIST_001 = IRI
			.create("http://purl.allotrope.org/ontologies/equipment/codelist/001");
	public static IRI AF_EQUIPMENT_CODELIST_002 = IRI
			.create("http://purl.allotrope.org/ontologies/equipment/codelist/002");
	public static IRI AF_EQUIPMENT_CODELIST_003 = IRI
			.create("http://purl.allotrope.org/ontologies/equipment/codelist/003");
	public static IRI AF_EQUIPMENT_CODELIST_004 = IRI
			.create("http://purl.allotrope.org/ontologies/equipment/codelist/004");
	public static IRI AF_EQUIPMENT_CODELIST_005 = IRI
			.create("http://purl.allotrope.org/ontologies/equipment/codelist/005");
	public static IRI AF_EQUIPMENT_CODELIST_006 = IRI
			.create("http://purl.allotrope.org/ontologies/equipment/codelist/006");

	public static IRI ADF_AUDIT = IRI.create("http://purl.allotrope.org/ontologies/audit");
	public static IRI ADF_DC = IRI.create("http://purl.allotrope.org/ontologies/datacube");
	public static IRI ADF_DC_HDF = IRI.create("http://purl.allotrope.org/ontologies/datacube-hdf-map");
	public static IRI ADF_DC_SHAPES = IRI.create("http://purl.allotrope.org/shapes/datacube");
	public static IRI ADF_CUBE = IRI.create("http://purl.org/linked-data/cube");
	public static IRI ADF_DP = IRI.create("http://purl.allotrope.org/ontologies/datapackage");
	public static IRI ADF_GRAPH = IRI.create("http://purl.allotrope.org/ontologies/graph");

	public static IRI AF_BP = IRI.create("http://purl.allotrope.org/ontologies/blueprint");
	public static IRI DCT = IRI.create("http://purl.org/dc/terms/");
	public static IRI FOAF = IRI.create("http://xmlns.com/foaf/0.1/");
	public static IRI HDF = IRI.create("http://purl.allotrope.org/ontologies/hdf5/1.8#");
	public static IRI HDF_SHAPES = IRI.create("http://purl.allotrope.org/shapes/hdf");
	public static IRI ORE = IRI.create("http://www.openarchives.org/ore/terms/");
	public static IRI ORG = IRI.create("http://www.w3.org/ns/org#");
	public static IRI PAV = IRI.create("http://purl.org/pav/");
	public static IRI PROV = IRI.create("http://www.w3.org/ns/prov-o#");
	public static IRI QUDT_DIM = IRI.create("http://qudt.org/1.1/schema/dimension");
	public static IRI QUDT_QUANTITY = IRI.create("http://qudt.org/1.1/schema/quantity");
	public static IRI QUDT = IRI.create("http://qudt.org/1.1/schema/qudt");
	public static IRI QUDT_DIM_UNIT = IRI.create("http://qudt.org/1.1/vocab/dimensionalunit");
	public static IRI QUDT_UNIT = IRI.create("http://qudt.org/1.1/vocab/unit");
	public static IRI QUDT_EXT = IRI.create("http://purl.allotrope.org/ontology/qudt-ext");
	public static IRI QUDT_SHAPES = IRI.create("http://purl.allotrope.org/shapes/qudt");
	public static IRI SKOS = IRI.create("http://www.w3.org/2004/02/skos/core");
	public static IRI TIME = IRI.create("http://www.w3.org/2006/time");
	public static IRI TIMEZONE = IRI.create("http://www.w3.org/2006/timezone");
}
