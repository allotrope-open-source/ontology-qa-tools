/*
 *    Copyright 2017-2025 Cencora
 *
 *      https://www.cencora.com/
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.osthus.ontology_qa_common;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Vector;

import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.SKOS;
import org.apache.jena.vocabulary.OWL;

public class TriplesCollector {

	// Nodes without parents
	private HashSet<String> headNodes;
	// child <parents>
	private HashMap<String, HashSet<String>> nodeParents;
	// All deprecated nodes
	private HashSet<String> deprecatedNodes;
	// All prefLabels
	HashMap<String, String> prefLabels;
	// All definitions
	HashMap<String, String> definitions;

	// Collects all direct child-parent relationships, parsing away anonymous
	// nodes
	public TriplesCollector(Vector<TripleInfo> tripleInfos) {
		super();
		System.out.println("Collecting parents...");
		HashMap<String, HashSet<String>> allChildParents = new HashMap<String, HashSet<String>>();
		HashSet<String> purls = new HashSet<String>();

		// Result containers
		headNodes = new HashSet<String>();
		nodeParents = new HashMap<String, HashSet<String>>();
		deprecatedNodes = new HashSet<String>();
		prefLabels = new HashMap<String, String>();
		definitions = new HashMap<String, String>();

		// Collect all object - subject relationships
		for (TripleInfo tripleInfo : tripleInfos) {
			if (allChildParents.containsKey(tripleInfo.subject)) {
				allChildParents.get(tripleInfo.subject).add(tripleInfo.object());
			} else {
				HashSet<String> children = new HashSet<String>();
				children.add(tripleInfo.object());
				allChildParents.put(tripleInfo.subject, children);
			}
			if (isInterestingPurl(tripleInfo.subject)) {
				purls.add(tripleInfo.subject);
				Property predicate = tripleInfo.statement.getPredicate();
				if (predicate.equals(OWL.deprecated)) {
					deprecatedNodes.add(tripleInfo.subject);
				} else if (predicate.equals(SKOS.prefLabel)) {
					prefLabels.put(tripleInfo.subject, tripleInfo.object());
				} else if (predicate.equals(SKOS.definition)) {
					definitions.put(tripleInfo.subject, tripleInfo.object());
				}
			}
		}

		System.out.println("Parsing away anonymous nodes...");

		for (String node : purls) {
			Queue<String> nodesToProcess = new LinkedList<String>();
			HashSet<String> visited = new HashSet<String>();
			nodesToProcess.add(node);
			HashSet<String> parentNodes = new HashSet<String>();
			/*
			 * TODO create a debug mode? final String testme = "AFE_0000224"; if
			 * (node.contains(testme)) { System.out.println("XXXXX"); }
			 */
			while (!nodesToProcess.isEmpty()) {
				final String currentNode = nodesToProcess.remove();
				/*
				 * TODO if (node.contains(testme)) { System.out.println("XXXXX " + currentNode);
				 * }
				 */

				// Make sure that we don't visit the same node twice
				if (visited.contains(currentNode)) {
					continue;
				}
				visited.add(currentNode);

				if (allChildParents.containsKey(currentNode)) {
					for (final String parentNode : allChildParents.get(currentNode)) {
						/*
						 * TODO if (node.contains(testme)) { System.out.println("YYYY " + parentNode); }
						 */
						if (isInterestingPurl(parentNode)) {
							parentNodes.add(parentNode);
						} else {
							nodesToProcess.add(parentNode);
						}
					}
				}
			}

			if (parentNodes.isEmpty()) {
				headNodes.add(node);
			} else {
				nodeParents.put(node, new HashSet<String>());
				for (final String parentNode : parentNodes) {
					nodeParents.get(node).add(parentNode);
				}
			}
		}
		System.out.println("Collecting triples done.");
	}

	public String findPreflabel(final String purl) {
		if (getPrefLabels().containsKey(purl)) {
			return getPrefLabels().get(purl);
		}
		return "";
	}

	private boolean isInterestingPurl(final String name) {
		return name.startsWith("http") && name.contains("://");
	}

	// Get all triples from the ttl files in the given list of catalog entries
	// We might to filter those before creating an object, so it's static and
	// not automatically done by the constructor.
	public static Vector<TripleInfo> getAllTriples(HashMap<String, StringPair> catalogs, final String basePath) {
		System.out.println("Fetching all triples...");
		Vector<TripleInfo> result = new Vector<TripleInfo>();
		final int basePathLength = basePath.replace("\\\\", "/").length() + 1;

		for (final String ttlFile : catalogs.keySet()) {
			final StringPair ontologyAndGroup = catalogs.get(ttlFile);
			try {
				for (Statement statement : RDFDataMgr.loadModel(ttlFile).listStatements().toList()) {
					result.add(new TripleInfo(statement, ttlFile.substring(basePathLength), ontologyAndGroup.first,
							ontologyAndGroup.second));
				} // statements
			} catch (Exception e) {
				e.printStackTrace();
				System.exit(1);
			}
		} // files
		return result;
	}

	public final HashSet<String> getDeprecatedNodes() {
		return deprecatedNodes;
	}

	public final HashSet<String> getHeadNodes() {
		return headNodes;
	}

	public final HashMap<String, String> getPrefLabels() {
		return prefLabels;
	}

	public final HashMap<String, String> getDefinitions() {
		return definitions;
	}

	public final HashMap<String, HashSet<String>> getNodeParents() {
		return nodeParents;
	}
}