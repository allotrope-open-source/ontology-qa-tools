/*
 *    Copyright 2017-2025 Cencora
 *
 *      https://www.cencora.com/
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.osthus.ontology_qa_common;

import org.apache.jena.graph.Node;
import org.apache.jena.rdf.model.Statement;

// Representation of a triple and its filename etc.
public class TripleInfo {

	public final Statement statement;
	public final String subject;
	public final String predicate;
	private final Node objectNode;
	public final String filename;
	public final String ontology;
	public final String group;
	
	public static String domainKeyword = "";

	public TripleInfo(Statement statement, final String filename, final String ontology, final String group) {
		this.statement = statement;
		this.subject = statement.getSubject().toString();
		this.predicate = statement.getPredicate().toString();
		this.objectNode = statement.asTriple().getObject();
		this.filename = filename;
		this.ontology = ontology;
		this.group = group;
	}
	
	public String object() {
		return objectNode.toString();
	}
	
	public Node objectNode() {
		return objectNode;
	}
	
	public boolean isInDomain() throws TripleInfoException {
		return isInDomain(subject) || isInDomain(object());
	}

	public static boolean isInDomain(final String iri) throws TripleInfoException {
		if (domainKeyword.isEmpty()) {
			throw new TripleInfoException("TripleInfo.domainKeyword has not been initialized");
		}
		return iri.toLowerCase().contains(domainKeyword);
	}

	public boolean isAnonymous() {
		return !subject.contains("/");
	}
	
	public String ontology() {
		return ontology;
	}
}