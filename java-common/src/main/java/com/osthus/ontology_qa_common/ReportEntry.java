/*
 *    Copyright 2017-2021 OSTHUS
 *
 *      https://www.osthus.com
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.osthus.ontology_qa_common;

// An entry in a TSV-formatted report
public class ReportEntry {

	private String message;
	private String filename;
	private String purl;
	private String prefLabel;
	private String information;

	public ReportEntry(String message, String filename, String purl, String prefLabel, String information) {
		super();
		this.message = message;
		this.filename = filename;
		this.purl = purl;
		this.prefLabel = prefLabel;
		// Get rid of Windows-formatted newlines, since that would mess up our
		// tsv
		this.information = information.replace("\r\n", "").replace("\n", "").trim();
	}

	public static String TSVHeader() {
		return "Error\tFile\tPURL\tPrefLabel\tInformation";
	}

	@Override
	public String toString() {
		return message + "\t" + filename + "\t" + purl + "\t" + prefLabel + "\t" + information;
	}
}