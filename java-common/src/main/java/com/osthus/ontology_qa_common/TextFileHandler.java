/*
 *    Copyright 2017-2021 OSTHUS
 *
 *      https://www.osthus.com
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.osthus.ontology_qa_common;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Collection;
import java.util.Vector;

public class TextFileHandler {

	// Read lines of a UTF-8 encoded text file into the given String collection
	public static void read(Collection<String> result, String filename, boolean cleanupLines) {
		BufferedReader br = null;
		try {
			String line = "";
			br = new BufferedReader(new InputStreamReader(new FileInputStream(filename), "UTF-8"));
			while ((line = br.readLine()) != null) {
				if (cleanupLines) {
					line = line.trim();
					if (!line.isEmpty() && !line.startsWith("#")) {
						result.add(line);
					}
				} else {
					result.add(line);
				}
			}
		} catch (IOException e) {
			System.out.println("ERROR! Unable to read the entries from " + filename);
			e.printStackTrace();
			System.exit(1);
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					System.out.println("ERROR! Unable to close the reader for " + filename);
					e.printStackTrace();
					System.exit(1);
				}
			}
		}
		if (result.isEmpty()) {
			System.out.println("ERROR! Nothing listed in " + filename);
			System.exit(1);
		}
	}
	
	// Read lines of a UTF-8 encoded text file and return as String, separated by \n.
	public static String read(String filename) {
		StringBuilder result = new StringBuilder();
		Vector<String> lines = new Vector<>();
		read(lines, filename, true);
		for (String line :lines) {
			result.append(line).append("\n");
		}	
		return result.toString();
	}

	
	// Writes the given string into a UTF-8 encoded file with the gicen filename
	public static void write(String writeme, String filename) {
		BufferedWriter bw = null;
		try {
			bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filename), "UTF-8"));
			bw.write(writeme);
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("ERROR! Unable to write to " + filename);
			e.printStackTrace();
			System.exit(1);
		} finally {
			if (bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					System.out.println("ERROR! Unable to close the writer for " + filename);
					e.printStackTrace();
					System.exit(1);
				}
			}
		}
	}
}
