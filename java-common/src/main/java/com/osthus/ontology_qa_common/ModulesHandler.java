/*
 *    Copyright 2017-2021 OSTHUS
 *
 *      https://www.osthus.com
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.osthus.ontology_qa_common;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.TreeMap;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class ModulesHandler {

	private static final String IMPORT_GROUP = "import";
	private static final Pattern ID_PATTERN = Pattern.compile("^id(\\d+)$");

	private static int errors;

	/**
	 * Collects information about ontology files from catalog xml files
	 * 
	 * @param basePath
	 *            - walks all file in this path, including its subdirectories
	 * @param groupsToExclude
	 *            - exclude these groups from the result
	 * @return a collection of entries with information about all catalog
	 *         entries
	 * @throws Exception
	 *             - the file did not satisfy the specification or an exception
	 *             occurred while parsing the xml
	 */
	public static Map<String, CatalogEntry> read(String basePath, HashSet<String> groupsToExclude) throws Exception {
		errors = 0;
		// Initialize the directories to walk
		Queue<File> directories = new LinkedList<File>();
		File dir = new File(basePath);
		if (dir.isDirectory()) {
			directories.add(dir);
		} else {
			System.out.println("Base path " + basePath + " is not a directory!");
			System.exit(1);
		}
		System.out.println("Reading catalogs ...");

		// Collect all catalog files
		Queue<File> catalogFiles = new LinkedList<File>();
		while (!directories.isEmpty()) {
			dir = directories.remove();
			for (File file : dir.listFiles()) {
				if (file.isDirectory()) {
					directories.add(file);
				} else if (file.getName().toLowerCase().endsWith(".xml")) {
					catalogFiles.add(file);
				}
			}
		}

		// Now read the catalogs

		Map<String, CatalogEntry> result = new HashMap<>();
		try {
			result = readCatalogs(catalogFiles, groupsToExclude);
		} catch (Exception e) {
			handleError(e.getMessage());
		}
		if (errors > 0) {
			System.out.println("Reading catalogs completed. Found " + errors + " error(s).");
			throw new Exception("Found " + errors + " error(s) while reading catalogs.");
		} else {
			System.out.println("Reading catalogs completed.");
		}
		return result;
	}

	/**
	 * Writes the given catalog entries into a tsv file.
	 * 
	 * @param catalogs
	 *            the catalogs to write
	 * @param headers
	 *            header names for the columns to include
	 * @param printHeaders
	 *            if false, the header row will be omitted
	 * @param outputFile
	 *            the file to write the catalog entries to
	 * @throws Exception
	 *             if a header doesn't match a CatalogEntry member variable name
	 *             or the file isn't writable
	 */
	public static void writeToTSV(Collection<CatalogEntry> catalogs, Vector<String> headers, boolean printHeaders,
			String outputFile) throws Exception {
		System.out.println("Writing " + catalogs.size() + " catalog entries...");
		errors = 0;
		String result = "";

		// Add headers
		if (printHeaders) {
			if (headers.isEmpty()) {
				result += "catalogFile\t";
				result += "groupName\t";
				result += "id\t";
				result += "name\t";
				result += "ttlFile";
			} else {
				for (int i = 0; i < headers.size(); ++i) {
					final String header = headers.get(i);
					result += header;
					if (i < headers.size() - 1) {
						result += "\t";
					}
				}
			}
			result += "\n";
		}

		// Dump lines into a set to squash duplicates
		HashSet<String> lines = new HashSet<String>();

		// Add content
		for (CatalogEntry entry : catalogs) {
			for (String row : entry.asTsvRows(headers)) {
				lines.add(row);
			}
		}

		System.out.println("We have " + lines.size() + " unique entries.");
		for (String line : lines) {
			result += line + "\n";
		}

		// Now write to file
		TextFileHandler.write(result, outputFile);
		System.out.println("Catalog entries have been written to: " + outputFile);
	}

	/**
	 * Collects the canonical paths of all files ending in '.ttl' from
	 * baseDirectory and all its subdirectories. If a directory contains a
	 * .ttlignore file, exclude those files from the list
	 * 
	 * @param baseDirectory
	 *            the directory to walk
	 * @return a set of all file paths found
	 */
	public static HashSet<String> collectTurtleFiles(final File baseDirectory) {
		if (!baseDirectory.isDirectory()) {
			System.out.println("Base path '" + baseDirectory.getAbsolutePath() + "' is not a directory!");
			System.exit(1);
		}
		// Read files to parse
		HashSet<String> result = new HashSet<String>();

		// Ignore files from .ttlignore
		HashSet<String> ignorelist = new HashSet<String>();

		try {
			System.out.println("Colleting files ...");

			// Collect all catalog and TTL files
			Queue<File> directories = new LinkedList<File>();
			HashSet<File> ttlFiles = new HashSet<File>();
			directories.add(baseDirectory);
			while (!directories.isEmpty()) {
				try {
					ignorelist.clear();
					final File dir = directories.remove();
					System.out.println("Walking dir: " + dir.getCanonicalPath());
					for (File file : dir.listFiles()) {
						if (file.isDirectory()) {
							directories.add(file);
						} else if (file.getName().equals(".ttlignore")) {
							TextFileHandler.read(ignorelist, file.getCanonicalPath(), true);
						}
					}
					for (File file : dir.listFiles()) {
						if (file.getName().toLowerCase().endsWith(".ttl") && !ignorelist.contains(file.getName())) {
							ttlFiles.add(file);
						}
					}
				} catch (IOException e) {
					e.printStackTrace();
					System.out.println(
							"Exception occurred while walking directory " + baseDirectory.getCanonicalPath() + " .");
				}
			}

			// Now parse the files
			for (File file : ttlFiles) {
				try {
					result.add(file.getCanonicalPath());
				} catch (IOException e) {
					e.printStackTrace();
					System.out.println("Exception occurred while adding " + file + " to file list.");
				}
			}
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		return result;
	}

	/**
	 * Contains information about a catalog entry
	 *
	 */
	public static class CatalogEntry {

		/** The catalog file that this is from */
		public final String catalogFile;
		/** The group name that the URI is in */
		public String groupName;
		/**
		 * The same ontology file can be registered with multiple name URIs.
		 * This variable lists the id/name pairs.
		 */
		public Map<String, Integer> namesAndIds;
		/** The .ttl file assigned to the ontology in this catalog group */
		public final String ttlFile;

		public CatalogEntry(String catalogFile, String groupName, int id, String name, String ttlFile) {
			super();
			this.catalogFile = catalogFile;
			this.groupName = groupName;
			this.namesAndIds = new TreeMap<>();
			this.namesAndIds.put(name, Integer.valueOf(id));
			this.ttlFile = ttlFile;
		}

		/**
		 * Returns the member variables as a tab-separated string.
		 * 
		 * @param headers
		 *            defines with variables will be included in which order.
		 *            Use empty list to include all entries.
		 * @return the member variables as tab-separated strings, filtered if
		 *         headers were provided. Multiple rows if available.
		 * @throws Exception
		 *             if a header doesn't match a member variable name.
		 */
		protected List<String> asTsvRows(Vector<String> headers) throws Exception {
			List<String> result = new ArrayList<>();

			for (String name : namesAndIds.keySet()) {
				String row = "";
				if (headers.isEmpty()) {
					row += this.catalogFile + "\t";
					row += this.groupName + "\t";
					row += namesAndIds.get(name) + "\t";
					row += name + "\t";
					row += this.ttlFile;
				} else {
					for (int i = 0; i < headers.size(); ++i) {
						final String header = headers.get(i);
						if (header.equals("catalogFile")) {
							row += this.catalogFile;
						} else if (header.equals("groupName")) {
							row += this.groupName;
						} else if (header.equals("id")) {
							row += namesAndIds.get(name);
						} else if (header.equals("name")) {
							row += name;
						} else if (header.equals("ttlFile")) {
							row += this.ttlFile;
						} else {
							throw new Exception("Illegal header: " + header);
						}
						if (i < headers.size() - 1) {
							row += "\t";
						}
					}
				}
				result.add(row);
			}
			return result;
		}

	}

	/**
	 * Adds an entry to the given result collection for each uri in the given
	 * group
	 * 
	 * @param result
	 *            - the collection to add the entries to. Maps URIs to catalog
	 *            entries
	 * @param group
	 *            - the group node to parse
	 * @param path
	 *            - the canonical path to the current catalog's directory
	 * @param catalogPath
	 *            - the canonical path to the current catalog
	 */
	private static void readGroup(Map<String, CatalogEntry> result, Element group, final String path,
			final String catalogPath) {
		final String groupId = group.getAttribute("id");
		NodeList subnodes = group.getChildNodes();
		for (int j = 0; j < subnodes.getLength(); ++j) {
			Node node = subnodes.item(j);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				Element element = (Element) node;
				// Check URIs and add them to the list.
				if (element.getTagName().equals("uri")) {
					try {
						final String idString = element.getAttribute("id");
						if (idString.isEmpty()) {
							handleError("URI " + element.getTagName() + " in group " + groupId + " has no ID");
							continue;
						}

						final String name = element.getAttribute("name");
						if (name.isEmpty()) {
							handleError("URI " + idString + " in group " + groupId + " has no name");
							continue;
						}
						final String uri = new File(path + "/" + element.getAttribute("uri")).getCanonicalPath();

						int id = -1;
						Matcher matcher = ID_PATTERN.matcher(idString);
						if (matcher.find()) {
							id = Integer.parseInt(matcher.group(1));
						}

						if (result.containsKey(uri)) {
							CatalogEntry existing = result.get(uri);

							if (!existing.groupName.equals(groupId)) {
								// Replace imports with something more
								// meaningful
								if (existing.groupName.equals(IMPORT_GROUP)) {
									existing.groupName = groupId;
								} else if (!groupId.equals(IMPORT_GROUP)) {
									// Fail on group conflicts
									String errorMessage = "Group conflict for file " + existing.ttlFile + "!";
									errorMessage += "\n- Catalog " + existing.catalogFile
											+ " is assigning it to group '" + existing.groupName + "'";
									errorMessage += "\n- Catalog " + catalogPath + " is assigning it to group '"
											+ groupId + '"';
									handleError(errorMessage);
								}
							} else {
								if (existing.namesAndIds.containsKey(name)) {
									Integer oldId = existing.namesAndIds.get(name);
									if (id > -1 && oldId.intValue() > id) {
										existing.namesAndIds.put(name, Integer.valueOf(id));
									}
								} else {
									existing.namesAndIds.put(name, Integer.valueOf(id));
								}
							}
						} else {
							result.put(uri, new CatalogEntry(catalogPath, groupId, Integer.valueOf(id), name, uri));
						}
					} catch (IOException e) {
						e.printStackTrace();
						handleError("Error reading from catalog " + catalogPath);
					}
				} else {
					handleError("Group " + groupId + " has a subnode " + element.getTagName() + " that is not a uri");
				}
			}
		}

	}

	/**
	 * Collects an CatalogEntry collection from the given list of catalog xml
	 * files. Expects that the files contain "group" elements, which in turn
	 * contain "uri" elements.
	 * 
	 * @param catalogFiles
	 *            - list of xml catalog files
	 * @param groupsToExclude
	 *            - exclude these groups from the result
	 * @return a collection of CatalogEntry objects
	 */
	private static Map<String, CatalogEntry> readCatalogs(Queue<File> catalogFiles, HashSet<String> groupsToExclude) {
		Map<String, CatalogEntry> result = new HashMap<>();
		while (!catalogFiles.isEmpty()) {
			try {
				File catalog = catalogFiles.remove();
				final String path = catalog.getParentFile().getCanonicalPath();
				System.out.println(" - Reading " + catalog.getCanonicalPath());
				DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder dBuilder;
				Document doc;

				dBuilder = dbFactory.newDocumentBuilder();
				doc = dBuilder.parse(catalog);
				NodeList nodes = doc.getDocumentElement().getChildNodes();
				// Bootstrap the nodes to parse
				Queue<Node> elementsToParse = new LinkedList<Node>();
				for (int i = 0; i < nodes.getLength(); ++i) {
					Node node = nodes.item(i);
					elementsToParse.add(node);
				}

				HashSet<String> existingGroups = new HashSet<String>();

				// Now parse the whole DOM tree
				while (!elementsToParse.isEmpty()) {
					Node node = elementsToParse.remove();
					boolean isGroup = false;
					if (node.getNodeType() == Node.ELEMENT_NODE) {
						Element element = (Element) node;
						// Check URIs and add them to the list.
						if (element.getTagName().equals("group")) {
							isGroup = true;
							final String groupId = element.getAttribute("id");
							if (!groupsToExclude.contains(groupId)) {
								if (existingGroups.contains(groupId)) {
									handleError("Duplicate group '" + groupId + "' in " + catalog.getAbsolutePath());
								}
								readGroup(result, element, path, catalog.getCanonicalPath());
								existingGroups.add(groupId);
							}
						} else if (element.getTagName().equals("uri")) {
							handleError("URI " + element.getAttribute("id") + " without group!");
						}
					}
					// Collect subnodes to parse
					if (!isGroup) {
						NodeList subnodes = node.getChildNodes();
						for (int j = 0; j < subnodes.getLength(); ++j) {
							elementsToParse.add(subnodes.item(j));
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				System.exit(1);
			}
		}
		return result;
	}

	private static void handleError(String message) {
		++errors;
		System.out.println("ERROR! " + message);
	}

}
