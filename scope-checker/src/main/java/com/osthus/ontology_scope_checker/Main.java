package com.osthus.ontology_scope_checker;

/*
 *    Copyright 2017-2018 OSTHUS
 *
 *      http://www.allotrope-framework-architect.com
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.util.StringTokenizer;
import java.util.Vector;

import org.apache.jena.atlas.json.JSON;
import org.apache.jena.atlas.json.JsonObject;
import org.apache.jena.atlas.json.JsonValue;

import com.osthus.ontology_qa_common.ModulesHandler;
import com.osthus.ontology_qa_common.ReportEntry;
import com.osthus.ontology_qa_common.StringPair;
import com.osthus.ontology_qa_common.TextFileHandler;
import com.osthus.ontology_qa_common.TripleInfo;
import com.osthus.ontology_qa_common.TripleInfoException;
import com.osthus.ontology_qa_common.TriplesCollector;

public class Main {

	static final String OWL_ANNOTATION_PROPERTY_KEY = "http://www.w3.org/2002/07/owl#AnnotationProperty";
	static final String OWL_DATATYPE_PROPERTY_KEY = "http://www.w3.org/2002/07/owl#DatatypeProperty";
	static final String OWL_OBJECT_PROPERTY_KEY = "http://www.w3.org/2002/07/owl#ObjectProperty";
	static final String OWL_ONTOLOGY_KEY = "http://www.w3.org/2002/07/owl#Ontology";
	static final String SKOS_DEFINITION_KEY = "http://www.w3.org/2004/02/skos/core#definition";

	static HashSet<String> groupsToExclude;
	static HashSet<ReportEntry> errors;
	static HashSet<ReportEntry> warnings;

	/**
	 * This tool will run checks to make sure that a node's parents are at least
	 * within the same scope.
	 * 
	 * @param args
	 *            <ontology-path> <JSON-file-with-required-properties>
	 *            <TSV-file-with-maturity-levels> <output-TSV-file>
	 */
	public static void main(String[] args) {
		System.out.println(
				"같같같같같같같같같같같�\n�  AFO Scope Checker  �\n�                     �\n같같같같같같같같같같같�\n");
		System.out.println(
				"This tool will check that a node's parents are at least within the same scope, and that is't not globally dangling.");
		if (args.length != 4) {
			handleError("Wrong number of arguments. Expected 4 but got " + args.length);
			return;
		}

		groupsToExclude = new HashSet<String>();

		// Nodes assigned to a higher numerical value may not contain parent
		// nodes at a lower numerical value
		// TODO read from JSON spec
		HashMap<String, Integer> hierarchy = new HashMap<String, Integer>();
		hierarchy.put("internal", new Integer(1000));
		hierarchy.put("external", new Integer(1000));
		hierarchy.put("public", new Integer(5));
		hierarchy.put("candidate", new Integer(4));
		hierarchy.put("submission", new Integer(3));
		hierarchy.put("deprecated", new Integer(1));
		hierarchy.put("rejected", new Integer(0));

		errors = new HashSet<ReportEntry>();
		warnings = new HashSet<ReportEntry>();

		String result = ReportEntry.TSVHeader();

		final String ontologyPath = args[0];
		checkDirectoryExists(new File(ontologyPath));

		final String requiredPropertiesFile = args[1];
		checkFileExists(requiredPropertiesFile);

		final String groupsTSVFile = args[2];
		checkFileExists(groupsTSVFile);

		final String outputFile = args[3];
		checkDirectoryExists(new File(outputFile).getAbsoluteFile().getParentFile());

		try {
			System.out.println("Reading spec...");
			// Get files from JSON
			JsonObject spec = JSON.read(requiredPropertiesFile);
			final String specDir = new File(requiredPropertiesFile).getParent();
			final String approvedSourcesFile = specDir + "/"
					+ spec.get("lists").getAsObject().get("approved-sources").getAsString().value();
			checkFileExists(approvedSourcesFile);
			final String properNamesFile = specDir + "/"
					+ spec.get("lists").getAsObject().get("proper-names").getAsString().value();
			checkFileExists(properNamesFile);

			// Configure namespaces
			TripleInfo.domainKeyword = spec.get("domain-keyword").getAsString().value();
			// TODO TripleInfo.taxonomyPURL =
			// spec.get("taxonomy-purl").getAsString().value();

			// Read check levels from JSON
			for (JsonValue entry : spec.get("excludes").getAsArray()) {
				groupsToExclude.add(entry.getAsString().value());
			}

			// Read catalogs and squash duplicates to save time
			HashMap<String, StringPair> catalogs = new HashMap<String, StringPair>();
			for (ModulesHandler.CatalogEntry catalog : ModulesHandler.read(ontologyPath, groupsToExclude)) {
				catalogs.put(catalog.ttlFile, new StringPair(catalog.name, catalog.groupName));
			}
			for (String filename : catalogs.keySet()) {
				checkFileExists(filename);
			}

			// TODO temporary restriction, remove later.

			HashSet<String> nodesFromTSV = new HashSet<String>();
			// Read group info from TSV file
			HashMap<String, String> purlGroups = new HashMap<String, String>();
			Vector<String> groupsRaw = new Vector<String>();
			TextFileHandler.read(groupsRaw, groupsTSVFile);
			for (final String line : groupsRaw) {
				StringTokenizer labelTokenzier = new StringTokenizer(line, "\t");
				if (labelTokenzier.countTokens() <= 2) {
					final String purl = labelTokenzier.nextToken();
					// Datacube entries are handled by the AFT project, so they
					// are not our ticket
					if (purl.startsWith("http://purl.allotrope.org/ontologies/datacube#")) {
						continue;
					}
					nodesFromTSV.add(purl);
					if (labelTokenzier.hasMoreTokens()) {
						purlGroups.put(purl, labelTokenzier.nextToken());
					} else {
						// TODO quick and dirty
						purlGroups.put(purl, "submission");
					}
				} else {
					errors.add(new ReportEntry("Specification error", groupsTSVFile, "", "",
							"Line has more than 2 columns: " + line));
				}
			}

			// Get all triples and then filter for skos:definition
			final Vector<TripleInfo> allTripleInfos = TriplesCollector.getAllTriples(catalogs, ontologyPath);
			HashMap<String, TripleInfo> tripleInfos = new HashMap<String, TripleInfo>();

			// Some quick excludes for nodes that are always fine as parents
			HashSet<String> excludedParents = new HashSet<String>();

			for (TripleInfo info : allTripleInfos) {
				// The skos:definition is where the main definition of the node
				// is
				if (info.predicate.equals(SKOS_DEFINITION_KEY)) {
					if (tripleInfos.containsKey(info.subject)) {
						final TripleInfo existingInfo = tripleInfos.get(info.subject);
						if (!existingInfo.group.equals("external") && !existingInfo.group.equals(info.group)) {
							if (info.group.equals("external")) {
								tripleInfos.put(info.subject, info);
							} else {
								errors.add(new ReportEntry("Inconsistent maturity level for skos:definition",
										info.filename, info.subject, "",
										info.subject + " is both in " + info.group + " and " + existingInfo.group));
							}
						}
					}
					tripleInfos.put(info.subject, info);
				} else if (info.object.equals(OWL_OBJECT_PROPERTY_KEY) || info.object.equals(OWL_DATATYPE_PROPERTY_KEY)
						|| info.object.equals(OWL_ANNOTATION_PROPERTY_KEY) || info.object.equals(OWL_ONTOLOGY_KEY)) {
					excludedParents.add(info.subject);
				}
			}

			// Use the triples to add missing groups that were not found in the
			// TSV file
			// Anything that's not explicitly set as a "candidate" is deemed to
			// be "submission" level
			for (final String key : tripleInfos.keySet()) {
				final TripleInfo info = tripleInfos.get(key);
				if (!purlGroups.containsKey(key) && hierarchy.containsKey(info.group)) {
					if (info.group.equals("candidate")) {
						purlGroups.put(key, info.group);
					} else {
						purlGroups.put(key, "submission");
					}
				}
			}

			TriplesCollector collector = new TriplesCollector(allTripleInfos);

			System.out.println("Checking for danglers...");
			HashSet<String> danglers = collector.getHeadNodes();
			for (final String dangler : danglers) {
				if (!TripleInfo.isInDomain(dangler)) {
					continue;
				}
				String fileLocation = "";
				if (tripleInfos.containsKey(dangler)) {
					fileLocation = tripleInfos.get(dangler).filename;
				} else {
					fileLocation = findFileLocation(dangler, allTripleInfos);
				}
				warnings.add(
						new ReportEntry("Dangling node", fileLocation, dangler, collector.findPreflabel(dangler), ""));
			}

			System.out.println("Looking for scope violations...");

			HashMap<String, HashSet<String>> nodeParents = collector.getNodeParents();
			// TODO nodeParents for (final String child : nodeParents.keySet())
			// {
			// Restrict to TSV members for now
			for (final String child : nodesFromTSV) {
				// We don't check deprecated entries
				if (collector.getDeprecatedNodes().contains(child)) {
					continue;
				}

				// We don't check third-party nodes
				if (!TripleInfo.isInDomain(child)) {
					continue;
				}

				// Collect some info to be added to any errors/warnings
				String fileLocation = "";
				if (tripleInfos.containsKey(child)) {
					fileLocation = tripleInfos.get(child).filename;
				} else {
					fileLocation = findFileLocation(child, allTripleInfos);
				}

				// TODO quick and dirty
				final String childGroup = getGroup(child, purlGroups, collector);
				final Integer childLevel = getLevel(hierarchy, childGroup);

				// Now check for unknown parents and any remaining danglers
				if (!nodeParents.containsKey(child)) {
					if (fileLocation.isEmpty() && !childGroup.equals("rejected")) {
						errors.add(new ReportEntry("Node not found in Turtle files", fileLocation, child, "",
								"Level: " + childGroup));
					}
					continue;
				}
				// We already reported the danglers above, so we just continue
				if (!nodeParents.containsKey(child)) {
					continue;
				}

				boolean hasNonDeprecated = false;

				for (final String parent : nodeParents.get(child)) {
					// Special check for deprecated
					if (!collector.getDeprecatedNodes().contains(parent)) {
						hasNonDeprecated = true;
					}

					// TODO temporary exclusion
					if (!nodesFromTSV.contains(parent)) {
						continue;
					}
					if (excludedParents.contains(parent)) {
						// We don't check for properties etc.
						continue;
					}
					String parentPrefLabel = "";
					if (collector.getPrefLabels().containsKey(parent)) {
						parentPrefLabel = collector.getPrefLabels().get(parent);
					}

					// Special check for deprecated
					if (collector.getDeprecatedNodes().contains(parent)) {
						errors.add(new ReportEntry("Deprecated parent for " + childGroup + " node", fileLocation, child,
								collector.findPreflabel(child),
								"Deprecated parent " + parentPrefLabel + " with IRI " + parent));
						continue;
					}

					final String parentGroup = getGroup(parent, purlGroups, collector);
					final Integer parentLevel = getLevel(hierarchy, parentGroup);
					if (childLevel > parentLevel) {
						errors.add(new ReportEntry("Immature parent for " + childGroup + " node", fileLocation, child,
								collector.findPreflabel(child),
								"Parent " + parentPrefLabel + " at level '" + parentGroup + "', IRI is " + parent));
					}
				}

				// Report nodes with only deprecated parents
				if (!hasNonDeprecated) {
					errors.add(new ReportEntry("All parents are deprecated for " + childGroup + " node", fileLocation,
							child, collector.findPreflabel(child), ""));

				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}

		// Now print the results
		for (ReportEntry error : errors) {
			result += "\n" + error;
		}

		for (ReportEntry warning : warnings) {
			result += "\nWarning: " + warning;
		}

		TextFileHandler.write(result, outputFile);

		System.out.println(
				"Scope checker completed with " + errors.size() + " error(s) and " + warnings.size() + " warning(s).");
		System.exit(errors.isEmpty() ? 0 : 1);
	}

	// Helper function to make sure that a file exists
	private static void checkFileExists(String fileToCheck) {
		File f = new File(fileToCheck);
		if (!f.exists() || f.isDirectory()) {
			handleError(fileToCheck + " does not exist!");
		}
	}

	// Helper function to make sure that a directory exists
	private static void checkDirectoryExists(File f) {
		if (!f.exists() || !f.isDirectory()) {
			handleError(f.getAbsolutePath() + " does not exist or is not a directory!");
		}
	}

	// Find a file that ths given purl is defined in as a subject
	private static String findFileLocation(final String purl, final Vector<TripleInfo> tripleInfos) {
		for (final TripleInfo info : tripleInfos) {
			if (info.subject.equals(purl)) {
				return info.filename;
			}
		}
		return "";
	}

	// Get a the group for the given node from the map
	// All non-Allotrope nodes are considered public-level
	// All other unknown nodes are considered submission-level
	private static String getGroup(final String node, HashMap<String, String> purlGroups,
			final TriplesCollector collector) {
		try {
			if (!TripleInfo.isInDomain(node)) {
				return "public";
			}
		} catch (TripleInfoException e) {
			handleError("You must define the key 'domain-keyword' in the JSON specification file.");
		}
		if (purlGroups.containsKey(node)) {
			return purlGroups.get(node);
		}
		warnings.add(new ReportEntry("No maturity level found", "", node, collector.findPreflabel(node),
				"No maturity level in either the tsv or our ttl files. Assuming 'submission'"));
		return "submission"; // TODO hard-coded
	}

	// Gets the assigned numerical hierarchy level for the given group
	private static Integer getLevel(HashMap<String, Integer> hierarchy, final String group) {
		if (hierarchy.containsKey(group)) {
			return hierarchy.get(group);
		}
		warnings.add(new ReportEntry("No hierarchy level found", "", group, "",
				"No hierarchy level defined by the checker for this group. Assuming 0 (= rejected)"));
		return new Integer(0);
	}

	public static void handleError(String errorMessage) {
		System.out.println("\nError: " + errorMessage);
		System.out.println(
				"\nUsage: java -jar afo-scope-checker.jar <ontology-path> <JSON-file-with-specifications> <TSV-file-with-maturity-levels> <output-TSV-file>\n");
		System.out.println(" - ontology-path                  Point to the base directory of the ontology");
		System.out.println(" - JSON-file-with-specifications  Point to specification filename");
		System.out.println(" - TSV-file-with-maturity-levels  Expects 2 columns: IRI + maturity level.");
		System.out.println(" - output-TSV-file                Path to the TSV file to write the output to");
		System.exit(1);
	}
}
