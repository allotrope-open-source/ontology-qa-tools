package com.osthus.ontology_styleguide_checker;

/*
 *    Copyright 2017-2021 OSTHUS
 *
 *      https://www.osthus.com
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import org.apache.jena.atlas.json.JsonArray;
import org.apache.jena.atlas.json.JsonObject;
import org.apache.jena.atlas.json.JsonValue;

import com.osthus.ontology_qa_common.TripleInfo;

/**
 * Information about file or node requirements for checking metadata or
 * definition source restrictions.
 */
public class RequirementList {
	/// For definition checks, whether the source is allowed
	public boolean isAllowed;
	/// For definition checks, an optional hint to add to the report to help
	/// ontology editors resolve the issue
	public String hint;
	/// These requirements must be fulfilled by any node that has been added to
	/// the check
	public List<Requirement> nodeRequirements;
	/// These requirements must be satisfied by any file that has been added to
	/// the check
	public List<Requirement> fileRequirements;

	public class Requirement {
		/// An optional hint to add to the report to help ontology editors
		/// resolve the issue
		public String hint;
		// Subjects are tested independently. If this is not empty, one of its
		// members needs to be present.
		public HashSet<String> subjects;
		// If this is not empty, one of its members needs to be present. If a
		// predicate is present, there can be object-related restrictions.
		public HashSet<String> predicates;
		// Objects are only tested if a predicate is present
		public HashSet<String> objects;
		// The objects needs to be of this type
		public String objectType;
		// Regex defining the object string's format
		public Pattern objectRegex;

		// IRIS
		Set<TripleInfo> pendingNodeChecks;

		HashMap<String, HashSet<String>> transitiveChecks;

		public Requirement() {
			hint = "";
			this.subjects = new HashSet<String>();
			this.predicates = new HashSet<String>();
			this.objects = new HashSet<String>();
			this.objectType = "";
			this.objectRegex = null;
			transitiveChecks = new HashMap<String, HashSet<String>>();
			pendingNodeChecks = new HashSet<>();
		}

		@Override
		public String toString() {
			return "Requirement [hint=" + hint + ", subject=" + subjects + ", predicate=" + predicates + ", object="
					+ objects + ", object_type=" + objectType + ", object_regex=" + objectRegex + "]";
		}
	}

	/// Constructor for metadata checks
	public RequirementList() {
		this(true, "");
	}

	/// Constructor for definition source checks
	public RequirementList(boolean isAllowed, String hint) {
		this.isAllowed = isAllowed;
		this.hint = hint;
		nodeRequirements = new ArrayList<>();
		fileRequirements = new ArrayList<>();
	}

	/**
	 * Reads the requirements that each checked node must satisfy. Does some
	 * validity checks on the specification.
	 * 
	 * @param requirements
	 *            Specification
	 */
	public void addNodeRequirements(JsonArray requirements) {
		addRequirements(requirements, nodeRequirements);

		for (Requirement requirement : nodeRequirements) {
			// Nodes can't have transitive checks
			if (!requirement.transitiveChecks.isEmpty()) {
				Main.handleError(
						"Transitive checks are not supported for node requirements. Rule info:\n\t " + requirement);
			}
			if (requirement.predicates.isEmpty()) {
				Main.handleError("Node requirement has to define a predicate. Rule info:\n\t " + requirement);
			}
		}
	}

	/**
	 * Reads the requirements that each checked file must satisfy. Does some
	 * validity checks on the specification.
	 * 
	 * @param requirements
	 *            Specification
	 */
	public void addFileRequirements(JsonArray requirements) {
		addRequirements(requirements, fileRequirements);
	}

	// Helper function for reading the spec
	private void addRequirements(JsonArray input, List<Requirement> result) {
		for (JsonValue requirementValue : input) {
			JsonObject requirementJson = requirementValue.getAsObject();
			Requirement requirement = new Requirement();
			if (requirementJson.hasKey("hint")) {
				requirement.hint = requirementJson.get("hint").getAsString().value();
			}

			if (requirementJson.hasKey("subject")) {
				for (JsonValue entry : requirementJson.get("subject").getAsArray()) {
					requirement.subjects.add(entry.getAsString().value());
				}
			}
			if (requirementJson.hasKey("predicate")) {
				for (JsonValue entry : requirementJson.get("predicate").getAsArray()) {
					requirement.predicates.add(entry.getAsString().value());
				}

				if (requirementJson.hasKey("object")) {
					for (JsonValue entry : requirementJson.get("object").getAsArray()) {
						requirement.objects.add(entry.getAsString().value());
					}
				}
				if (requirementJson.hasKey("object_type")) {
					requirement.objectType = requirementJson.get("object_type").getAsString().value();
				}
				if (requirementJson.hasKey("object_regex")) {
					requirement.objectRegex = Pattern
							.compile(requirementJson.get("object_regex").getAsString().value());
				}
				if (requirementJson.hasKey("object_transitive")) {
					requirement.transitiveChecks.put(requirementJson.get("object_transitive").getAsString().value(),
							new HashSet<String>(requirement.predicates));

				}
			}
			if (requirement.objects.isEmpty()) {
				if (requirement.objectRegex != null && !requirement.objectType.isEmpty()) {
					Main.handleError(
							"Requirement can only define one of 'object', 'object_type', 'object_regex'. Rule info:\n\t "
									+ requirement);
				}
			} else {
				if (requirement.objectRegex != null || !requirement.objectType.isEmpty()) {
					Main.handleError(
							"Requirement can only define one of 'object', 'object_type', 'object_regex'. Rule info:\n\t "
									+ requirement);
				}
			}
			result.add(requirement);
		}
	}

}
