package com.osthus.ontology_styleguide_checker;

/*
 *    Copyright 2017-2025 Cencora
 *
 *      https://www.cencora.com/
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.jena.atlas.json.JSON;
import org.apache.jena.atlas.json.JsonArray;
import org.apache.jena.atlas.json.JsonObject;
import org.apache.jena.atlas.json.JsonValue;
import org.apache.jena.graph.Node;
import org.apache.jena.graph.Triple;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.riot.RDFDataMgr;

import com.osthus.ontology_qa_common.ModulesHandler;
import com.osthus.ontology_qa_common.ModulesHandler.CatalogEntry;
import com.osthus.ontology_qa_common.ReportEntry;
import com.osthus.ontology_qa_common.StringPair;
import com.osthus.ontology_qa_common.TextFileHandler;
import com.osthus.ontology_qa_common.TripleInfo;
import com.osthus.ontology_qa_common.TripleInfoException;
import com.osthus.ontology_qa_common.TriplesCollector;
import com.osthus.ontology_styleguide_checker.RequirementList.Requirement;

public class Main {

	// TODO AFO_DEFINED_CLASS_KEY is obsolete - add a check to make sure we
	// won't have any.
	static final String AFO_DEFINED_CLASS_KEY = "http://purl.allotrope.org/ontologies/property#AFX_0002798";

	static final String DCT_REPLACES_KEY = "http://purl.org/dc/terms/replaces";

	static final String OWL_ANNOTATION_PROPERTY_KEY = "http://www.w3.org/2002/07/owl#AnnotationProperty";
	static final String OWL_CLASS_KEY = "http://www.w3.org/2002/07/owl#Class";
	static final String OWL_DATATYPE_PROPERTY_KEY = "http://www.w3.org/2002/07/owl#DatatypeProperty";
	static final String OWL_DEPRECATED_KEY = "http://www.w3.org/2002/07/owl#deprecated";
	static final String OWL_NAMED_INDIVIDUAL_KEY = "http://www.w3.org/2002/07/owl#NamedIndividual";
	static final String OWL_OBJECT_PROPERTY_KEY = "http://www.w3.org/2002/07/owl#ObjectProperty";
	static final String OWL_RESTRICTION_KEY = "http://www.w3.org/2002/07/owl#Restriction";
	static final String OWL_EQUIVALENT_CLASS_KEY = "http://www.w3.org/2002/07/owl#equivalentClass";
	static final String OWL_INTERSECTION_OF_KEY = "http://www.w3.org/2002/07/owl#intersectionOf";

	static final String RDFS_LABEL_KEY = "http://www.w3.org/2000/01/rdf-schema#label";
	static final String RDFS_SUB_CLASS_OF_KEY = "http://www.w3.org/2000/01/rdf-schema#subClassOf";
	// For getting an ontology
	static final String RDF_TYPE_KEY = "http://www.w3.org/1999/02/22-rdf-syntax-ns#type";

	static final String SKOS_DEFINITION_KEY = "http://www.w3.org/2004/02/skos/core#definition";
	static final String SKOS_PREFLABEL_KEY = "http://www.w3.org/2004/02/skos/core#prefLabel";

	static HashSet<String> groupsToCheck;
	static HashSet<String> groupsToExclude;
	static HashSet<ReportEntry> errors;
	static HashSet<ReportEntry> warnings;

	/**
	 * This tool will run checks to help ensure style guide conformity.
	 * 
	 * @param args <ontology-path> <JSON-file-with-required-properties> <level>
	 *             <output-TSV-file>
	 */
	public static void main(String[] args) {
		System.out.println(
				"°°°°°°°°°°°°°°°°°°°°°°°°°°°°\n°  AFO Styleguide Checker  °\n°                          °\n°°°°°°°°°°°°°°°°°°°°°°°°°°°°\n");
		System.out.println("This tool will run checks to help conform to our style guide.");
		if (args.length != 4) {
			handleError("Wrong number of arguments. Expected 4 but got " + args.length);
			return;
		}

		groupsToCheck = new HashSet<String>();
		groupsToExclude = new HashSet<String>();
		errors = new HashSet<ReportEntry>();
		warnings = new HashSet<ReportEntry>();

		String result = ReportEntry.TSVHeader();

		final String ontologyPath = args[0];
		checkDirectoryExists(new File(ontologyPath));

		final String requiredPropertiesFile = args[1];
		checkFileExists(requiredPropertiesFile);

		final String outputFile = args[3];
		checkDirectoryExists(new File(outputFile).getParentFile());

		try {
			System.out.println("Reading spec...");
			// Get files from JSON
			JsonObject spec = JSON.read(requiredPropertiesFile);
			final String specDir = new File(requiredPropertiesFile).getParent();

			// Configure namespaces
			TripleInfo.domainKeyword = spec.get("domain-keyword").getAsString().value();

			// Read check levels from JSON
			if (spec.get("levels").getAsObject().hasKey(args[2])) {
				for (JsonValue entry : spec.get("levels").getAsObject().get(args[2]).getAsArray()) {
					groupsToCheck.add(entry.getAsString().value());
				}
			} else {
				handleError("Unknown check level '" + args[2] + "'. Aborting.");
			}
			for (JsonValue entry : spec.get("excludes").getAsArray()) {
				groupsToExclude.add(entry.getAsString().value());
			}

			// Read catalogs and squash duplicates to save time
			HashMap<String, StringPair> catalogs = new HashMap<String, StringPair>();
			Map<String, CatalogEntry> modules = ModulesHandler.read(ontologyPath, groupsToExclude);
			for (String catalogName : modules.keySet()) {
				ModulesHandler.CatalogEntry catalog = modules.get(catalogName);
				catalogs.put(catalog.ttlFile, new StringPair(catalogName, catalog.groupName));
			}
			for (String filename : catalogs.keySet()) {
				checkFileExists(filename);
			}

			final Vector<TripleInfo> tripleInfos = TriplesCollector.getAllTriples(catalogs, ontologyPath);
			System.out.println("Found " + tripleInfos.size() + " triples");
			final TriplesCollector collector = new TriplesCollector(tripleInfos);
			for (TripleInfo tripleInfo : tripleInfos) {
				if (!needsChecking(tripleInfo.group) || !tripleInfo.isInDomain()) {
					continue;
				}
				if (tripleInfo.object().substring(1, tripleInfo.object().length() - 1).isEmpty()) {
					errors.add(new ReportEntry("Empty object!", tripleInfo.filename, tripleInfo.subject,
							collector.findPreflabel(tripleInfo.subject), ""));
					continue;
				}
			}

			// Now run the checks
			try {
				if (spec.hasKey("check-sources")) {
					checkSources(ontologyPath, specDir, spec.get("check-sources").getAsObject(), tripleInfos,
							collector);
				}

				// Syntax check collection
				if (spec.hasKey("check-case")) {
					for (JsonValue entry : spec.get("check-case").getAsArray()) {
						checkCase(tripleInfos, collector, specDir, entry.getAsObject());
					}
				}

				if (spec.hasKey("check-regex")) {
					for (JsonValue entry : spec.get("check-regex").getAsArray()) {
						checkRegex(tripleInfos, collector, entry.getAsObject());
					}
				}

				// Required predicates
				if (spec.hasKey("check-required-predicates")) {
					for (JsonValue entry : spec.get("check-required-predicates").getAsArray()) {
						checkRequiredPredicates(tripleInfos, collector, entry.getAsObject());
					}
				}

				if (spec.hasKey("check-duplicates")) {
					for (JsonValue entry : spec.get("check-duplicates").getAsArray()) {
						checkDuplicates(tripleInfos, collector, entry.getAsObject());
					}
				}
				if (spec.hasKey("check-multiples")) {
					checkMultiples(tripleInfos, spec.get("check-multiples").getAsObject(), collector);
				}
				if (spec.hasKey("check-punning")) {
					for (JsonValue entry : spec.get("check-punning").getAsArray()) {
						checkPunning(tripleInfos, collector, entry.getAsObject());
					}
				}
				if (spec.hasKey("check-taxonomy")) {
					checkTaxonomy(tripleInfos, collector, spec.get("check-taxonomy").getAsString().value());
				}
				if (spec.hasKey("check-deprecated")) {
					checkDeprecated(tripleInfos, collector);
				}
				if (spec.hasKey("check-metadata")) {
					checkMetadata(ontologyPath, catalogs, spec.get("check-metadata").getAsArray());
				}
				if (spec.hasKey("check-aristotelian")) {
					checkAristotelian(tripleInfos, collector);
				}
			} catch (TripleInfoException te) {
				handleError("You must define 'domain-keyword' and 'taxonomy-purl' in the JSON specification file.");
			}

		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}

		// Now print the results
		for (ReportEntry error : errors) {
			result += "\n" + error;
		}

		for (ReportEntry warning : warnings) {
			result += "\nWarning: " + warning;
		}

		TextFileHandler.write(result, outputFile);
		System.out.println("Output written to " + outputFile);

		System.out.println("Style guide checker completed with " + errors.size() + " error(s) and " + warnings.size()
				+ " warning(s).");
		System.exit(errors.isEmpty() ? 0 : 1);
	}

	// *********************************************************
	// *************** General *********************************
	// *********************************************************

	private static boolean needsChecking(String group) {
		return groupsToCheck.contains(group);
	}

	// Check for missing required predicates and if given, an object value too.
	// Can be restricted to a single ontology.
	private static void checkRequiredPredicates(final Vector<TripleInfo> allTriples, final TriplesCollector collector,
			JsonObject required) throws TripleInfoException {
		final String predicateName = required.get("predicate").getAsString().value();
		final String objectValue = required.hasKey("object") ? required.get("object").getAsString().value() : "";
		final String description = required.get("label").getAsString().value();
		final boolean checkAll = required.hasKey("check-all-groups")
				&& required.get("check-all-groups").getAsBoolean().value();
		final boolean isError = required.get("level").getAsString().value().equals("error");
		final String ontology = required.hasKey("ontology") ? required.get("ontology").getAsString().value() : "";

		if (ontology.isEmpty()) {
			System.out.println("Checking for missing " + predicateName + "...");
		} else {
			System.out.println("Checking for missing " + predicateName + " in ontology " + ontology + "...");
		}

		// Collect the wanted PURLs for the check
		HashSet<String> purls = new HashSet<String>();
		HashMap<String, String> purlFilenames = new HashMap<String, String>();

		for (TripleInfo tripleInfo : allTriples) {
			if (!checkAll && (!needsChecking(tripleInfo.group) || !tripleInfo.isInDomain())) {
				continue;
			} else if (tripleInfo.group.equals("deprecated")) {
				continue;
			}
			// TODO dirty hack to exclude ADF
			Pattern adf_pattern = Pattern.compile(
					"http://purl.allotrope.org/ontologies/(datacube|datacube-hdf-map|audit|datapackage|graph|hdf5/1.8)#([a-zA-Z])+([a-zA-Z_0-9])");
			Matcher matcher = adf_pattern.matcher(tripleInfo.subject);
			if (matcher.find()) {
				continue;
			}
			// Some checks are restricted to a single ontology
			if (!ontology.isEmpty() && !tripleInfo.ontology.equals(ontology)) {
				continue;
			}

			// We only need triples that have RDFTypeKey
			if (tripleInfo.predicate.equals(RDF_TYPE_KEY)) {
				final String objectString = tripleInfo.object();
				// We are only interested in triples that have these object
				// types
				if (objectString.equals(OWL_CLASS_KEY) || objectString.equals(OWL_ANNOTATION_PROPERTY_KEY)
						|| objectString.equals(OWL_DATATYPE_PROPERTY_KEY)
						|| objectString.equals(OWL_OBJECT_PROPERTY_KEY)) {
					// Exclude anonymous nodes and filter for the correct domain
					if (!tripleInfo.isAnonymous() && tripleInfo.isInDomain()) {
						purls.add(tripleInfo.subject);
					}
				}
			}
		}

		// Remove all entries that have predicateName and satisfy the object
		// requirement
		for (final TripleInfo checkme : allTriples) {
			if (checkme.predicate.equals(predicateName)) {
				if (objectValue.isEmpty() || checkme.object().equals(objectValue)) {
					purls.remove(checkme.subject);
				}
			} else {
				purlFilenames.put(checkme.subject, checkme.filename);
			}
		}

		// Now write the result
		final String errorMessage = objectValue.isEmpty() ? "Missing " + description
				: "Missing " + description + ", or wrong value";
		for (String purl : purls) {
			final ReportEntry entry = new ReportEntry(errorMessage, purlFilenames.get(purl), purl,
					collector.findPreflabel(purl), objectValue.isEmpty() ? "" : "Expected value: " + objectValue);
			if (isError) {
				errors.add(entry);
			} else {
				warnings.add(entry);
			}
		}
	}

	/**
	 * If a triple's subject is in the Allotrope domain, check if its subject or
	 * object is a subject in a deprecated group and create an error if an entry is
	 * found. Triples may refer to deprecated objects if they are replacing them.
	 * 
	 * Disallow "deprecated" keyword in files that don't contain "deprecated" in
	 * their name, except in notes.
	 * 
	 * Print an error if an OWLDeprecatedKey predicate is present
	 * 
	 * @return true if no entries were found
	 * @throws TripleInfoException
	 */
	private static void checkDeprecated(final Vector<TripleInfo> tripleInfos, final TriplesCollector collector)
			throws TripleInfoException {
		System.out.println("Checking for deprecated items...");

		// Triples may refer to deprecated objects if they are replacing them.
		HashSet<String> whitelist = new HashSet<String>();
		for (TripleInfo tripleInfo : tripleInfos) {
			if (!needsChecking(tripleInfo.group) || !tripleInfo.isInDomain()) {
				continue;
			}
			if (tripleInfo.predicate.equals(DCT_REPLACES_KEY)) {
				whitelist.add(tripleInfo.subject);
			}
		}

		for (TripleInfo tripleInfo : tripleInfos) {
			if (!needsChecking(tripleInfo.group) || !tripleInfo.isInDomain()) {
				continue;
			}
			if (collector.getDeprecatedNodes().contains(tripleInfo.subject)) {
				errors.add(new ReportEntry("Deprecated subject", tripleInfo.filename, tripleInfo.subject,
						collector.findPreflabel(tripleInfo.subject), ""));
			}
			if (collector.getDeprecatedNodes().contains(tripleInfo.object())
					&& !whitelist.contains(tripleInfo.subject)) {
				errors.add(new ReportEntry("Deprecated object", tripleInfo.filename, tripleInfo.subject,
						collector.findPreflabel(tripleInfo.subject),
						"Object: " + tripleInfo.object() + " "
								+ (collector.getPrefLabels().containsKey(tripleInfo.object())
										? collector.getPrefLabels().get(tripleInfo.object())
										: "")));
			}
			if (tripleInfo.object().toLowerCase().contains("deprecated")
					&& !tripleInfo.predicate.toLowerCase().contains("note")) {
				errors.add(new ReportEntry("Found 'deprecated' keyword", tripleInfo.filename, tripleInfo.subject,
						collector.findPreflabel(tripleInfo.subject),
						tripleInfo.predicate + ": " + tripleInfo.object()));
			}
			if (tripleInfo.predicate.equals(OWL_DEPRECATED_KEY)) {
				errors.add(new ReportEntry("Marked as deprecated", tripleInfo.filename, tripleInfo.subject,
						collector.findPreflabel(tripleInfo.subject), ""));
			}
		}
	}

	/**
	 * Looks for regular expression matches. Patterns can be required or forbidden.
	 * 
	 * Example for a configuration:
	 * 
	 * { "predicates" : [ "http://www.w3.org/2004/02/skos/core#definition",
	 * "http://www.w3.org/2004/02/skos/core#prefLabel" ], "position": "object",
	 * "label": "definition or label starts with a date", "level": "error",
	 * "forbidden" : "^\"[0-9][0-9][0-9][0-9]\\-[0-9][0-9]\\-[0-9][0-9].*" },
	 *
	 * "predicates" is optional. If left out, all triples will be checked.
	 * 
	 * "exclude-types" List of rdf:types to exclude
	 * 
	 * "forbidden" is a forbidden pattern. We can also have "required" patterns.
	 * 
	 * @param tripleInfos the triples to check
	 * @param collector   the TriplesCollector for finding labels
	 * @param settings    the settings for this particular check
	 * @throws TripleInfoException
	 */
	private static void checkRegex(Vector<TripleInfo> tripleInfos, final TriplesCollector collector,
			final JsonObject settings) throws TripleInfoException {
		String label = settings.get("label").getAsString().value();

		boolean check_forbidden = settings.hasKey("forbidden");
		boolean check_required = settings.hasKey("required");
		if ((!check_forbidden && !check_required) || (check_forbidden == check_required)) {
			errors.add(new ReportEntry("Configuration error in check-regex!", "", "", "",
					"Expected either \"forbidden\": \"<pattern>\" or \"required\": \"<pattern>\", but nothing found"));
			return;
		}

		Pattern pattern = check_forbidden ? Pattern.compile(settings.get("forbidden").getAsString().value())
				: Pattern.compile(settings.get("required").getAsString().value());

		if (check_forbidden) {
			System.out.println("Checking forbidden regular expression " + pattern.pattern() + " for " + label + "...");
		} else {
			System.out.println("Checking required regular expression " + pattern.pattern() + " for " + label + "...");
		}

		HashSet<String> predicatesToCheck = new HashSet<String>();
		if (settings.hasKey("predicates")) {
			for (JsonValue entry : settings.get("predicates").getAsArray()) {
				predicatesToCheck.add(entry.getAsString().value());
			}
		}

		HashSet<String> typesToExclude = new HashSet<String>();
		if (settings.hasKey("exclude-types")) {
			for (JsonValue entry : settings.get("exclude-types").getAsArray()) {
				typesToExclude.add(entry.getAsString().value());
			}
		}

		// Avoid superfluous checking of subjects
		Set<String> checkedSubjects = new HashSet<>();
		for (TripleInfo tripleInfo : tripleInfos) {
			if (!needsChecking(tripleInfo.group) || !tripleInfo.isInDomain()) {
				checkedSubjects.add(tripleInfo.subject);
			} else if (!predicatesToCheck.isEmpty() && !predicatesToCheck.contains(tripleInfo.predicate)) {
				checkedSubjects.add(tripleInfo.subject);
			} else if (tripleInfo.predicate.equals(RDF_TYPE_KEY) && typesToExclude.contains(tripleInfo.object())) {
				checkedSubjects.add(tripleInfo.subject);
			}
		}

		for (TripleInfo tripleInfo : tripleInfos) {
			if (checkedSubjects.contains(tripleInfo.subject)) {
				continue;
			}

			String position = settings.get("position").getAsString().value();
			if (position.equals("any")) {
				checkRegexOnPosition(collector, settings, label, check_forbidden, pattern, tripleInfo, "subject");
				checkRegexOnPosition(collector, settings, label, check_forbidden, pattern, tripleInfo, "predicate");
				checkRegexOnPosition(collector, settings, label, check_forbidden, pattern, tripleInfo, "object");
				checkedSubjects.add(tripleInfo.subject);
			} else {
				checkRegexOnPosition(collector, settings, label, check_forbidden, pattern, tripleInfo, position);
				if ("subject".equals(position)) {
					checkedSubjects.add(tripleInfo.subject);
				}
			}
		}
	}

	/**
	 * Helper function to perform regex checks
	 * 
	 * @param collector       Contains all triples
	 * @param settings        JSON settings
	 * @param label           Label to be used in error messages
	 * @param check_forbidden If <code>true</code>, perform check for forbidden
	 *                        regex. If <code>false</code>, perform check for
	 *                        required regex
	 * @param pattern         The regex pattern to forbid or required
	 * @param tripleInfo      The triple to be checked
	 * @param position        The position to be checked: "subject", "predicate", or
	 *                        "object"
	 */
	private static void checkRegexOnPosition(final TriplesCollector collector, final JsonObject settings,
			final String label, boolean check_forbidden, Pattern pattern, TripleInfo tripleInfo, String position) {
		String checkme = "";

		switch (position) {
		case "subject":
			if (tripleInfo.isAnonymous()) {
				return;
			}
			checkme = tripleInfo.subject;
			break;
		case "predicate":
			checkme = tripleInfo.predicate;
			break;
		case "object":
			checkme = tripleInfo.object();
			break;
		default:
			errors.add(new ReportEntry("Configuration error in check-regex!", "", "", "",
					"Expected \"position\": \"subject\", \"position\": \"predicate\", \"position\": \"object\" or \"position\": \"any\", but got \"position\": \""
							+ position + "\""));
		}

		Matcher matcher = pattern.matcher(checkme);
		String match = "";
		if (matcher.find()) {
			match = checkme.substring(matcher.start(), matcher.end());
		}
		if (check_forbidden) {

			if (!match.isEmpty()) {
				final ReportEntry entry = new ReportEntry(label, tripleInfo.filename, tripleInfo.subject,
						collector.findPreflabel(tripleInfo.subject),
						"Forbidden pattern \"" + settings.get("forbidden").getAsString().value() + "\" found in "
								+ position + " position: " + checkme);
				if (settings.get("level").getAsString().value().equals("warning")) {
					warnings.add(entry);
				} else {
					errors.add(entry);
				}
			}
		} else {
			if (match.isEmpty()) {
				final ReportEntry entry = new ReportEntry(label, tripleInfo.filename, tripleInfo.subject,
						collector.findPreflabel(tripleInfo.subject),
						"Required pattern \"" + settings.get("required").getAsString().value() + "\" not found in "
								+ position + " position: " + checkme);
				if (settings.get("level").getAsString().value().equals("warning")) {
					warnings.add(entry);
				} else {
					errors.add(entry);
				}
			}
		}
	}

	// Do some general syntax checks:
	// - altLabels and prefLabels must start with a lower-case letter or a
	// number. Upper-case letters are only for allowed acronyms.
	// - definitions must start with an upper-case letter.
	private static void checkCase(Vector<TripleInfo> tripleInfos, final TriplesCollector collector,
			final String specDir, final JsonObject settings) throws TripleInfoException {
		final String predicate = settings.get("predicate").getAsString().value();
		final String label = settings.get("label").getAsString().value();
		final String caseString = settings.get("case").getAsString().value();
		if (!(caseString.equals("upper") || caseString.equals("lower"))) {
			errors.add(new ReportEntry("Configuration error in check-case!", "", "", "",
					"Expected \"case\": \"upper\" or \"case\": \"lower\", but got \"case\": \"" + caseString + "\""));
			return;
		}
		final boolean checkUpperCase = caseString.equals("upper");
		System.out.println("Checking that all " + label + " entries start with " + caseString + " case...");

		// Read proper names from file
		HashSet<String> allowedProperNames = new HashSet<String>();
		if (settings.hasKey("whitelist")) {
			final String allowedProperNamesFile = specDir + "/" + settings.get("whitelist").getAsString().value();
			checkFileExists(allowedProperNamesFile);
			TextFileHandler.read(allowedProperNames, allowedProperNamesFile, true);
		}

		for (TripleInfo tripleInfo : tripleInfos) {
			if (!needsChecking(tripleInfo.group) || !tripleInfo.isInDomain()) {
				continue;
			}

			if (tripleInfo.predicate.equals(predicate)) {
				boolean found_error = false;
				final String testme = tripleInfo.object().substring(1, tripleInfo.object().length() - 1);
				if (checkUpperCase) {
					if (!testme.substring(0, 1).equals(testme.substring(0, 1).toUpperCase())) {
						found_error = true;
					}
				} else {
					if (!testme.substring(0, 1).equals(testme.substring(0, 1).toLowerCase())) {
						found_error = true;
					}
				}
				if (found_error) {
					// Maybe the object starts with a proper name or acronym?
					boolean isProperName = false;
					for (String properName : allowedProperNames) {
						if (testme.startsWith(properName)) {
							isProperName = true;
							break;
						} else {
							StringTokenizer labelTokenzier = new StringTokenizer(testme, " -/");
							if (labelTokenzier.hasMoreTokens()) {
								if (labelTokenzier.nextToken().equals(properName)) {
									isProperName = true;
									break;
								}
							}
						}
					}
					if (!isProperName) {
						errors.add(new ReportEntry(label + " does not start with " + caseString + "-case letter",
								tripleInfo.filename, tripleInfo.subject, collector.findPreflabel(tripleInfo.subject),
								label + ": " + tripleInfo.object()));
					}
				}
			}
		} // triples
	}

	// *********************************************************
	// *************** definitions *****************************
	// *********************************************************

	// Check for unapproved/restricted/missing definition sources and formal
	// errors and report
	private static void checkSources(String ontologyDir, String specDir, JsonObject json,
			Vector<TripleInfo> tripleInfos, final TriplesCollector collector) throws TripleInfoException {

		// Read specification
		String sourcesFile = specDir + "/" + json.get("source-info").getAsString().value();
		checkFileExists(sourcesFile);

		// Source name, filenames
		Map<String, Set<File>> sourceFiles = new HashMap<>();

		// Source name, requirements
		Map<String, RequirementList> sourceRequirements = new HashMap<>();

		for (JsonValue sourceInfoValue : JSON.read(sourcesFile).get("source-info").getAsArray()) {

			// Read conditions
			JsonObject sourceInfo = sourceInfoValue.getAsObject();
			boolean isAllowed = sourceInfo.get("allowed").getAsBoolean().value();
			String hint = sourceInfo.hasKey("hint") ? sourceInfo.get("hint").getAsString().value() : "";
			RequirementList sourceProperties = new RequirementList(isAllowed, hint);

			if (sourceInfo.hasKey("requirements")) {
				JsonObject requirements = sourceInfo.get("requirements").getAsObject();
				if (requirements.hasKey("node")) {
					sourceProperties.addNodeRequirements(requirements.get("node").getAsArray());
				}
				if (requirements.hasKey("file")) {
					sourceProperties.addFileRequirements(requirements.get("file").getAsArray());
				}
			}

			// Read list of source names that these conditions apply to
			for (JsonValue nameValue : sourceInfo.get("sources").getAsArray()) {
				String name = nameValue.getAsString().value();
				sourceRequirements.put(name, sourceProperties);
			}
		}

		// Definition texts with sources need to have the form "Some
		// definition text. [Sourcename]".
		// There can be a trailing localization tag, e.g. "@en-gb".
		// ISO codes can be 2 or 3 letters long, and the country can be omitted.
		Pattern pattern = Pattern.compile("\\. \\[(.+)\\]\"(@(\\w){2,3}(-(\\w){2,3}){0,1}){0,1}$");

		// Check given list of predicates
		for (JsonValue predicateToCheckValue : json.get("predicates").getAsArray()) {
			String predicateToCheck = predicateToCheckValue.getAsString().value();
			String displayFriendlyPredicateToCheck = predicateToCheck.substring(predicateToCheck.lastIndexOf("#") + 1);

			System.out.println("Collecting requirements for " + displayFriendlyPredicateToCheck + " sources...");

			// Look for definition sources
			for (TripleInfo tripleInfo : tripleInfos) {
				if (!needsChecking(tripleInfo.group) || !tripleInfo.isInDomain()) {
					continue;
				}
				// Check whether the triple contains an approved source
				if (tripleInfo.predicate.equals(predicateToCheck)) {
					String match = "";
					Matcher matcher = pattern.matcher(tripleInfo.object());
					if (matcher.find() && matcher.groupCount() > 0) {
						match = tripleInfo.object().substring(matcher.start(1), matcher.end(1));
					} else {
						errors.add(new ReportEntry(
								"Source: Missing, or formal error in " + displayFriendlyPredicateToCheck,
								tripleInfo.filename, tripleInfo.subject, collector.findPreflabel(tripleInfo.subject),
								tripleInfo.object()));
						continue;
					}

					// We can have a list of sources
					StringTokenizer sourceTokenzier = new StringTokenizer(match, ",");
					String checkme = "";
					while (sourceTokenzier.hasMoreTokens()) {
						checkme = sourceTokenzier.nextToken().trim();

						RequirementList properties = null;
						if (sourceRequirements.containsKey(checkme)) {
							// We have it
							properties = sourceRequirements.get(checkme);
						} else {
							// We might still have it, but with inconsistent
							// upper/lowercase
							for (String sourceName : sourceRequirements.keySet()) {
								if (checkme.toUpperCase().equals(sourceName.toUpperCase())) {
									properties = sourceRequirements.get(sourceName);
									errors.add(new ReportEntry(
											"Source: Inconsistent spelling in " + displayFriendlyPredicateToCheck
													+ ", should be [" + sourceName + "]",
											tripleInfo.filename, tripleInfo.subject,
											collector.findPreflabel(tripleInfo.subject), tripleInfo.object()));
								}
							}
						}
						// Source has not been vetted yet
						if (properties == null) {
							errors.add(new ReportEntry(
									"Source: [" + checkme + "] in " + displayFriendlyPredicateToCheck
											+ " is not on our list yet, legal should vet this.",
									tripleInfo.filename, tripleInfo.subject,
									collector.findPreflabel(tripleInfo.subject), tripleInfo.object()));
						} else {
							if (!properties.isAllowed) {
								// Has been vetted and is disallowed
								errors.add(new ReportEntry(
										"Source: [" + checkme + "] in " + displayFriendlyPredicateToCheck
												+ " is not allowed. " + properties.hint,
										tripleInfo.filename, tripleInfo.subject,
										collector.findPreflabel(tripleInfo.subject), tripleInfo.object()));
							} else {
								// Source is allowed. Let's check if there are
								// any requirements
								if (!properties.nodeRequirements.isEmpty()) {
									// Collect per-node checks
									for (Requirement requirement : sourceRequirements.get(checkme).nodeRequirements) {
										requirement.pendingNodeChecks.add(tripleInfo);
									}
								}
								if (!properties.fileRequirements.isEmpty()) {
									// Collect per-file checks
									if (!sourceFiles.containsKey(checkme)) {
										sourceFiles.put(checkme, new HashSet<>());
									}
									sourceFiles.get(checkme).add(new File(ontologyDir + "/" + tripleInfo.filename));
								}
							}
						}
					} // token
				} // predicate matches
			} // triples
		} // predicate

		// Perform per-node checks
		System.out.println("Checking per-node source requirements...");
		for (TripleInfo tripleInfo : tripleInfos) {
			for (RequirementList requirementList : sourceRequirements.values()) {
				for (Requirement requirement : requirementList.nodeRequirements) {
					Iterator<TripleInfo> it = requirement.pendingNodeChecks.iterator();
					while (it.hasNext()) {
						TripleInfo currentRequirement = it.next();
						if (currentRequirement.subject.equals(tripleInfo.subject)) {
							if (checkNodeSatisfiesObjectRequirements(requirement, tripleInfo.objectNode())) {
								requirement.pendingNodeChecks.remove(currentRequirement);
								break;
							}
						}
					}
				}
			}
		}

		// Report per-node checks
		for (RequirementList requirementList : sourceRequirements.values()) {
			for (Requirement requirement : requirementList.nodeRequirements) {
				for (TripleInfo tripleInfo : requirement.pendingNodeChecks) {
					reportUnsatisfiedObjectRequirements(requirement, "Source: ", tripleInfo.filename,
							tripleInfo.subject, tripleInfo.predicate, tripleInfo.object());
				}
			}
		}

		// Perform the per-file checks
		for (String sourceName : sourceFiles.keySet()) {
			System.out.println("Checking per-file requirements for source [" + sourceName + "]...");
			RequirementList requirements = sourceRequirements.get(sourceName);
			for (File file : sourceFiles.get(sourceName)) {
				checkFileExists(file.getAbsolutePath());
				checkFileRequirements(ontologyDir, file.getAbsolutePath(), "Source: ", requirements);
			}
		}
	}

	// *********************************************************
	// *************** prefLabels ******************************
	// *********************************************************

	// Helper function to make sure that a file exists
	private static void checkFileExists(String fileToCheck) {
		File f = new File(fileToCheck);
		if (!f.exists() || f.isDirectory()) {
			handleError(fileToCheck + " does not exist!");
		}
	}

	// Helper function to make sure that a directory exists
	private static void checkDirectoryExists(File f) {
		if (!f.exists() || !f.isDirectory()) {
			handleError(f.getAbsolutePath() + " does not exist or is not a directory!");
		}
	}

	// Make sure that there are no duplicate pref labels in the same namespace
	// that have distinct purls
	private static void checkDuplicates(Vector<TripleInfo> tripleInfos, final TriplesCollector collector,
			final JsonObject parameters) throws TripleInfoException {
		final String predicate = parameters.get("predicate").getAsString().value();
		final String label = parameters.get("label").getAsString().value();

		System.out.println("Checking for duplicate " + label + "s...");

		Vector<TripleInfo> foundTriples = new Vector<TripleInfo>();

		for (TripleInfo tripleInfo : tripleInfos) {
			if (!needsChecking(tripleInfo.group) || !tripleInfo.isInDomain() || tripleInfo.isAnonymous()) {
				continue;
			}

			// Check whether label exists within the same namespace.
			// We use everything in the purl before the # as a
			// namespace.
			if (tripleInfo.predicate.equals(predicate)) {
				for (TripleInfo existing : foundTriples) {
					if (existing.object().equals(tripleInfo.object()) && !existing.subject.equals(tripleInfo.subject)) {
						if (existing.ontology.equals(tripleInfo.ontology)) {
							errors.add(new ReportEntry("Duplicate " + label + " in same domain", tripleInfo.filename,
									tripleInfo.subject, collector.findPreflabel(tripleInfo.subject),
									tripleInfo.object()));
						} else {
							warnings.add(new ReportEntry("Duplicate cross-domain " + label, tripleInfo.filename,
									tripleInfo.subject, collector.findPreflabel(tripleInfo.subject),
									tripleInfo.object() + " occurred already in " + existing.filename));

						}
					}
				}
				foundTriples.add(tripleInfo);
			}
		} // triples
	}

	// *********************************************************
	// *************** Multiples *******************************
	// *********************************************************

	private static class LabelSet {

		private HashMap<String, TripleInfo> labelNames;

		public LabelSet(String purl) {
			super();
			labelNames = new HashMap<String, TripleInfo>();
		}

		public void addLabelName(String labelname, String filename, TripleInfo label) {
			this.labelNames.put(labelname + "|" + filename, label);
		}

		public boolean hasLabel(String labelname, String filename) {
			return labelNames.containsKey(labelname + "|" + filename);
		}
	}

	// Special checks for nodes in <http://purl.allotrope.org/taxonomy>
	// - Must have only 1 superclass
	// - Must not contain any owl:Restriction
	// - Must not contain any defined classes (AFODefinedClassKey contains
	// "true")
	private static void checkTaxonomy(Vector<TripleInfo> tripleInfos, final TriplesCollector collector,
			final String taxonomyName) throws TripleInfoException {
		System.out.println("Checking taxonomy inheritance...");
		HashMap<String, Main.LabelSet> nodes = new HashMap<String, Main.LabelSet>();
		Vector<TripleInfo> multiples = new Vector<TripleInfo>();
		HashSet<String> taxonomyPurls = new HashSet<String>();
		// PURL, filename
		HashMap<String, String> processedNodesWithRestriction = new HashMap<String, String>();

		HashMap<String, String> collectedRestrictions = new HashMap<String, String>();
		HashMap<String, String> objectSubject = new HashMap<String, String>();

		for (TripleInfo tripleInfo : tripleInfos) {
			if (!needsChecking(tripleInfo.group) || !tripleInfo.ontology().equals(taxonomyName)) {
				continue;
			}
			final String subject = tripleInfo.subject;
			final String predicate = tripleInfo.predicate;
			final String object = tripleInfo.object();
			taxonomyPurls.add(subject);
			if (object.equals(OWL_RESTRICTION_KEY)) {
				collectedRestrictions.put(subject, tripleInfo.filename);
			}
			objectSubject.put(object, subject);

			// Check for entries that have multiple superclasses
			if (predicate.equals(RDFS_SUB_CLASS_OF_KEY)) {
				if (nodes.containsKey(predicate)) {
					Main.LabelSet node = nodes.get(predicate);

					if (node.hasLabel(subject, tripleInfo.filename)) {
						multiples.add(tripleInfo);
					} else {
						node.addLabelName(subject, tripleInfo.filename, tripleInfo);
					}
				} else {
					Main.LabelSet node = new Main.LabelSet(predicate);
					node.addLabelName(subject, tripleInfo.filename, tripleInfo);
					nodes.put(predicate, node);
				}
			}
		}

		// Find non-anonymous parents for nodes with restriction and
		// add them to the list
		for (String node : collectedRestrictions.keySet()) {
			while (objectSubject.containsKey(node) && !node.contains("/")) {
				node = objectSubject.get(node);
			}
			processedNodesWithRestriction.put(node, collectedRestrictions.get(node));
		}

		for (final String purl : taxonomyPurls) {
			// Find taxonomy entries that have a restriction
			if (processedNodesWithRestriction.containsKey(purl)) {
				errors.add(new ReportEntry("Entry in Taxonomy has an owl:Restriction",
						processedNodesWithRestriction.get(purl), purl, collector.findPreflabel(purl), ""));
			}
		}

		// Print any multiples
		for (TripleInfo info : multiples) {
			errors.add(new ReportEntry("Entry in Taxonomy has multiple superclasses (rdfs:subClassOf)", info.filename,
					info.subject, collector.findPreflabel(info.subject), ""));
		}
	}

	// Checks whether all definitions are Aristotelian. We consider a definition
	// to be Aristotelian if it contains its superclasses' labels
	// We include the 'external' group for label reference and as a modelling
	// boundary, but we don't check it.
	private static void checkAristotelian(Vector<TripleInfo> tripleInfos, final TriplesCollector collector)
			throws TripleInfoException {
		System.out.println("Checking for Aristotelian definitions...");

		// Collect all subclass relationships, labels and definitions. If a
		// PURL has multiple definitions of labels we ignore that, since
		// those will already fail the multiples check.

		// PURL of node, PURLs of supernodes
		HashMap<String, HashSet<String>> allSuperclasses = new HashMap<String, HashSet<String>>();
		// PURL-prefLabel pairs
		HashMap<String, String> labels = new HashMap<String, String>();
		// PURLs mapped to definition - filename pairs
		HashMap<String, StringPair> definitions = new HashMap<String, StringPair>();

		// Object-subject pairs to track down superclasses higher up
		HashMap<String, String> objectSubject = new HashMap<String, String>();

		Set<String> allSubjects = new HashSet<>();
		HashMap<String, String> subjectType = new HashMap<String, String>();

		// Get labels from external files

		for (TripleInfo tripleInfo : tripleInfos) {
			allSubjects.add(tripleInfo.subject);
			if (tripleInfo.group.equals("external")) {
				// Collect label of a Node
				if (tripleInfo.predicate.equals(SKOS_PREFLABEL_KEY) || tripleInfo.predicate.equals(RDFS_LABEL_KEY)) {
					labels.put(tripleInfo.subject, tripleInfo.object());
				} else if (tripleInfo.predicate.equals(RDFS_SUB_CLASS_OF_KEY)) {
					objectSubject.put(tripleInfo.object(), tripleInfo.subject);
				}
			} else if (needsChecking(tripleInfo.group) && tripleInfo.isInDomain()) {
				// Collect all superclasses of a Node
				if (tripleInfo.predicate.equals(RDFS_SUB_CLASS_OF_KEY)
						|| tripleInfo.predicate.equals(OWL_RESTRICTION_KEY)
						|| tripleInfo.predicate.equals(OWL_EQUIVALENT_CLASS_KEY)
						|| tripleInfo.predicate.equals(OWL_INTERSECTION_OF_KEY)) {
					if (!allSuperclasses.containsKey(tripleInfo.subject)) {
						allSuperclasses.put(tripleInfo.subject, new HashSet<String>());
					}
					allSuperclasses.get(tripleInfo.subject).add(tripleInfo.object());
					objectSubject.put(tripleInfo.object(), tripleInfo.subject);
				} else if (tripleInfo.predicate.equals(SKOS_DEFINITION_KEY)) {
					definitions.put(tripleInfo.subject, new StringPair(tripleInfo.object(), tripleInfo.filename));

				} else if (tripleInfo.predicate.equals(SKOS_PREFLABEL_KEY)) {
					labels.put(tripleInfo.subject, tripleInfo.object());
				} else if (tripleInfo.predicate.equals(RDFS_LABEL_KEY)) {
					// If we inherit from external ontologies, maybe they are
					// using rdfs:label rather than skos:prefLabel
					labels.put(tripleInfo.subject, tripleInfo.object());
				} else if (tripleInfo.predicate.equals(RDF_TYPE_KEY)) {
					// Add the subject type for in-scope entries so that we can
					// check for dangling classes
					subjectType.put(tripleInfo.subject, tripleInfo.object());
				}
			}
		}

		// Now check if all definitions collected contain all their superclass'
		// labels or prefLabels
		for (String subclassPurl : definitions.keySet()) {
			final StringPair definition = definitions.get(subclassPurl);
			// We need to find the parents for anonymous nodes
			Queue<String> actualSuperclasses = new LinkedList<String>();
			if (!allSuperclasses.containsKey(subclassPurl)) {
				String type = subjectType.get(subclassPurl);
				// Every class must have a superclass
				if (OWL_CLASS_KEY.equals(type)) {
					// Nonexisting superclass found - we have a dangling node
					errors.add(new ReportEntry("Dangling node in Aristotelian check", definition.second, subclassPurl,
							collector.findPreflabel(subclassPurl), "No superclass found for class " + subclassPurl));
				}
				// Properties, Named Individuals etc. don't need a superclass,
				// so we simply skip those
				continue;
			}
			for (String superclassPurl : allSuperclasses.get(subclassPurl)) {
				actualSuperclasses.add(superclassPurl);
			}

			// Do the check against the superclasses' labels
			while (!actualSuperclasses.isEmpty()) {
				final String superclassPurl = actualSuperclasses.remove();
				if (!superclassPurl.contains("/")) {
					// We have an anonymous node on our hands, so add its
					// superclass instead
					if (objectSubject.containsKey(superclassPurl)) {
						actualSuperclasses.add(objectSubject.get(superclassPurl));
					}
					continue;
				}
				if (!allSubjects.contains(superclassPurl)) {
					// We're inheriting from a nonexistent superclass
					errors.add(new ReportEntry("Dangling node in Aristotelian check", definition.second, subclassPurl,
							collector.findPreflabel(subclassPurl),
							"Superclass " + superclassPurl + " for class " + subclassPurl + " does not exist"));
				}

				// Now check the superclass' label
				String superclassLabel = "";
				if (labels.containsKey(superclassPurl)) {
					superclassLabel = labels.get(superclassPurl);
				}
				if (!superclassLabel.isEmpty()) {
					// Check if the definition contains the label.
					// Labels have the form "label" and we need to
					// get rid of the "". Also, make it lower-case so that we
					// can deal with sentence case. This should be more
					// sophisticated, but it will do for now.
					if (!definition.first.toLowerCase().contains(superclassLabel.replace("\"", "").toLowerCase())) {
						warnings.add(new ReportEntry("Definition is not Aristotelian", definition.second, subclassPurl,
								collector.findPreflabel(subclassPurl),
								"The definition " + definition.first + " does not contain the prefLabel "
										+ superclassLabel + " of its superclass: " + superclassPurl));
					}
				} else {
					// No label found
					warnings.add(new ReportEntry("Missing label for Aristotelian check", definition.second,
							subclassPurl, collector.findPreflabel(subclassPurl), "The definition " + definition.first
									+ " has a superclass without label or prefLabel: " + superclassPurl));
				}
			}
		}
	}

	// Collects Requirement objects and checks if each file satisfies those
	// conditions.
	// There is also a transitive check performed.
	private static void checkMetadata(String ontologyPath, HashMap<String, StringPair> catalogs, JsonArray required) {
		RequirementList requirements = new RequirementList();
		requirements.addFileRequirements(required.getAsArray());
		System.out.println("Checking for " + requirements.fileRequirements.size() + " required file properties...");

		// Now check the files
		for (String filename : catalogs.keySet()) {
			if (!groupsToCheck.contains(catalogs.get(filename).second)) {
				continue;
			}
			checkFileRequirements(ontologyPath, filename, "Metadata: ", requirements);
		}
	}

	/**
	 * For "multiples" - "warn" from the JSON multiplesSpec, will generate a warning
	 * if an IRI has multiple triples with one of those predicates. For any other
	 * predicates that are not in the "multiples" - "allow" list, generate an error.
	 * 
	 * @throws TripleInfoException
	 */
	private static void checkMultiples(Vector<TripleInfo> tripleInfos, JsonObject multiplesSpec,
			final TriplesCollector collector) throws TripleInfoException {
		System.out.println("Checking for multiple predicates...");
		HashMap<String, Main.LabelSet> nodes = new HashMap<String, Main.LabelSet>();
		Vector<TripleInfo> multiples = new Vector<TripleInfo>();

		// Multiples are allowed for some label types
		HashSet<String> allowedMultiples = new HashSet<String>();
		if (multiplesSpec.hasKey("allow")) {
			for (JsonValue entry : multiplesSpec.get("allow").getAsArray()) {
				allowedMultiples.add(entry.getAsString().value());
			}
		}
		for (TripleInfo tripleInfo : tripleInfos) {
			if (!needsChecking(tripleInfo.group) || !tripleInfo.isInDomain()) {
				continue;
			}
			final String nodename = tripleInfo.subject;
			final String propertyname = tripleInfo.predicate;
			final String content = tripleInfo.object();

			// Check for entries with multiple labels of the same type
			// Rule out localized labels, e.g. "@en-gb
			if (!content.contains("\"@") && !allowedMultiples.contains(propertyname)) {
				if (nodes.containsKey(propertyname)) {
					Main.LabelSet node = nodes.get(propertyname);

					if (node.hasLabel(nodename, tripleInfo.filename)) {
						multiples.add(tripleInfo);
					} else {
						node.addLabelName(nodename, tripleInfo.filename, tripleInfo);
					}
				} else {
					Main.LabelSet node = new Main.LabelSet(propertyname);
					node.addLabelName(nodename, tripleInfo.filename, tripleInfo);
					nodes.put(propertyname, node);
				}
			}
		}

		// Print any multiples
		HashSet<String> multiplesToReport = new HashSet<String>();

		if (multiplesSpec.hasKey("warn")) {
			for (JsonValue entry : multiplesSpec.get("warn").getAsArray()) {
				multiplesToReport.add(entry.getAsString().value());
			}
		}

		for (TripleInfo info : multiples) {
			// Multiples listed in this file don't count as errors
			if (multiplesToReport.contains(info.predicate)) {
				warnings.add(new ReportEntry("Entry has 2 of " + info.predicate, info.filename, info.subject,
						collector.findPreflabel(info.subject), ""));
			} else {
				errors.add(new ReportEntry("Entry has 2 of " + info.predicate, info.filename, info.subject,
						collector.findPreflabel(info.subject), ""));
			}
		}
	}

	/**
	 * Checks if an entity has multiple objects from a list of objects for a given
	 * predicate. Can be configured to produce an error or a warning.
	 * 
	 * @throws TripleInfoException
	 */
	private static void checkPunning(Vector<TripleInfo> tripleInfos, final TriplesCollector collector,
			final JsonObject settings) throws TripleInfoException {

		Set<String> requiredKeys = Set.of("predicate", "objects", "level");
		for (String requiredKey : requiredKeys) {
			if (!settings.hasKey(requiredKey)) {
				errors.add(new ReportEntry("Configuration error in check-punning!", "", "", "",
						"Missing key \"" + requiredKey + "\""));
				System.out.println("Configuration ERROR in check-punning! Missing key \"" + requiredKey + "\".");
				return;
			}
		}

		String errorLevel = settings.get("level").getAsString().value();
		String predicateToCheck = settings.get("predicate").getAsString().value();
		HashSet<String> objectsToCheck = new HashSet<String>();

		for (JsonValue entry : settings.get("objects").getAsArray()) {
			objectsToCheck.add(entry.getAsString().value());
		}

		System.out.print("Checking for punning: " + predicateToCheck + " -");
		for (String entry : objectsToCheck) {
			System.out.print(" " + entry);
		}
		System.out.println("...");

		// <subject, <object, filename>>
		HashMap<String, HashMap<String, String>> foundObjects = new HashMap<String, HashMap<String, String>>();

		for (TripleInfo tripleInfo : tripleInfos) {
			if (!needsChecking(tripleInfo.group) || !tripleInfo.isInDomain()) {
				continue;
			}

			if (predicateToCheck.equals(tripleInfo.predicate) && objectsToCheck.contains(tripleInfo.object())) {
				if (!foundObjects.containsKey(tripleInfo.subject)) {
					foundObjects.put(tripleInfo.subject, new HashMap<String, String>());
				}
				foundObjects.get(tripleInfo.subject).put(tripleInfo.object(), tripleInfo.filename);
			}
		}

		for (String subject : foundObjects.keySet()) {
			HashMap<String, String> entries = foundObjects.get(subject);

			if (entries.size() > 1) {
				String filename = "";
				String errorLabel = "Punning with predicate " + predicateToCheck + ":";
				for (String object : entries.keySet()) {
					filename = entries.get(object);
					errorLabel += " " + object;
				}

				ReportEntry reportEntry = new ReportEntry("Punning", filename, subject,
						collector.findPreflabel(subject), errorLabel);

				if ("error".equals(errorLevel)) {
					errors.add(reportEntry);
				} else {
					warnings.add(reportEntry);
				}
			}
		}
	}

	// Common code for checkRequiredProperties and CheckSources
	private static void checkFileRequirements(String ontologyPath, String filename, String reportPrefix,
			final RequirementList requirements) {
		List<Requirement> fileRequirements = new ArrayList<>();
		fileRequirements.addAll(requirements.fileRequirements);

		// There needs to be a triple with this Subject, <Object, hint>
		Map<String, StringPair> transitiveChecksToDo = new HashMap<>();

		try {
			// Use PrefixMap to convert predicates & objects
			Model model = RDFDataMgr.loadModel(filename);
			for (Requirement requirement : fileRequirements) {
				for (String predicate : requirement.predicates) {
					String newPredicate = model.expandPrefix(predicate);
					if (!predicate.equals(newPredicate) && newPredicate != null && !newPredicate.isEmpty()) {
						requirement.predicates.add(newPredicate);
						requirement.predicates.remove(predicate);
					}
				}
				for (String object : requirement.objects) {
					String newObject = model.expandPrefix(object);
					if (!object.equals(newObject) && newObject != null && !newObject.isEmpty()) {
						requirement.objects.add(newObject);
						requirement.objects.remove(object);
					}
				}
			}

			for (Statement statement : model.listStatements().toList()) {
				Triple checkme = statement.asTriple();
				String subjectAsString = checkme.getSubject().toString();
				String predicateAsString = checkme.getPredicate().toString();
				String objectAsString = checkme.getObject().toString();

				for (Requirement requirement : fileRequirements) {
					boolean needsPredicate = !requirement.predicates.isEmpty();
					boolean hasSubject = requirement.subjects.contains(subjectAsString)
							|| requirement.subjects.isEmpty();
					if (needsPredicate && hasSubject) {
						// Find predicates. If a predicate is present,
						// there are some possible sub-properties to check for
						// their objects.
						if (requirement.predicates.contains(predicateAsString)) {
							if (checkNodeSatisfiesObjectRequirements(requirement, checkme.getObject())) {
								requirement.predicates.clear();
								requirement.objects.clear();
								requirement.objectType = "";
								requirement.objectRegex = null;
								needsPredicate = false;
							}

							// Identify find objects to be used as
							// subjects in transitive checks
							for (String transitiveObject : requirement.transitiveChecks.keySet()) {
								HashSet<String> predicatesForTransitive = requirement.transitiveChecks
										.get(transitiveObject);
								if (predicatesForTransitive.contains(predicateAsString)) {
									transitiveChecksToDo.put(objectAsString,
											new StringPair(transitiveObject, requirement.hint));
									requirement.transitiveChecks.remove(transitiveObject);
									requirement.predicates.clear();
									needsPredicate = false;
								}
							}
						}
					}
					// We only clear the subjects if predicate is not needed or predicate-object was
					// satisfied
					if (!needsPredicate && hasSubject) {
						requirement.subjects.clear();
					}
				}
			} // Statements
		} catch (Exception e) {
			errors.add(new ReportEntry("Exception occurred", filename, "checkFileRequirements", "", e.getMessage()));
			e.printStackTrace();
		}

		String reportFilename = filename.substring(ontologyPath.replace("\\\\", "/").length() + 1);

		// Report unsatisfied objects after all requirements have been gone through
		for (Requirement requirement : fileRequirements) {
			String subjectString = requirement.subjects != null && !requirement.subjects.isEmpty()
					? requirement.subjects.iterator().next()
					: "";
			String predicateString = requirement.predicates != null && !requirement.predicates.isEmpty()
					? requirement.predicates.iterator().next()
					: "";
			String objectString = requirement.objects != null && !requirement.objects.isEmpty()
					? requirement.objects.iterator().next()
					: "";
			reportUnsatisfiedObjectRequirements(requirement, reportPrefix, reportFilename, subjectString,
					predicateString, objectString);
		}

		// Now do the transitive check
		try {
			for (Statement statement : RDFDataMgr.loadModel(filename).listStatements().toList()) {
				final Triple checkme = statement.asTriple();
				final String subjectAsString = checkme.getSubject().toString();
				if (transitiveChecksToDo.containsKey(subjectAsString)) {
					if (transitiveChecksToDo.get(subjectAsString).first.equals(checkme.getObject().toString())) {
						transitiveChecksToDo.remove(subjectAsString);
					}
				}
			}
		} catch (Exception e) {
			errors.add(new ReportEntry("Exception occurred", filename, "checkRequiredProperties (transitive)", "",
					e.getMessage()));
			e.printStackTrace();
		}

		// Errors if some subjects that were identified in the
		// transitive checks are missing their required objects
		for (String subjectAsString : transitiveChecksToDo.keySet()) {
			StringPair reportme = transitiveChecksToDo.get(subjectAsString);
			errors.add(new ReportEntry(reportPrefix + "Required Subject - Object pair is missing", reportFilename,
					subjectAsString, "", "Missing object: " + reportme.first + ". " + reportme.second));
		}
		// Errors for missing predicates and objects
		for (Requirement requirement : fileRequirements) {
			if (!requirement.subjects.isEmpty()) {
				final String subjects = requirement.subjects.toString().replace("[", "").replace("]", "");
				errors.add(new ReportEntry(reportPrefix + "Required Subject is missing", reportFilename, subjects, "",
						"One of these subjects needs to be present: " + subjects + ". " + requirement.hint));
			}
			if (!requirement.predicates.isEmpty()) {
				final String predicates = requirement.predicates.toString().replace("[", "").replace("]", "");
				errors.add(new ReportEntry(reportPrefix + "Required Predicate is missing", reportFilename, predicates,
						"", "One of these predicates needs to be present: " + predicates + ". " + requirement.hint));
			}

			// Errors if predicates for performing transitive checks were
			// not present
			for (String objectAsString : requirement.transitiveChecks.keySet()) {
				errors.add(new ReportEntry(
						reportPrefix + "Predicate for identifying a required triple with the given Object is missing",
						reportFilename, objectAsString, "", "Missing predicates: "
								+ requirement.transitiveChecks.get(objectAsString) + ". " + requirement.hint));
			}
		}

	}

	// Helper function for checkSources and checkFileRequirements. Checks if
	// object conditions are satisfied.
	private static boolean checkNodeSatisfiesObjectRequirements(Requirement requirement, Node object) {
		if (!requirement.objectType.isEmpty()) {
			if (object.isLiteral() && requirement.objectType.equals(object.getLiteralDatatypeURI())) {
				return true;
			} else if (requirement.objectType.equals("URI") && object.isURI()) {
				return true;
			}
		} else if (requirement.objectRegex != null) {
			if (object.isLiteral()) {
				// Parse away " around the string
				String objectString = object.toString();
				if (objectString.startsWith("\"") && objectString.endsWith("\"")) {
					objectString = objectString.substring(1, objectString.length() - 1);
				}
				Matcher matcher = requirement.objectRegex.matcher(objectString);
				if (matcher.find()) {
					return true;
				}
			}
		} else if (!requirement.objects.isEmpty()) {
			String compareme = object.isLiteral() ? object.getLiteralValue().toString() : object.toString();
			if (requirement.objects.contains(compareme)) {
				return true;
			}
		} else {
			return true;
		}
		return false;
	}

	// Helper function for checkSources and checkFileRequirements. Reports any
	// missing objects.
	private static void reportUnsatisfiedObjectRequirements(Requirement requirement, String reportPrefix,
			String filename, String subject, String predicate, String object) {
		if (requirement.objectRegex != null) {
			errors.add(new ReportEntry(reportPrefix + "Predicate has wrong object format", filename, "", subject,
					"'" + object + "' for predicate " + predicate + " needs to satisfy the regex: "
							+ requirement.objectRegex + ". " + requirement.hint));
		} else if (!requirement.objectType.isEmpty()) {
			errors.add(new ReportEntry(reportPrefix + "Predicate has wrong object type", filename, subject, "",
					"'" + object + "' for predicate " + predicate + " needs to be of type " + requirement.objectType
							+ ". " + requirement.hint));
		} else if (!requirement.objects.isEmpty()) {
			errors.add(new ReportEntry(reportPrefix + "Predicate lacks object", filename, subject, "",
					"One of the following objects needs to be present for predicate \"" + predicate + "\": "
							+ requirement.objects + ". " + requirement.hint));
		}
	}

	// Print error message and command line help, then exit 1.
	public static void handleError(String errorMessage) {
		System.out.println("\nError: " + errorMessage);
		System.out.println(
				"\nUsage: java -jar afo-styleguide-checker.jar <ontology-path> <JSON-file-with-specifications> <level> <output-TSV-file>\n");
		System.out.println(" - ontology-path                  Point to the base directory of the ontology");
		System.out.println(" - JSON-file-with-specifications  Point to specification filename");
		System.out.println(
				" - level                          Perform checks at the given level. Levels are specified in the JSON file.");
		System.out.println(" - output-TSV-file                Path to the TSV file to write the output to");
		System.exit(1);
	}

}
