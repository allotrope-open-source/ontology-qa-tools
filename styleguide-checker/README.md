# Ontology Style Guide Checker

This checker tool will run various integrity checks on the ontology's triples.

See our [Devops Wiki](https://gitlab.com/allotrope-open-source/allotrope-devops/wikis/The-Ontology-Styleguide-Checker) for documentation.
